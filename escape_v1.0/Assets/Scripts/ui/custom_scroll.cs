﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class custom_scroll : MonoBehaviour {

	//public float offset_y;

	private float limit_y_top=1136.0f/2.0f-240.0f;

	//screenheight/2 -100.0f;
	private float limit_y_bottom=-(1136.0f/2.0f-200.0f);
	//private float limit_y_bottom=-(1136.0f/2.0f-300.0f);

	private float limit_x_left=-320.0f;

	private float limit_x_right=320.0f;

	public float cellWidth = 120f;
	public float cellHeight = 120f;

	public int max_object_per_line=5;

	public List<List<GameObject>> children= new List<List<GameObject>>();
	public List<GameObject> children_line=new List<GameObject> ();
	public GameObject children_line_gameobject;
	public List<GameObject> children_line_gameobject_list=new List<GameObject> ();
	public int shown_line_pointer = 0;


	public bool is_button_panel=true;
	public bool is_tutorial_window=false;

	public bool is_draggable=false;
	public bool is_x_draggable=false;
	public bool is_y_draggable=false;
	public bool is_pressed=false;
	public bool is_moving=false;

	public Vector2 press_started_position;
	public Vector3 press_started_transform_position;

	public float adjust_spring_power_y=300.0f;
	public float adjust_spring_power_x=300.0f;

	public float cell_spring_power=20.0f;

	public float inertia_y=0;
	public float inertia_x=0;
	public float inertia_sign_y=0;
	public float inertia_sign_x=0;
	public float inertia_brake_power_y= 2000.0f;
	public float inertia_brake_power_x= 2000.0f;

	public bool is_spring_on_x_debug;
	public bool is_spring_on_y_debug;
	public bool is_inertia_on_x_debug;
	public bool is_inertia_on_y_debug;


	// Use this for initialization
	void Start () {
		if(is_button_panel){
			//limit_y_bottom = -(Screen.height/ 2) * (640.0f / Screen.width) + 200.0f;
			limit_y_bottom = -(Screen.height/ 2) * (640.0f / Screen.width) + 400.0f;

		}
	}
	
	// Update is called once per frame
	void Update () {

		if(is_draggable){

			if(is_pressed){

				Vector2 pressed_position= new Vector2(0,0);

				if(get_touched_position(out pressed_position)){

					Vector3 prev_position= transform.localPosition;

					float difference_x= 0;

					if(is_x_draggable){
						difference_x= (pressed_position-press_started_position).x;
					}

					

					float difference_y= 0;

					if(is_y_draggable){
						difference_y= (pressed_position-press_started_position).y;
					}


					Vector3 next_position= press_started_transform_position+ new Vector3(difference_x,difference_y,0);

					transform.localPosition=next_position;

					if(is_x_draggable){

						inertia_x=Mathf.Abs((next_position-prev_position).x/Time.deltaTime);

						inertia_sign_x= Mathf.Sign((next_position-prev_position).x);

					}

					if(is_y_draggable){
						inertia_y=Mathf.Abs((next_position-prev_position).y/Time.deltaTime);

						inertia_sign_y= Mathf.Sign((next_position-prev_position).y);
					}


					change_shown_buttons();

					
				}else{
					Debug.LogError("cannot detect pressed position!");
				}

			

			}else{


				if(is_moving){

					//Vector3 inertia_position= new Vector3(0,0,0);

					bool is_spring_on_x=false;
					
					bool is_inertia_on_x=false;

					bool is_spring_on_y=false;
					
					bool is_inertia_on_y=false;

					float inertia_position_x=0;

					float difference_x= 0;

					float inertia_position_y=0;
					
					float difference_y= 0;

					float position_type_x=0;

					float position_type_y=0;
				


					if(is_x_draggable){

						is_spring_on_x=true;
						
						is_inertia_on_x=true;

				
						float out_bound_length_x=get_out_bound_length_x(transform.localPosition);

					

						position_type_x=Mathf.Sign(out_bound_length_x);

						if(out_bound_length_x==0){
							position_type_x=0;
						}

						if(inertia_x>0){

							//inertia_position= new Vector3(inertia_sign_x*inertia_x*Time.deltaTime,inertia_sign_y*inertia_y*Time.deltaTime,0);

							inertia_position_x=inertia_sign_x*inertia_x*Time.deltaTime;
			
							//inertia_y-=inertia_y*Time.deltaTime*inertia_brake_power_y;
							inertia_x-=inertia_x*Time.deltaTime*inertia_brake_power_x;
							
								/*
							if(inertia_y<1){
								inertia_y=0;
							}*/

							if(inertia_x<1){
								inertia_x=0;
							}


						}else{
								is_inertia_on_x=false;
						}

					

					
						difference_x= -1*out_bound_length_x*adjust_spring_power_x*Time.deltaTime;

						if(is_tutorial_window&&out_bound_length_x==0){

							float cell_outbound=0.0f;

							cell_outbound=(((int) transform.localPosition.x+160) % 960) -480;

							/*
							if(cell_outbound<0){
								cell_outbound=0;
							}*/

							/*
							if(cell_outbound<-360){
								cell_outbound=160;
							//}else if(cell_outbound<-80){
							//	cell_outbound=0;
							}*/
							/*
							if(inertia_x>0&&cell_outbound<0){
								cell_outbound=0;
							}

							if(Mathf.Abs(cell_outbound*1.0f)<5){
								cell_outbound=0;
							}*/

							
							if(cell_outbound<-160&&(inertia_sign_x*inertia_x)<0){
								cell_outbound=160;
							}

							if(cell_outbound>160&&(inertia_sign_x*inertia_x)>0){
								cell_outbound=-160;
							}

							if(Mathf.Abs(cell_outbound*1.0f)>160){
								cell_outbound=Mathf.Sign(cell_outbound)*160;
							}


							/*
							if(Mathf.Abs(cell_outbound*1.0f)>160){
								cell_outbound=0;
							}*/



							difference_x+=-1*cell_outbound*cell_spring_power*Time.deltaTime;

						}
						
						if(Mathf.Abs(difference_x)<1){
							difference_x=-1*position_type_x;
						}


					}

					
					if(is_y_draggable){
						
						is_spring_on_y=true;
						
						is_inertia_on_y=true;
									
						float out_bound_length_y=get_out_bound_length_y(transform.localPosition);

						position_type_y=Mathf.Sign(out_bound_length_y);

						if(out_bound_length_y==0){
							position_type_y=0;
						}
						
						
						
						if(inertia_y>0){
							
							//inertia_position= new Vector3(inertia_sign_x*inertia_x*Time.deltaTime,inertia_sign_y*inertia_y*Time.deltaTime,0);
							
							inertia_position_y=inertia_sign_y*inertia_y*Time.deltaTime;
							
							inertia_y-=inertia_y*Time.deltaTime*inertia_brake_power_y;
							//inertia_x-=inertia_x*Time.deltaTime*inertia_brake_power_x;
							
							/*
							if(inertia_y<1){
								inertia_y=0;
							}*/
							
							if(inertia_y<1){
								inertia_y=0;
							}
							
							
						}else{
							is_inertia_on_y=false;
						}
						

						difference_y= -1*out_bound_length_y*adjust_spring_power_y*Time.deltaTime;

						if(Mathf.Abs(difference_y)<1){
							difference_y=-1*position_type_y;
						}
						
					
						
						
					}


					Vector3 spring_position=  new Vector3(difference_x,difference_y,0);

					Vector3 inertia_position= new Vector3(inertia_position_x,inertia_position_y,0);

					Vector3 next_position= transform.localPosition+spring_position+inertia_position;

					float next_position_x=next_position.x;

					float next_position_y=next_position.y;

					float next_position_z=next_position.z;

					Vector3 current_position =transform.localPosition;


					if(is_x_draggable){
						if(Mathf.Abs(get_out_bound_length_x(next_position))<1){
							
							if(position_type_x==-1){
								
								next_position_x=0;
								
								inertia_x=0;
								
								is_inertia_on_x=false;
								
							}else if(position_type_x==1){
								
								
								Vector3 temp =transform.localPosition;
								
								float available_screen_length = limit_x_right-limit_x_left;
								
								float total_cells_width = max_object_per_line*cellWidth; 
								
								if(total_cells_width-available_screen_length<0){
									
									next_position_x=0;
									
									//transform.localPosition=new Vector3(temp.x,0,temp.z);
								}else{
									
									next_position_x=total_cells_width-available_screen_length;
									
									//transform.localPosition=new Vector3(temp.x,total_cells_height-available_screen_length,temp.z);
								}
								
								inertia_x=0;
								is_inertia_on_x=false;

								
							}

							is_spring_on_x=false;

							
							
							
							
						}
					}

					if(is_y_draggable){
						if(Mathf.Abs(get_out_bound_length_y(next_position))<1){

							if(position_type_y==-1){

								next_position_y=0;

								inertia_y=0;

								is_inertia_on_y=false;

							}else if(position_type_y==1){

								float available_screen_length = limit_y_top - limit_y_bottom;
								
								float total_cells_height = children.Count *cellHeight; 

								if(total_cells_height-available_screen_length<0){

									next_position_y=0;

									//transform.localPosition=new Vector3(temp.x,0,temp.z);
								}else{

									next_position_y=total_cells_height-available_screen_length;

									//transform.localPosition=new Vector3(temp.x,total_cells_height-available_screen_length,temp.z);
								}

								inertia_y=0;

								is_inertia_on_y=false;

							}

							is_spring_on_y=false;


				

						
						}
					}

					next_position= new Vector3(next_position_x,next_position_y,next_position_z);

					transform.localPosition=next_position;

					//print ()

					is_spring_on_x_debug=is_spring_on_x;
					is_spring_on_y_debug=is_spring_on_y;
					is_inertia_on_x_debug=is_inertia_on_x;
					is_inertia_on_y_debug=is_inertia_on_y;

					is_moving=is_inertia_on_x||is_spring_on_x||is_inertia_on_y||is_spring_on_y;

					change_shown_buttons();

				
				}
				



			}

		}

	}

	public float get_out_bound_length_y(Vector3 input_posotion){

		float output = 0;

		float available_screen_length = limit_y_top - limit_y_bottom;

		float total_cells_height =children.Count*cellHeight; 

		if (input_posotion.y == 0) {
			output =0;
		}else if (input_posotion.y < 0) {
			output = input_posotion.y;

		}else if(total_cells_height-available_screen_length<0){

			output = input_posotion.y;
		}else if(input_posotion.y>total_cells_height-available_screen_length){
			output=input_posotion.y-(total_cells_height-available_screen_length);

			//print (input_posotion.y-(total_cells_height-available_screen_length));

		}

		//print ("outbound_y:"+ output);

		return output;
	}

	public float get_out_bound_length_x(Vector3 input_posotion){
		
		float output = 0;
		
		float available_screen_length =limit_x_right- limit_x_left ;
		
		float total_cells_width= max_object_per_line*cellWidth; 
		
		if (input_posotion.x == 0) {
			output =0;
		}else if (input_posotion.x < 0) {
			output = input_posotion.x;
		}else if(total_cells_width-available_screen_length<0){
			output = input_posotion.x;
		}else if(input_posotion.x>total_cells_width-available_screen_length){
			output=input_posotion.x-(total_cells_width-available_screen_length);
			
		}
		
		return output;
	}



	public Vector2 screen_position_to_local_position(Vector2 input_position){
		
		return new Vector2 ((input_position.x - (Screen.width / 2)) * (640.0f / Screen.width), (input_position.y - (Screen.height / 2)) * (640.0f / Screen.width));
	}


	public bool get_touched_position(out Vector2 output_position){
		Vector2 input_position= new Vector2(0,0);
		
		bool output=false;
		
		if (Input.touchCount>0) {
			output=true;
			input_position=  Input.GetTouch(0).position;
		} 
		
		if(Input.GetMouseButton(0)){
			output=true;
			input_position=  new Vector2 (Input.mousePosition.x,Input.mousePosition.y);
		}
		
		
		output_position=screen_position_to_local_position(input_position);

		return output;
	}

	public void pressed(bool input_is_pressed){


		is_pressed = input_is_pressed;

		if (input_is_pressed) {

			press_started_transform_position = transform.localPosition;

			if (!get_touched_position (out press_started_position)) {
				Debug.LogError ("cannot detect pressed position!");
			}
		} else {
			is_moving=true;
		}

	}

	public void drag(Vector2 delta){
		transform.localPosition += new Vector3 (0,delta.y,0);
	}

	public void initialize_position(){
		Vector3 temp =transform.localPosition;
		
		transform.localPosition=new Vector3(temp.x,0,temp.z);

		change_shown_buttons();
	}


	public void add_gameobject(GameObject input_gameobject){

		if(children_line_gameobject==null){
			children_line=new List<GameObject>();
			children.Add (children_line);
			children_line_gameobject= new GameObject();
			children_line_gameobject.name="line "+children.Count;
			if(children.Count>8){
				children_line_gameobject.SetActive(false);
			
			}

			children_line_gameobject.transform.parent=transform;
			children_line_gameobject.transform.localPosition= new Vector3(0,0,0);
			children_line_gameobject.transform.localScale= new Vector3(1,1,1);
			children_line_gameobject_list.Add(children_line_gameobject);
		}
		

		input_gameobject.transform.parent = children_line_gameobject.transform;

		if (input_gameobject.GetComponent<button_select_level_controller> () != null) {
			input_gameobject.GetComponent<button_select_level_controller> ().scroll=this;
		}


		Vector3 temp = input_gameobject.transform.localPosition;



	

		float center_x =max_object_per_line* cellWidth/2.0f;

		int y = children.Count-1 ;

		input_gameobject.transform.localPosition = new Vector3 (((children_line.Count)+0.5f)*cellWidth-center_x,limit_y_top-(y+0.5f)*cellHeight,temp.z);

		children_line.Add (input_gameobject);

		if(children_line.Count==max_object_per_line){

			print ("line count:"+children.Count);


			children_line_gameobject= null;

		}



		//print (input_gameobject.transform.localPosition);
	}

	public void change_shown_buttons(){

		int next_shown_line = 0;

		float out_bound = get_out_bound_length_y(transform.localPosition);

		//print (out_bound);

		if (out_bound ==0) {
			next_shown_line=Mathf.FloorToInt(transform.localPosition.y/cellHeight);
		} else if (out_bound < 0) {

		} else if (out_bound>0){
			next_shown_line=children.Count-8;
		}

		print (next_shown_line);

		if(shown_line_pointer!=next_shown_line){

			int shown_line_pointer_bottom=shown_line_pointer+8;

			int next_shown_line_bottom=next_shown_line+8;


			int top;

			int bottom;



			if(shown_line_pointer<next_shown_line){

				top=shown_line_pointer;

				if(shown_line_pointer_bottom<next_shown_line){
					bottom=shown_line_pointer_bottom;
				}else{
					bottom=next_shown_line;
				}

			}else{
				bottom=shown_line_pointer_bottom;

				if(next_shown_line_bottom<shown_line_pointer){
					top=shown_line_pointer;
				}else{
					top=next_shown_line_bottom;
				}
			}

			if(bottom>children.Count){
				bottom=children.Count;
			}

			for(int i=top;i<bottom;i++){
				print ("hiding line:"+ i);
				children_line_gameobject_list[i].SetActive(false);
			}



			if(shown_line_pointer<next_shown_line){

				bottom=next_shown_line_bottom;
				
				if(shown_line_pointer_bottom<next_shown_line){
					top=shown_line_pointer_bottom;
				}else{
					top=next_shown_line;
				}
			
				
			}else{
				top=next_shown_line;
				
				if(next_shown_line_bottom<shown_line_pointer){
					bottom=next_shown_line_bottom;
				}else{
					bottom=shown_line_pointer;
				}
			}

			if(bottom>children.Count){
				bottom=children.Count;
			}


			for(int i = top;i<bottom;i++){
				children_line_gameobject_list[i].SetActive(true);
			}


			shown_line_pointer=next_shown_line;


		}

	}


}
