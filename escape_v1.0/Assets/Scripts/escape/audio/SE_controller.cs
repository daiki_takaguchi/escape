﻿using UnityEngine;
using System.Collections;

public class SE_controller : MonoBehaviour {

	public AudioClip click;
	public AudioClip clear;
	public AudioClip dot;
	public AudioClip time_out;

	public bool mute=false;

	public enum SE_list{
		click,
		clear,
		dot,
		time_out
	}


	// Use this for initialization
	void Start () {
		/*
		if(PlayerPrefs.HasKey("SE_mute")){
			mute=(PlayerPrefs.GetInt ("SE_mute") == 1) ? true : false;
		}else{
			mute=false;
		}
		*/
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void play_audio(SE_list input_SE){

		if(!mute){

			AudioClip temp=null;

			if(input_SE==SE_list.click){
				temp=click;
			}else if(input_SE==SE_list.clear){
				temp=clear;
			}else if(input_SE==SE_list.dot){
				temp=dot;
			}else if(input_SE==SE_list.time_out){
				temp=time_out;
			}


			audio.PlayOneShot (temp);

		}
		
	}

	public void switch_mute(){
		
		mute = !mute;
		//PlayerPrefs.SetInt ("SE_mute",mute ? 1 : 0);
		
	}




}
