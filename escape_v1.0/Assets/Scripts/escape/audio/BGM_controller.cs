﻿using UnityEngine;
using System.Collections;

namespace ESCAPE.AUDIO{
public class BGM_controller : MonoBehaviour {


	public AudioClip BGM;

	public bool mute=false;


	public enum BGM_state{
		play,
		stop
	}

	public enum BGM_list{
		first
	}
	

	public BGM_state state=BGM_state.play;
	// Use this for initialization
	void Start () {


		set_play_BGM (BGM_list.first);
	}
	
	// Update is called once per frame
	void Update () {
		/*
		if (!audio.isPlaying) {
			if(state!=BGM_state.stop){
				set_play_BGM (BGM_list.first);
			}
		}*/
	}

	public void set_play_BGM(BGM_list input_BGM){
		if (state == BGM_state.stop) {
			set_BGM(input_BGM);
			play_audio();
		}else if (state == BGM_state.play){
			stop_BGM();
			set_BGM(input_BGM);
			play_audio();
		}
	}

	public void play_BGM(){
		if (state == BGM_state.stop) {
			state = BGM_state.play;
			play_audio();

		}
		//}
	}

	public void play_audio(){

		if(!mute){
			if(audio.clip!=null){
				state=BGM_state.play;
				audio.Play ();
			}
		}
	}

	public void stop_BGM(){
		state = BGM_state.stop;
		audio.Stop();
		//if (bgm_state == "stop") {
			
			
		//}
	}


	public void switch_mute(){
		mute = !mute;

		if(mute){
			stop_BGM();
		}else{
			play_BGM();

		}



		//PlayerPrefs.SetInt ("is_BGM_mute",mute ? 1 : 0);

	}

	public void set_BGM(BGM_list input_BGM){

		if(input_BGM==BGM_list.first){
			audio.clip =BGM;
		}


	}


}
}
