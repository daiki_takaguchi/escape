﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ESCAPE.SYSTEM;

namespace ESCAPE.UI{
	public class Window_tutorial_controller : MonoBehaviour {

		// Use this for initialization

		public Ui ui;

		public List<GameObject> slides= new List<GameObject>();

		public Transform scale;

		public int click_count=0;

		public int slide_no=0;


		void Start () {

			if(ui.main.settings.get_screen_type()==Settings.screen_type_enum.ipad_3_4){
				scale.localScale=new Vector3(0.66f,0.66f,1);
				Vector3 position=scale.localPosition;
				position.y=-50.0f;
				scale.localPosition=position;
			}

		
		}
		
		// Update is called once per frame
		void Update () {
			/*
			if(custom_scroll.transform.localPosition.x<640){
				parent.SetActive (false);
			}*/
		}


		void OnClick(){
			if(click_count>=0){
				/*
				foreach(GameObject slide in slides){
					slide.SetActive(false);

				}

				gameObject.SetActive(false);

				if(slide_no==5){


					UI_controller ui=GameObject.FindObjectOfType<UI_controller>();

					ui.UI_label_large.GetComponent<time_count_controller> ().start_count (ui.hitofude_data.settings.dan_stage_remaining_time_in_seconds);

					
				}*/

				if(slide_no!=7){
					hide ();
				}

			}

			click_count++;
		}

		public void show(int no){
			slide_no = no;
			click_count = 0;
			slides[no].SetActive(true);
		}

		public void hide(){
			foreach(GameObject slide in slides){
				slide.SetActive(false);
				
			}
			
			gameObject.SetActive(false);
			
			if(slide_no==6){
				
				
				UI_controller ui=GameObject.FindObjectOfType<UI_controller>();
				
				ui.UI_label_large.GetComponent<time_count_controller> ().start_count (ui.hitofude_data.settings.dan_stage_remaining_time_in_seconds);
				
				
			}
		}
	}
}
