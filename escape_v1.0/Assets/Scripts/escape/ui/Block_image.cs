﻿using UnityEngine;
using System.Collections;
using ESCAPE.DATA;
using ESCAPE.SYSTEM;
using System;
namespace ESCAPE.UI{

	public class Block_image : MonoBehaviour {

		public Block current_block;
		public Block_base current_block_base;
		public BoxCollider box;
		public GameObject  block_image_gameobject;
		public GameObject block_item_gameobject;
		public GameObject block_back_gameobject;
		public UISprite block_image;
		public Depth_controller block_image_depth;
		public UISprite block_item_image;
		public Depth_controller block_item_image_depth;
		public UISprite block_back_image;
		public Depth_controller block_back_image_depth;

		//private float block_x_center=64*3.5f;
		//private float block_y_center=-64*4.5f;
		public bool is_pressed;

		//public Vector3 mouse_pressed_position;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void reset(Block input_block){
			current_block = input_block;

			block_image_gameobject.SetActive (true);

			//initialize_position (input_block.current_block_base);

			change_image ();

			change_block_image ();

			reposition(current_block.current_block_base);


		}



		public void reset(Block_base input_block_base){
			current_block_base = input_block_base;
					
			//initialize_position (input_block_base);

			change_image ();

			reposition(input_block_base);

			//disabel_box ();
		}


		/*
		public void initialize_position(){
			Block_base temp=null;

			if(current_block_base!=null){
				temp=current_block_base;
			}else if(current_block!=null){
				temp=current_block.current_block_base;
			}


			initialize_position(temp);

		}*/

		public void reposition(Block_base input_block_base){
			if(input_block_base!=null){

				Vector3 temp_position = transform.localPosition;
				
				temp_position.x = input_block_base.position_x*64-Main.main_static.data.block_base_list.block_base_center_x;
				temp_position.y = input_block_base.position_y*(-64)+Main.main_static.data.block_base_list.block_base_center_y;
				transform.localPosition = temp_position;
			}
		
		}

		public void disabel_box(){
			box.enabled = false;
		}

		public void change_color(Color color){
			block_image.color = color;
		}

		public void change_image(){

			if(current_block!=null){

				Const.Data.Block_group_type input_type=current_block.current_group.block_group_type;

				int input_group_no=current_block.current_group.group_no;

				block_back_image.color= new Color32(204,0,0,255);

				if(input_group_no>=0&&input_group_no<20){

					if(input_type==Const.Data.Block_group_type.normal){
						block_image.spriteName="gam_block_"+input_group_no.ToString("D2");
					}else if(input_type==Const.Data.Block_group_type.human){

						block_image.spriteName="gam_block_00";

						block_item_gameobject.SetActive(true);
						block_item_image.spriteName="gam_block_item_00";

					}else if(input_type==Const.Data.Block_group_type.killer){

						block_image.spriteName="gam_block_"+input_group_no.ToString("D2");

						block_item_gameobject.SetActive(true);
						block_item_image.spriteName="gam_block_item_02";

					}else if(input_type==Const.Data.Block_group_type.number){

						block_image.spriteName="gam_block_"+input_group_no.ToString("D2");
						
						block_item_gameobject.SetActive(true);
						change_count_image();

					}else if(input_type==Const.Data.Block_group_type.key){
						block_image.spriteName="gam_block_"+input_group_no.ToString("D2");
						
						block_item_gameobject.SetActive(true);
						block_item_image.spriteName="gam_block_item_03";
						block_back_image_depth.change_layer(Depth_controller.depth_layer.Block_base_item);
					}


				}
			}else if(current_block_base!=null){

				Const.Data.Block_base_type input_type=current_block_base.block_base_type;

				if (input_type == Const.Data.Block_base_type.empty) {

					block_image_gameobject.SetActive (true);
					
					block_image_depth.change_layer (Depth_controller.depth_layer.Block_base);

					block_back_gameobject.SetActive (true);
					
					block_back_image_depth.change_layer (Depth_controller.depth_layer.Block_back);

					block_image.spriteName = "gam_block_back";
					
				} else if (input_type == Const.Data.Block_base_type.wall) {

					block_image.gameObject.SetActive (false);
					
				}else if(input_type==Const.Data.Block_base_type.gate){

					block_image_gameobject.SetActive (true);

					block_image.spriteName = "gam_block_back";
					
					block_image_depth.change_layer (Depth_controller.depth_layer.Block_base);
					
					block_back_gameobject.SetActive (true);
					
					block_back_image_depth.change_layer (Depth_controller.depth_layer.Block_back);

					block_item_gameobject.SetActive(true);

					block_item_image.spriteName="gam_block_item_01";



					//change_gate_image();
					//block_item_image.spriteName="gam_block_item_01";
					
				}else if(input_type==Const.Data.Block_base_type.locked_gate){

					block_image_gameobject.SetActive (true);

					block_image.spriteName = "gam_block_back";
					
					block_image_depth.change_layer (Depth_controller.depth_layer.Block_base);
					
					block_back_gameobject.SetActive (true);
					
					block_back_image_depth.change_layer (Depth_controller.depth_layer.Block_back);

					block_item_gameobject.SetActive(true);

					change_gate_image();

					
				}else if(input_type==Const.Data.Block_base_type.lock_base){

					block_image_gameobject.SetActive (true);
					
					block_image_depth.change_layer (Depth_controller.depth_layer.Block_base);
					
					block_back_gameobject.SetActive (true);
					
					block_back_image_depth.change_layer (Depth_controller.depth_layer.Block_back);
					
					block_image.spriteName = "gam_block_back";

					block_item_gameobject.SetActive(true);

					block_item_image.spriteName="gam_block_item_04";
					
				}
			}
			
		}

		public void change_block_image (){
			if(current_block!=null){
				if(current_block.current_group.block_group_state==Const.Data.Block_group_state.none){

					block_image_depth.change_layer (Depth_controller.depth_layer.Block);
					block_item_image_depth.change_layer (Depth_controller.depth_layer.Block_item);
					block_back_gameobject.SetActive(false);
					//block_back_image_depth.change_layer (Depth_controller.depth_layer.Block);

				}else if(current_block.current_group.block_group_state==Const.Data.Block_group_state.selected){

					block_image_depth.change_layer (Depth_controller.depth_layer.Block_selected);
					block_item_image_depth.change_layer (Depth_controller.depth_layer.Block_item_selected);
					block_back_gameobject.SetActive(true);
					block_back_image_depth.change_layer (Depth_controller.depth_layer.Block_back_selected);
				}
			}
		}

		public void change_count_image (){
			if(current_block!=null){
				if (current_block.current_group.block_group_type == Const.Data.Block_group_type.number) {
					block_item_image.spriteName = "gam_block_move" + current_block.current_group.current_count.ToString ("D2");
				} else {
					Debug.LogError("");
				}
			}
		}


		public void change_gate_image(){

			if(current_block_base!=null){
				if (current_block_base.block_base_type==Const.Data.Block_base_type.locked_gate) {

					//block_item_image.spriteName = "gam_block_move" + current_block.current_group.current_count.ToString ("D2");
					if(current_block_base.block_base_state==Const.Data.Block_base_state.empty){

						block_item_image.spriteName="gam_block_item_01";
					}else if(current_block_base.block_base_state==Const.Data.Block_base_state.gate_closed){
						block_item_image.spriteName="gam_block_item_01b";
					}else{
						Debug.LogError("error!");
					}
				
				} else {
					Debug.LogError("");
				}
			}
		
		}
			


		void animation_end(object sender,EventArgs e){


		}

		public void on_press (bool input_pressed){
		
			if (current_block != null) {

				if (input_pressed) {

					//Main.main_static.ui.SE.play_audio (SE_controller.SE_list.dot);

					Main.main_static.game_state_manager.set_mouse_pressed_position ();

					Main.main_static.game_state_manager.select_group (current_block.current_group);



				} else {
					if (is_pressed) {
						print ("press_end");
					}
				
				}


				is_pressed = input_pressed;
			}else if(current_block_base!=null){

				if(!current_block_base.has_block()){
				print ("block base pressed");



					Main.main_static.game_state_manager.move_if_movable(
						Const.Data.get_opposite_direction(current_block_base.is_selected_group_in_neighborhood()));

				}

			}
		}

		public void on_drag (){



			if (current_block != null) {
				if (is_pressed) {

					if(Main.main_static.data.block_group_list.selected_group==current_block.current_group){

						Main.main_static.game_state_manager.check_if_move();
					}
				}
			}else if(current_block_base != null){

			}
			

			
		}
	}
}
