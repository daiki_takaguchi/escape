﻿using UnityEngine;
using System.Collections;
using ESCAPE.SYSTEM;
using ESCAPE.AUDIO;
using System.Collections.Generic;

namespace ESCAPE.UI{
	public class Ui : MonoBehaviour {

	// Use this for initialization
		// Use this for initialization
		
		public bool is_unlock_url_found=false;
		
		public Camera UI_camera;
		//public Camera UI_camera_UI_2;
		
		public ad_controller ad;
		/*
		public bool is_select_level_show;
		public bool is_select_level_hide;
		public int current_button_count;
		public int frame_count=0;*/

		public List<int> unrefreshed_select_buttons_level = new List<int> ();
		public List<int> unrefreshed_select_buttons_dan = new List<int> ();

		public ESCAPE.SYSTEM.Main main;
		
		public UI_animation_controller UI_animator;
		
		
		public answer_point_animation answer_point;
		
		
		
		public BGM_controller BGM;
		public SE_controller SE;
		
		public GameObject button_pressed_animation_parent;
		public GameObject button_pressed_animation;
		
		
		public GameObject select_level_button;
		
		public GameObject UI_background_title;
		public GameObject UI_background_play;
		public GameObject UI_background_play_label;
		public GameObject UI_background_select_level;
		
		public GameObject UI_effect;
		
		public GameObject UI_buttons_title;
		
		public custom_scroll UI_buttons_select_level_parent;
		public custom_scroll UI_buttons_select_dan_parent;
		
		public GameObject UI_buttons_result;
		
		public GameObject[] UI_buttons_select_level;
		public GameObject[] UI_buttons_select_dan;
		
		public GameObject UI_button_mode_normal_parent;
		public GameObject UI_button_mode_dan_parent;
		public GameObject UI_button_mode_dan;

		/*
		public GameObject UI_button_home;
		public GameObject UI_button_undo;
		public GameObject UI_button_restart;
		public GameObject UI_button_hint;
		public GameObject UI_button_answer;
		*/

		public Buttons_bottom UI_buttons_bottom;
		
		
		public GameObject UI_button_applipromotion;
		
		//public GameObject UI_buttons_bottom_background;
		
		public GameObject UI_window_hint;
		public GameObject UI_window_answer;
		public GameObject UI_window_result;
		public window_result_controller UI_window_result_controller;
		public GameObject UI_window_result_comment;
		
		public GameObject UI_window_dan_clear;
		public GameObject UI_game_over;
		public GameObject UI_window_tutorial;
		//public UIAnchor UI_window_tutorial_anchor;
		
		public GameObject UI_label_large;
		public GameObject UI_label_small;
		public GameObject UI_label_turn;
		
		public GameObject UI_label_progress_level;
		public GameObject UI_label_progress_dan;
		
		public GameObject UI_label_select_level;
		
		//public panel_scroll_controller scroll_level;
		//public panel_scroll_controller scroll_dan;
		
		public Transform UI_touch_panel_scale;
		public Transform UI_touch_panel_position;
		public GameObject UI_button_touch_panel;
		
		//public bool is_animation_on;
		//public bool is_window_shown=false;
		public bool is_button_animation_on=false;
		public bool is_repositioning_screen=false;
		
		public enum UI_state{
			title,
			title_animation,
			select_level,
			select_level_animation,
			select_dan,
			select_dan_animation,
			hitofude_normal,
			hitorude_animation,
			hitofude_hint,
			hitofude_hint_window,
			hitofude_answer,
			hitofude_answer_window,
			button_animation,
			result,
			dan_clear,
			time_out
		}
		
		public UI_state state= UI_state.title;
		public UI_state prev_state=UI_state.title;
		
		public int result_shown_count=0;
		
		void Start () {
			
			Application.targetFrameRate = 60;
			
			create_UI_buttons_select_level ();

			for(int i=0;i<main.settings.max_level;i++){
				unrefreshed_select_buttons_level.Add(i);
			}
		
			refresh_UI_buttons_select_level ();

			hide_UI_buttons_select_level ();


			create_UI_buttons_select_dan ();

			for(int i=0;i<main.settings.max_dan;i++){
				unrefreshed_select_buttons_dan.Add(i);
			}


			refresh_UI_buttons_select_dan ();
			hide_UI_select_dan();

			
			refresh_UI_buttons_title();
			
			UI_background_title.GetComponent<Background_title_controller>().initialize ();



			refresh_UI_button_applipromotion (true);
			
			StartCoroutine (check_unlock_url());
			
		}
		
		
		private IEnumerator check_unlock_url(){
			WWW www = new WWW(main.settings.dan_unlock_url);
			
			yield return StartCoroutine(ResponceCheckForTimeOutWWW(www, 5.0f));
			
			if (www.error != null) {
				
			}else if(www.isDone){
				is_unlock_url_found=true;
			}
			//Debug.Log(www.text); //リクエスト成功の場合
		}
		
		private IEnumerator ResponceCheckForTimeOutWWW(WWW www, float timeout)
		{
			float requestTime = Time.time;
			
			while(!www.isDone)
			{
				if(Time.time - requestTime < timeout)
					yield return null;
				else
				{
					Debug.LogWarning("TimeOut"); //タイムアウト
					break;
				}
			}
			
			yield return null;
		}
		

		
		// Update is called once per frame
		void Update () {
			/*
			if(is_select_level_show){

				if(frame_count>3){
					if(current_button_count>=UI_buttons_select_level.Length){

						is_select_level_show=false;

					}else{
						show_UI_buttons_select_level(current_button_count,current_button_count+20);
						current_button_count+=20;
					}
					frame_count=0;
				
				}
				frame_count++;


			}

			if(is_select_level_hide){
				
				if(frame_count>3){
					if(current_button_count>=UI_buttons_select_level.Length){
						
						is_select_level_hide=false;
						
					}else{
						hide_UI_buttons_select_level(current_button_count,current_button_count+20);
						current_button_count+=20;
					}
					frame_count=0;
					
				}
				frame_count++;
				
				
			}*/
			
		}
		
		public bool is_button_available(Button.button_type input_type){
			
			bool output = false;
			
			if(state==UI_state.title){
				if(input_type==Button.button_type.button_mode_normal
				   ||input_type==Button.button_type.button_mode_dan){
					output=true;
				}
				
			}else if(state==UI_state.select_level){
				
				if(input_type==Button.button_type.button_select_level
				   ||input_type==Button.button_type.button_select_dan
				   ||input_type==Button.button_type.button_home){
					output=true;
				}
				
			}else if(state==UI_state.select_dan){
				
				if(input_type==Button.button_type.button_select_level
				   ||input_type==Button.button_type.button_select_dan
				   ||input_type==Button.button_type.button_home){
					output=true;
				}
			}else if(state==UI_state.hitofude_normal){
				output=true;
				
				
				if(input_type==Button.button_type.button_select_level
				   ||input_type==Button.button_type.button_select_dan
				   ||input_type==Button.button_type.button_next){
					output=false;
				}

				if(main.game_state_manager.game_state!=Const.Data.Game_state.play){

					if(input_type!=Button.button_type.button_home_no_SE){
						output=false;
					}


				}
			}else if(state==UI_state.hitofude_hint){
				output=true;
				/*if( input_type==Button.button_type.button_hint){
			output=false;
		}*/
			}else if(state==UI_state.hitofude_hint_window){
				if(input_type==Button.button_type.button_hint_yes||input_type==Button.button_type.button_hint_no){
					output=true;
				}
			}else if(state==UI_state.hitofude_answer){
				output=true;
				
				/*
		if( input_type==Button.button_type.button_hint||input_type==Button.button_type.button_answer){
			output=false;
		}*/
			}else if(state==UI_state.hitofude_answer_window){
				
				if(input_type==Button.button_type.button_answer_yes||input_type==Button.button_type.button_answer_no){
					output=true;
				}
				
			}else if(state==UI_state.result){
				output=true;
			}
			
			if(input_type==Button.button_type.button_applipromotion){
				output=true;
			}
			
			if(input_type==Button.button_type.button_unlock_dan_no
			   ||input_type==Button.button_type.button_unlock_dan_yes){
				output=true;
			}
			
			if(is_repositioning_screen){
				if(input_type!=Button.button_type.touch_panel){
					output=false;
				}
			}
			
			return output&&(!is_button_animation_on);
		}
		
		public void button_pressed_delay(Button input_button){
			if (is_button_available (input_button.type)) {
				
				float wait_time=0.0f;
				
				
				if(input_button.type==Button.button_type.button_mode_normal
				   ||input_button.type==Button.button_type.button_mode_dan){
					
					button_pressed_animation_start (input_button,main.settings.button_mode_ripple_diameter);
					wait_time=main.settings.button_pressed_animation_wait_time;
					SE.play_audio(SE_controller.SE_list.click);

					
				}else if(input_button.type==Button.button_type.button_home
				         ||input_button.type==Button.button_type.button_undo
				         ||input_button.type==Button.button_type.button_restart
				         ||input_button.type==Button.button_type.button_hint
				         ||input_button.type==Button.button_type.button_answer){
					
					button_pressed_animation_start (input_button,main.settings.button_home_ripple_diameter);
					wait_time=main.settings.button_pressed_animation_wait_time;
					SE.play_audio(SE_controller.SE_list.click);
					
				}else if(input_button.type==Button.button_type.button_hint_yes
				         ||input_button.type==Button.button_type.button_hint_no
				         ||input_button.type==Button.button_type.button_answer_yes
				         ||input_button.type==Button.button_type.button_answer_no){
					
					button_pressed_animation_start (input_button,main.settings.button_window_ripple_diameter);
					wait_time=main.settings.button_pressed_animation_wait_time;
					SE.play_audio(SE_controller.SE_list.click);
					
				}else if(input_button.type==Button.button_type.button_select_level
				         ||input_button.type==Button.button_type.button_select_dan){
					
					button_pressed_animation_start (input_button,main.settings.button_level_ripple_diameter);
					wait_time= main.settings.button_pressed_animation_wait_time;
					SE.play_audio(SE_controller.SE_list.click);
					
				}else if(input_button.type==Button.button_type.button_next
				         ||input_button.type==Button.button_type.button_tweet
				         ||input_button.type==Button.button_type.button_review){
					
					button_pressed_animation_start (input_button,main.settings.button_next_ripple_diameter);
					wait_time=main.settings.button_pressed_animation_wait_time;
					SE.play_audio(SE_controller.SE_list.click);
					
				}else if(input_button.type==Button.button_type.button_applipromotion){
					
					//button_pressed_animation_start (input_button,hitofude_data.settings.button_window_ripple_diameter);
					wait_time=main.settings.button_pressed_animation_wait_time;
					SE.play_audio(SE_controller.SE_list.click);
					
				}else if(input_button.type==Button.button_type.button_unlock_dan_yes){
					
					//wait_time=hitofude_data.settings.button_pressed_animation_wait_time;
					SE.play_audio(SE_controller.SE_list.click);
					
				}else if(input_button.type==Button.button_type.button_unlock_dan_no){
					//wait_time=hitofude_data.settings.button_pressed_animation_wait_time;
					SE.play_audio(SE_controller.SE_list.click);
				}
				
				is_button_animation_on=true;
				StartCoroutine(button_pressed_wait_for_seconds(input_button,wait_time));
			}
		}
		
		public IEnumerator button_pressed_wait_for_seconds(Button input_button,float wait_time){
			yield return new WaitForSeconds(wait_time);
			
			is_button_animation_on=false;
			button_pressed(input_button);
			
		}
		
		
		public void button_pressed(Button input_button){
			if(is_button_available(input_button.type)){
				if(input_button.type==Button.button_type.button_mode_normal){
					
						show_UI_select_level ();
						
						
						UI_animator.animation_start(UI_animation_controller.animation_type.normal_mode);
						
						
						
						change_state(UI_state.select_level_animation);
						
						
						
					}else if(input_button.type==Button.button_type.button_mode_dan){
						
						show_UI_select_dan();
						
						
						
						
						//ui_animator.Play ("background-play_slide_anim");
						UI_animator.animation_start(UI_animation_controller.animation_type.dan_mode);
						
						
						
						
						change_state(UI_state.select_dan_animation);
						
					}else if(input_button.type==Button.button_type.button_home){
						
						ad.hide_icon();
						
						show_UI_title();
						
						
						UI_animator.animation_start(UI_animation_controller.animation_type.home);
						
						UI_label_large.GetComponent<time_count_controller> ().stop_count();
						
						
						
						change_state(UI_state.title_animation);
						
					}else if(input_button.type==Button.button_type.button_home_no_SE){
						
						ad.hide_icon();
						
						show_UI_title();
						
						
						
						UI_animator.animation_start(UI_animation_controller.animation_type.home);
						
						UI_label_large.GetComponent<time_count_controller> ().stop_count();
						
						
						change_state(UI_state.title_animation);
						
					}else if(input_button.type==Button.button_type.button_undo){
						
						main.game_state_manager.undo();
						
						
					}else if(input_button.type==Button.button_type.button_restart){
						
						main.game_state_manager.reset();
						
					}else if(input_button.type==Button.button_type.button_hint){
						
						/*
						if((!main.)){
							
							UI_label_large.GetComponent<time_count_controller> ().pause_count ();
							show_UI_window_hint ();
							
							
							
							change_state(UI_state.hitofude_hint_window);
						}*/
						
						
					}else if(input_button.type==Button.button_type.button_hint_yes){
						
						
						
						UI_label_large.GetComponent<time_count_controller> ().resume_count ();
						
						main.save_data.hint_used ();
						//main.button.hint_yes_pressed();
						
						hide_UI_window_hint ();
						
						
						
						change_state(UI_state.hitofude_hint);
						
					}else if(input_button.type==Button.button_type.button_hint_no){
						
						
						
						UI_label_large.GetComponent<time_count_controller> ().resume_count ();
						
						hide_UI_window_hint ();
						
						
						
						change_state(prev_state);
						
					}else if(input_button.type==Button.button_type.button_answer){
						
						/*
						if((!hitofude_data.is_answer_mode)){
							UI_label_large.GetComponent<time_count_controller> ().pause_count ();
							show_UI_window_answer ();
							
							
							
							change_state(UI_state.hitofude_answer_window);
						}*/
						
					}else if(input_button.type==Button.button_type.button_answer_yes){
						
						
						
						main.save_data.answer_used ();
						//main.button.answer_yes_pressed();
						
						UI_label_large.GetComponent<time_count_controller> ().resume_count ();
						
						
						hide_UI_window_answer();
						
						
						
						change_state(UI_state.hitofude_answer);
						
					}else if(input_button.type==Button.button_type.button_answer_no){
						
						
						
						UI_label_large.GetComponent<time_count_controller> ().resume_count ();
						
						hide_UI_window_answer();
						
						change_state(prev_state);
						
						
					}else if(input_button.type==Button.button_type.button_next){

						main.game_state_manager.next_level();
					
					//if(Main.main_static.game_state_manager.game_mode==leve)
					//	main.game_state_manager.next_level();
					//}
						
						
						/*
						if(hitofude_data.is_dan){
							
							Button temp = new Button();
							temp.type=Button.button_type.button_home_no_SE;
							
							button_pressed (temp);
						}else{
							if(hitofude_data.current_level==(hitofude_data.settings.max_level-1)){
								Button temp = new Button();
								temp.type=Button.button_type.button_home_no_SE;
								
								button_pressed (temp);
								
							}else{
								hide_UI_result();
								show_UI_play_level(hitofude_data.current_level+1);
								
							}
						}
						*/
						
						
					}else if(input_button.type==Button.button_type.button_tweet){
						
						string url="";
						
						if(Application.platform==RuntimePlatform.IPhonePlayer){
							
							url=main.settings.tweet_url_iOS;
						}else if(Application.platform==RuntimePlatform.Android){
							url=main.settings.tweet_url_android;
						}
						
						string comment="";
						
						if(Application.platform==RuntimePlatform.IPhonePlayer){
							
							comment=main.settings.tweet_comment_iOS;
						}else if(Application.platform==RuntimePlatform.Android){
							comment=main.settings.tweet_comment_android;
						}
						
						SocialConnector.Share (comment,url);
						
					}else if(input_button.type==Button.button_type.button_review
					         ||input_button.type==Button.button_type.button_unlock_dan_yes){
						
						string url="https://www.google.com/";
						
						if(Application.platform==RuntimePlatform.IPhonePlayer){
							
							url=main.settings.review_url_iOS;
						}else if(Application.platform==RuntimePlatform.Android){
							url=main.settings.review_url_andorid;
						}
						
						if(input_button.type==Button.button_type.button_unlock_dan_yes){
							main.save_data.set_dan_ready();
							UI_window_tutorial.GetComponent<Window_tutorial_controller>().hide ();
						}
						
						Application.OpenURL(url);
						
						
						
					}else if(input_button.type==Button.button_type.button_select_level){
						
						show_UI_play_level (input_button.level);
						//hide_UI_select_level ();
						

						UI_animator.animation_start(UI_animation_controller.animation_type.play_level);
						
						change_state(UI_state.hitofude_normal);
						
					}else if(input_button.type==Button.button_type.button_select_dan){
						
						show_UI_play_dan (input_button.level);
						//hide_UI_select_dan ();
						
						UI_animator.animation_start(UI_animation_controller.animation_type.play_dan);
						
						change_state(UI_state.hitofude_normal);
						
					}else if(input_button.type==Button.button_type.button_applipromotion){
						//ad.show_wall();

						string url=main.settings.osusume_url_android;

						if(Application.platform==RuntimePlatform.IPhonePlayer){
							url=main.settings.osusume_url_iOS;
						}
						
						
						
						Application.OpenURL(url);
						
					}else if(input_button.type==Button.button_type.button_unlock_dan_no){
						
						UI_window_tutorial.GetComponent<Window_tutorial_controller>().hide ();
					}
					
					
				}
			}
			
			public void button_point_pressed(hitofude_point_controller pressed){
				/*
				if(is_button_available(Button.button_type.point)){
					if(hitofude_data.is_point_available(pressed)){
						
						SE.play_audio(SE_controller.SE_list.dot);
						pressed.change_state(hitofude_point_controller.point_state.selected);
						hitofude_data.add_selected_line(pressed);
						
					}
				}*/
			}
			
			
			
			
			
			
			
			public void button_result_pressed(){
				
				//hitofude_data.clear_data ();

				
				UI_label_large.GetComponent<time_count_controller> ().pause_count ();
				
				UI_buttons_bottom.hide(Buttons_bottom.Button_buttom_type.all );
				/*
				UI_button_undo.SetActive (false);
				UI_button_restart.SetActive (false);
				UI_button_hint.SetActive (false);
				UI_button_answer.SetActive (false);
				*/
				
				
				show_UI_result ();

				
				
				//.UI_button_touch_panel.GetComponent<button_touch_panel_controller>().disable_touch_panel();
				change_state (UI_state.result);
				
			}
			
			
			public void button_next_dan_stage_pressed(){
				
				
				
				UI_label_large.GetComponent<time_count_controller> ().pause_count ();
				//UI_window_dan_clear.SetActive (true);
				
				//print ();
				UI_window_dan_clear.GetComponent<window_dan_clear_controller> ().show_window ((main.game_state_manager.current_dan_stage+1)+"/"+main.settings.max_dan_stage+" Cleared");
				
				
				change_state(UI_state.dan_clear);
				
				StartCoroutine (hide_UI_window_dan_clear_timer(1.5f));
				
			}
			
			public void button_time_out_pressed(){
				
				//if()
				UI_window_dan_clear.GetComponent<window_dan_clear_controller> ().show_window (window_dan_clear_controller.animation_state.time_out);
				
				UI_buttons_bottom.hide (Buttons_bottom.Button_buttom_type.all);
				/*
				UI_button_home.SetActive (false);
				UI_button_undo.SetActive (false);
				UI_button_restart.SetActive (false);
				UI_button_hint.SetActive (false);
				UI_button_answer.SetActive (false);*/
				
				change_state (UI_state.time_out);
				
				SE.play_audio (SE_controller.SE_list.time_out);
				
				StartCoroutine (hide_UI_window_dan_time_out_timer(1.5f));
				
				//}
			}
			
			
			public void button_touch_panel_pressed(Vector2 input_position){
				
				if(is_button_available(Button.button_type.touch_panel)){
					//print (input_position.x + "," + input_position.y);
					//print ("ok");
					//hitofude_data.show_dragged_line (input_position);
				}
				
			}
			
			public void button_touch_panel_unpressed(){
				
				if(is_button_available(Button.button_type.touch_panel_unpressed)){
					//hitofude_data.hide_dragged_line ();
				}
				
			}
			
			public void button_touch_panel_pinched(Vector2 input_position,float input_scale){
				
				if(is_button_available(Button.button_type.touch_panel)){
					scale_UI_play(input_position,input_scale);
				}
				
			}
			
			
			public void animation_end(UI_animation_controller.animation_type input_type){
				
				print ("animation_end:"+input_type);
				
				if (input_type == UI_animation_controller.animation_type.normal_mode||input_type == UI_animation_controller.animation_type.dan_mode) {
					
					hide_UI_title ();
					
					
					if(input_type == UI_animation_controller.animation_type.normal_mode){
						//show_UI_buttons_select_level(main.settings.max_button_level_shown,UI_buttons_select_level.Length);
						//scroll_level.reposition();
						//scroll_level.refresh();
						//current_button_count=main.settings.max_button_level_shown;
						//is_select_level_show=true;
						//is_select_level_hide=false;
						enable_UI_buttons_select_level();
					}else{
						//show_UI_buttons_select_dan(main.settings.max_button_level_shown,UI_buttons_select_dan.Length);
						//scroll_dan.reposition();
						//scroll_dan.refresh();
						enable_UI_buttons_select_dan();
					}
					
					change_state(UI_state.select_level);
					
				} else if(input_type==UI_animation_controller.animation_type.play_level){
					
					hide_UI_select_level();
					//current_button_count=main.settings.max_button_level_shown;
					//is_select_level_show=false;
					//is_select_level_hide=true;
					
					
				} else if(input_type==UI_animation_controller.animation_type.play_dan){
					
					hide_UI_select_dan();
					
				} else if(input_type==UI_animation_controller.animation_type.home){
					hide_UI_play();
					hide_UI_select_dan();
					
					hide_UI_select_level();
					//current_button_count=main.settings.max_button_level_shown;
					//is_select_level_show=false;
					//is_select_level_hide=true;
					
					
					change_state(UI_state.title);
					
					
				}
				
			}
			
			public void create_UI_buttons_select_level(){
				
				UI_buttons_select_level =new GameObject[main.settings.max_level];
				for (int i =0;i<main.settings.max_level;i++){
					GameObject temp = (GameObject) Instantiate (select_level_button);
					UI_buttons_select_level[i]=temp;
					
					UI_buttons_select_level_parent.add_gameobject(temp);
					
					
					//temp.transform.localPosition=new Vector3(0,0,0);
					temp.SetActive(true);
					temp.transform.localScale=new Vector3(1,1,1);
					temp.GetComponent<Button_select_level>().ui=this;
					temp.GetComponent<Button_select_level>().color.ui=this;
					temp.GetComponent<Button_select_level>().change_label(i,(i+1).ToString(),main.save_data.get_select_level_data(i),false);
					temp.GetComponent<Button_select_level>().scroll=UI_buttons_select_level_parent;
					//temp.GetComponent<Button_select_level>().show_button();
				}
				
				//UI_buttons_select_level_parent.gameObject.SetActive (false);
				
				
			}
			
			
			
			
			public void create_UI_buttons_select_dan(){
				
				UI_buttons_select_dan =new GameObject[main.settings.max_dan];
				
				for (int i =0;i<main.settings.max_dan;i++){
					GameObject temp = (GameObject) Instantiate (select_level_button);
					
					UI_buttons_select_dan[i]=temp;
					
					//temp.transform.parent = UI_buttons_select_dan_parent.transform;
					
					UI_buttons_select_dan_parent.add_gameobject(temp);
					
					//temp.transform.localPosition=new Vector3(0,0,0);
					temp.SetActive(true);
					temp.transform.localScale=new Vector3(1,1,1);
					temp.GetComponent<Button_select_level>().ui=this;
					temp.GetComponent<Button_select_level>().color.ui=this;
					temp.GetComponent<Button_select_level>().change_label(i,main.settings.int_to_dan(i),main.save_data.get_select_dan_data(i),true);
					temp.GetComponent<Button_select_level>().scroll=UI_buttons_select_dan_parent;
			}
				
				
				//UI_buttons_select_dan_parent.GetComponent<UIGrid> ().Reposition ();
				
			}
			
			public void refresh_UI_buttons_select_level(){
				
				List<int> refreshed = new List<int> ();
				foreach (int i in unrefreshed_select_buttons_level) {
					
					refreshed.Add(i);

					UI_buttons_select_level[i].GetComponent<Button_select_level>().set_state(main.save_data);
				}

				
				foreach (int i in refreshed) {
				
					unrefreshed_select_buttons_level.Remove (i);
				}
				
			}
			
			public void destroy_UI_buttons_select_level(){
				foreach (GameObject temp in UI_buttons_select_level) {
					Destroy(temp.gameObject);
				}
				
			}
			
			
			
			
			public void refresh_UI_buttons_select_dan(){
				foreach (GameObject temp in UI_buttons_select_dan) {
					temp.GetComponent<Button_select_level>().set_state(main.save_data);
				}
			}
			
			public void destroy_UI_buttons_select_dan(){
				foreach (GameObject temp in UI_buttons_select_dan) {
					Destroy(temp.gameObject);
				}
				
			}
			
			public void refresh_UI_button_applipromotion(bool is_title){
				
				Vector3 temp= UI_button_applipromotion.transform.localPosition;
				
				if (main.settings.get_screen_type () == Settings.screen_type_enum.ipad_3_4) {
					
					
					
					//print ("aaaaa");
					
					if(is_title){
						
						UI_button_applipromotion.transform.localPosition= new Vector3(220,-50,temp.z);
						
					}else{
						UI_button_applipromotion.transform.localPosition= new Vector3(220,-130,temp.z);
					}
					
				}else if(main.settings.get_screen_type () == Settings.screen_type_enum.iphone_4_2_3){
					UI_button_applipromotion.transform.localPosition= new Vector3(220,-50,temp.z);
				}
				
				
			}
			
			public void show_UI_title(){
				
				
				UI_background_play_label.SetActive(false);
				UI_background_title.SetActive (true);
				UI_buttons_title.SetActive(true);
				
				refresh_UI_buttons_title ();
				
				refresh_UI_button_applipromotion (true);
				
			}
			
			public void hide_UI_title(){
				UI_background_title.SetActive (false);
				UI_buttons_title.SetActive (false);
				
				UI_label_progress_level.GetComponent<UILabel> ().text = "";
				UI_label_progress_dan.GetComponent<UILabel> ().text = "";
			}
			
			
			
			public void show_UI_select_level(){

				
				
				UI_label_select_level.GetComponent<Depth_controller> ().change_layer (Depth_controller.depth_layer.Background_2_label,UI_label_select_level.layer);
				
				UI_label_select_level.GetComponent<UILabel> ().text = "Select Level";
				
				UI_background_play_label.SetActive(true);
				UI_background_select_level.SetActive (true);

				UI_buttons_bottom.show (Buttons_bottom.Button_buttom_type.home);
				//UI_button_home.SetActive (true);
				//UI_buttons_bottom_background.SetActive (true);
				
				refresh_UI_buttons_select_level ();
				
				//UI_buttons_select_level_parent.gameObject.SetActive (true);
				
				UI_buttons_select_level_parent.enabled = true;
				UI_buttons_select_level_parent.initialize_position ();
				//scroll_level.reposition ();
				
				//show_UI_buttons_select_level (0,main.settings.max_button_level_shown);
				show_UI_buttons_select_level ();
				
				
				
			}
			
			
			
			public void show_UI_select_dan(){
				
				//UI_label_large.GetComponent<UILabel> ().text = "";
				UI_label_select_level.GetComponent<Depth_controller> ().change_layer (Depth_controller.depth_layer.Background_2_label,UI_label_select_level.layer);
				
				UI_label_select_level.GetComponent<UILabel> ().text = "IQ Test";

				UI_background_play_label.SetActive(true);
				UI_background_select_level.SetActive (true);
				//UI_button_home.SetActive (true);
				UI_buttons_bottom.show(Buttons_bottom.Button_buttom_type.home);
				//UI_buttons_bottom_background.SetActive (true);
				
				refresh_UI_buttons_select_dan ();
				//UI_buttons_select_dan_parent.gameObject.SetActive (true);
				UI_buttons_select_dan_parent.enabled = true;
				UI_buttons_select_dan_parent.initialize_position ();
				
					
				show_UI_buttons_select_dan ();
				//show_UI_buttons_select_dan(0,main.settings.max_button_level_shown);
				
		
				
				
			}
			
			public void show_UI_buttons_select_level(){
				Vector3 temp = UI_buttons_select_level_parent.gameObject.transform.localPosition;

				
				temp.z = 0;
				
						
				UI_buttons_select_level_parent.gameObject.transform.localPosition = temp;
				
			}

			public void hide_UI_buttons_select_level(){
				Vector3 temp = UI_buttons_select_level_parent.gameObject.transform.localPosition;
				
				
				temp.z = 200;
				
				
				UI_buttons_select_level_parent.gameObject.transform.localPosition = temp;

				
				
			}

			/*
			public void show_UI_buttons_select_level(int input_min, int input_max){
				
				if(input_min<input_max&&input_min<UI_buttons_select_level.Length){
					int limit =input_max;
					
					if(limit>UI_buttons_select_level.Length){
						limit=UI_buttons_select_level.Length;
					}
					
					for(int i=input_min;i<limit;i++){
						
						UI_buttons_select_level[i].GetComponent<Button_select_level>().show_button();
						
					}
				}
				
				//scroll_level.panel.Refresh ();
				
			}*/
			
			public void enable_UI_buttons_select_level(){
				foreach (GameObject temp in UI_buttons_select_level) {
					temp.GetComponent<Button_select_level>().enable_button();
				}
				
			}

			public void disable_UI_buttons_select_level(){
				foreach (GameObject temp in UI_buttons_select_level) {
					temp.GetComponent<Button_select_level>().disable_button();
				}
				
			}
			
			public void hide_UI_select_level(){
				
				UI_background_select_level.SetActive (false);
				UI_label_select_level.GetComponent<UILabel> ().text = "";
				/*foreach (GameObject temp in UI_buttons_select_level) {
					temp.GetComponent<Button_select_level>().hide_button();
				}*/
				//hide_UI_buttons_select_level (0,main.settings.max_button_level_shown);
				hide_UI_buttons_select_level ();
				//UI_buttons_select_level_parent.gameObject.SetActive (false);
				UI_buttons_select_level_parent.enabled = false;
				//UI_buttons_bottom_background.SetActive (false);
				disable_UI_buttons_select_level();
				
				
				//UI_buttons_select_level_parent.SetActive (false);
			}

			
			/*
			public void hide_UI_buttons_select_level(int input_min, int input_max){
				
				if(input_min<input_max&&input_min<UI_buttons_select_level.Length){
					int limit =input_max;
					
					if(limit>UI_buttons_select_level.Length){
						limit=UI_buttons_select_level.Length;
					}
					
					for(int i=input_min;i<limit;i++){
						
						UI_buttons_select_level[i].GetComponent<Button_select_level>().hide_button();
						
					}
				}
				
				//scroll_level.panel.Refresh ();
				
			}*/
			/*
			public void show_UI_buttons_select_dan(int input_min, int input_max){
				
				if(input_min<input_max&&input_min<UI_buttons_select_dan.Length){
					int limit =input_max;
					
					if(limit>UI_buttons_select_dan.Length){
						limit=UI_buttons_select_dan.Length;
					}
					
					for(int i=input_min;i<limit;i++){
						UI_buttons_select_dan[i].GetComponent<Button_select_level>().show_button();
					}
				}

				
				
				//scroll_dan.panel.Refresh ();
			}*/
			
			public void show_UI_buttons_select_dan(){
				Vector3 temp = UI_buttons_select_dan_parent.gameObject.transform.localPosition;
				
				
				temp.z = 0;
				
				
				UI_buttons_select_dan_parent.gameObject.transform.localPosition = temp;
				
			}

			public void hide_UI_buttons_select_dan(){
				Vector3 temp = UI_buttons_select_dan_parent.gameObject.transform.localPosition;
				
				
				temp.z =200;
				
				
				UI_buttons_select_dan_parent.gameObject.transform.localPosition = temp;
				
			}
			public void enable_UI_buttons_select_dan(){
				foreach (GameObject temp in UI_buttons_select_dan) {
					temp.GetComponent<Button_select_level>().enable_button();
				}
			}

			public void disable_UI_buttons_select_dan(){
				foreach (GameObject temp in UI_buttons_select_dan) {
					temp.GetComponent<Button_select_level>().disable_button();
				}
			}
			
			public void hide_UI_select_dan(){
				
				UI_background_select_level.SetActive (false);
				
				UI_label_select_level.GetComponent<UILabel> ().text = "";
				/*
				foreach (GameObject temp in UI_buttons_select_dan) {
					temp.GetComponent<Button_select_level>().hide_button();
				}*/

				hide_UI_buttons_select_dan ();
				
				//UI_buttons_select_dan_parent.gameObject.SetActive (false);
				UI_buttons_select_dan_parent.enabled = false;
				//UI_buttons_bottom_background.SetActive (false);
				disable_UI_buttons_select_dan ();
				//UI_buttons_select_dan_parent.SetActive (false);
				
				
			}
			
			
			
			
			
			public void show_UI_play_level(int input_level){
				
				UI_background_play.SetActive (true);
				
				//UI_label_large.SetActive (false);
				//UI_label_large.GetComponent<UILabel> ().text = "aa";
				
				//UI_label_large.GetComponent<depth_controller> ().change_layer (depth_controller.depth_layer.Background_1,UI_label_large.layer);
				
				UI_label_large.GetComponent<Depth_controller> ().change_layer (Depth_controller.depth_layer.Button,UI_label_large.layer);
				UI_label_small.GetComponent<Depth_controller> ().change_layer (Depth_controller.depth_layer.Button,UI_label_small.layer);
				UI_label_turn.GetComponent<Depth_controller> ().change_layer (Depth_controller.depth_layer.Button,UI_label_turn.layer);
				
				UI_label_large.GetComponent<UILabel> ().text = "";
				UI_label_small.GetComponent<UILabel> ().text = "Level "+(input_level+1);
				UI_label_turn.GetComponent<UILabel> ().text = "Turn 1";
				
				UI_buttons_bottom.show(Buttons_bottom.Button_buttom_type.all);
				/*
				UI_button_home.SetActive (true);
				UI_button_undo.SetActive (true);
				UI_button_restart.SetActive (true);*/
				//UI_button_hint.SetActive (true);
				//UI_button_answer.SetActive (true);
				
				main.game_state_manager.load_level (input_level);
				
				//UI_camera.GetComponent<UICamera> ().stickyPress = false;
				UI_camera.GetComponent<UICamera> ().stickyPress = true;

				UI_button_touch_panel.GetComponent<Button_touch_panel>().enable_touch_panel();

				//UI_window_tutorial_anchor.enabled=false;
				
				show_UI_window_tutorial_level(input_level);
				
				refresh_UI_button_applipromotion (false);
				
				
			}
			
			public void show_UI_window_tutorial_level(int input_level){
				
				if(!main.save_data.has_tutorial_shown(input_level,false)){
					
					
					if(input_level==0){
						UI_window_tutorial.SetActive(true);
						UI_window_tutorial.GetComponent<Window_tutorial_controller>().show (0);
					}else if(input_level==1){
						UI_window_tutorial.SetActive(true);
						UI_window_tutorial.GetComponent<Window_tutorial_controller>().show (1);
					}else if(input_level==5){
						UI_window_tutorial.SetActive(true);
						UI_window_tutorial.GetComponent<Window_tutorial_controller>().show (2);
					}else if(input_level==10){
						UI_window_tutorial.SetActive(true);
						UI_window_tutorial.GetComponent<Window_tutorial_controller>().show (3);
						
					}else if(input_level==15){
						
						UI_window_tutorial.SetActive(true);
						UI_window_tutorial.GetComponent<Window_tutorial_controller>().show (4);
					/*
					}else if(input_level==19){
						
						UI_window_tutorial.SetActive(true);
						UI_window_tutorial.GetComponent<Window_tutorial_controller>().show (7);*/
					}
				}
				
			}
			
			/*
	public void hide_UI_hitofude_level(){

		UI_label_large.GetComponent<time_count_controller> ().stop_count ();
		UI_background_play.SetActive (false);

		UI_button_home.SetActive (false);
		UI_button_undo.SetActive (false);
		UI_button_restart.SetActive (false);
		UI_button_hint.SetActive (false);
		UI_button_answer.SetActive (false);


	}*/
			
			
			
			public void show_UI_play_dan(int input_level){
				
				print (input_level+"-dan");
				
				UI_background_play.SetActive (true);
				
				UI_label_large.GetComponent<Depth_controller> ().change_layer (Depth_controller.depth_layer.Button,UI_label_large.layer);
				UI_label_small.GetComponent<Depth_controller> ().change_layer (Depth_controller.depth_layer.Button,UI_label_small.layer);
				UI_label_turn.GetComponent<Depth_controller> ().change_layer (Depth_controller.depth_layer.Button,UI_label_turn.layer);
				
				/*
		if(input_level!=0){

			UI_label_large.GetComponent<time_count_controller> ().start_count (hitofude_data.settings.dan_stage_remaining_time_in_seconds);

		}*/
				UI_label_small.GetComponent<UILabel> ().text = "IQ "+main.settings.int_to_dan(input_level)+"  1/"+main.settings.max_dan_stage;
				//UI_label_small.GetComponent<UILabel> ().text = "Level "+(input_level+1);
				UI_label_turn.GetComponent<UILabel> ().text = "Turn 1";
				
				UI_label_large.GetComponent<ngui_screen_adjust> ().type = ngui_screen_adjust.object_type.large_label_dan;
				UI_label_large.GetComponent<ngui_screen_adjust> ().reposition ();
				
				UI_label_small.GetComponent<ngui_screen_adjust> ().type = ngui_screen_adjust.object_type.small_label_dan;
				UI_label_small.GetComponent<ngui_screen_adjust> ().reposition ();
				
				UI_buttons_bottom.show(Buttons_bottom.Button_buttom_type.all);
				/*
				UI_button_home.SetActive (true);
				UI_button_undo.SetActive (true);
				UI_button_restart.SetActive (true);*/
				//UI_button_hint.SetActive (true);
				//UI_button_answer.SetActive (true);
				
				main.game_state_manager.load_dan (input_level);
				
				UI_camera.GetComponent<UICamera> ().stickyPress = true;
				//UI_camera_UI_2.GetComponent<UICamera> ().stickyPress = false;
				//UI_button_touch_panel.GetComponent<button_touch_panel_controller>().enable_touch_panel();
				UI_button_touch_panel.GetComponent<Button_touch_panel>().enable_touch_panel();
				//UI_camera_UI_2.GetComponent<UICamera> ().stickyPress = false;
				
				//show_UI_window_tutorial_dan (input_level);

				//UI_window_tutorial_anchor.enabled=false;
				
				if (!show_UI_window_tutorial_dan (input_level)) {
					
					UI_label_large.GetComponent<time_count_controller> ().start_count (main.settings.dan_stage_remaining_time_in_seconds);
					
					//UI_label_small.GetComponent<UILabel> ().text =
					
				} else {
					UI_label_large.GetComponent<time_count_controller> ().set_text(main.settings.dan_stage_remaining_time_in_seconds);
				}
				
				refresh_UI_button_applipromotion (false);


			}
			
			public bool show_UI_window_tutorial_dan(int input_level){
				
				bool output = false;
				
				if(!main.save_data.has_tutorial_shown(input_level,true)){
					
					
					if(input_level==0){
						UI_window_tutorial.SetActive(true);
						UI_window_tutorial.GetComponent<Window_tutorial_controller>().show (5);
						
						output=true;
					}
				}
				
				return output;
				
			}
			
			public void show_UI_hitofude_dan_again(){
				
				
				//UI_label_large.SetActive (true);
				//UI_label_large.GetComponent<depth_controller> ().change_layer (depth_controller.depth_layer.Button,UI_label_large.layer);
				UI_label_large.SetActive (true);
				UI_label_large.GetComponent<time_count_controller> ().resume_count ();
				//UI_label_small.GetComponent<UILabel> ().text =  main.settings.int_to_dan(hitofude_data.current_dan)+" "+(hitofude_data.current_dan_stage+2)+"/"+hitofude_data.settings.max_dan_stage;
				
				UI_label_turn.GetComponent<UILabel> ().text = "Turn 0";
				
				UI_label_large.GetComponent<ngui_screen_adjust> ().type = ngui_screen_adjust.object_type.large_label_dan;
				UI_label_large.GetComponent<ngui_screen_adjust> ().reposition ();
				
				UI_label_small.GetComponent<ngui_screen_adjust> ().type = ngui_screen_adjust.object_type.small_label_dan;
				UI_label_small.GetComponent<ngui_screen_adjust> ().reposition ();
				

				UI_buttons_bottom.show(Buttons_bottom.Button_buttom_type.all);
				/*
				UI_button_home.SetActive (true);
				UI_button_undo.SetActive (true);
				UI_button_restart.SetActive (true);
				UI_button_hint.SetActive (true);
				UI_button_answer.SetActive (true);*/
				
				main.game_state_manager.load_dan_again ();
				
				UI_camera.GetComponent<UICamera> ().stickyPress = false;
				//UI_camera_UI_2.GetComponent<UICamera> ().stickyPress = false;
				UI_button_touch_panel.GetComponent<button_touch_panel_controller>().enable_touch_panel();
				//UI_camera_UI_2.GetComponent<UICamera> ().stickyPress = false;
			}
			
		public void hide_UI_play(){
			//main.data.clear_data();
			scale_UI_play (new Vector2(0,0),1);
			
			UI_label_large.GetComponent<ngui_screen_adjust> ().type = ngui_screen_adjust.object_type.large_label_level;
			UI_label_large.GetComponent<ngui_screen_adjust> ().reposition ();
			
			UI_label_small.GetComponent<ngui_screen_adjust> ().type = ngui_screen_adjust.object_type.small_label_level;
			UI_label_small.GetComponent<ngui_screen_adjust> ().reposition ();
			
			
			UI_label_large.GetComponent<UILabel> ().text = "";
			UI_label_small.GetComponent<UILabel> ().text = "";
			UI_label_turn.GetComponent<UILabel> ().text = "";
			
			UI_background_play.SetActive (false);
			UI_background_play_label.SetActive(false);

			UI_buttons_bottom.hide(Buttons_bottom.Button_buttom_type.all);
			/*
			UI_button_home.SetActive (false);
			UI_button_undo.SetActive (false);
			UI_button_restart.SetActive (false);
			UI_button_hint.SetActive (false);
			UI_button_answer.SetActive (false);*/
			
			hide_UI_result ();
			
			UI_camera.GetComponent<UICamera> ().stickyPress = true;
			//UI_camera_UI_2.GetComponent<UICamera> ().stickyPress = true;
			//UI_button_touch_panel.GetComponent<button_touch_panel_controller>().disable_touch_panel();
			UI_button_touch_panel.GetComponent<Button_touch_panel>().disable_touch_panel();
			
		}
		
		public void scale_UI_play(Vector2 center, float scale_factor ){
			//UI_touch_panel_scale.localScale = new Vector3 (scale_factor,scale_factor,1);
			//UI_touch_panel_position.localPosition = new Vector3 (center.x,center.y,0);
			/*
			if(hitofude_data.settings.fix_scale_lines_and_points){
				hitofude_data.rescale_lines_and_points(1/scale_factor);
			}*/
		}
		

			
			
		public void show_UI_window_hint(){
			
			UI_window_hint.SetActive (true);
		}
		
		public void hide_UI_window_hint(){
			UI_window_hint.SetActive (false);
		}
		
		public void show_UI_window_answer(){
			
			UI_window_answer.SetActive (true);
		}
		
		public void hide_UI_window_answer(){
			
			UI_window_answer.SetActive (false);
		}
		
		
		
		
		public void show_UI_result(){
			
			UI_window_result.SetActive (true);

			SE.play_audio (SE_controller.SE_list.clear);
			
			UI_window_result_controller.show_result (main.game_state_manager.get_game_result());
			
			UI_window_result_comment.GetComponent<comment_switcher> ().show (main.game_state_manager.current_level);
		
			if((main.game_state_manager.game_mode==Game_state_manager.Game_mode.normal)&&(main.game_state_manager.current_level==19)&&(!main.save_data.is_dan_ready())){
				//print ("is_dan_ready"+hitofude_data.save_data.is_dan_ready());
				
				if(is_unlock_url_found){
					UI_window_tutorial.SetActive(true);
					UI_window_tutorial.GetComponent<Window_tutorial_controller>().show(7);
				}
			}

			
			//if((Application.systemLanguage != SystemLanguage.Japanese)&&hitofude_data.settings.show_interstitial_automatically){
			if(main.settings.show_interstitial_automatically){
				//print ("result shown: "+ result_shown_count);
				
				result_shown_count++;
				
				if(result_shown_count>=main.settings.interstitial_interval){
					result_shown_count=0;
					//ad.show_intersititial_google();
					
					ad.show_wall_result();
				}else{
					
				}
				
			}
			
			UI_buttons_result.SetActive (true);
			
			
		}
		
		public void hide_UI_result(){
			
			
			
			UI_window_result.GetComponent<window_result_controller> ().hide_result ();
			
			UI_window_result.SetActive (false);
			UI_buttons_result.SetActive (false);
			
			ad.hide_icon ();
		}
		
		
		IEnumerator hide_UI_window_dan_clear_timer(float time){
			yield return new WaitForSeconds(time);
			
			//hitofude_data.clear_data ();
			hide_UI_window_dan_clear ();
			show_UI_hitofude_dan_again ();
			
			change_state (UI_state.hitofude_normal);
		}
		
		IEnumerator hide_UI_window_dan_time_out_timer(float time){
			
			yield return new WaitForSeconds(time);
			
			hide_UI_window_dan_time_out();
			

			
		}


		public void hide_UI_window_dan_clear(){
			UI_window_dan_clear.SetActive (false);
		}
		
		public void hide_UI_window_dan_time_out(){
			UI_window_dan_clear.SetActive (false);
			change_state (UI_state.hitofude_normal);
			
			Button temp =new Button();
			temp.type = Button.button_type.button_home_no_SE;
			button_pressed (temp);
		}
		
		public Const.Ui.Level_state result_to_level_state(Const.Data.Game_result input_result){
			Const.Ui.Level_state output = Const.Ui.Level_state.Zero;
			
			
			if(input_result==Const.Data.Game_result.perfect){
				output=Const.Ui.Level_state.Three;
			}else if(input_result==Const.Data.Game_result.fantastic){
				output=Const.Ui.Level_state.Two;
			}else if(input_result==Const.Data.Game_result.good){
				output=Const.Ui.Level_state.One;
			}
			
			return output;
			
		}
		
		public void button_pressed_animation_start(Button input_button, int input_diameter){
			
			GameObject temp = (GameObject)Instantiate ( (GameObject)button_pressed_animation);

			temp.SetActive (true);
			
			temp.transform.parent = button_pressed_animation_parent.transform;
			
			temp.GetComponent<Button_pressed_animation> ().ui = this;
			temp.GetComponent<Button_pressed_animation> ().child.ui = this;
			temp.GetComponent<Button_pressed_animation> ().circle.ui = this;
			temp.GetComponent<Button_pressed_animation> ().animation_start (button_position_to_screen_position(input_button.transform),input_diameter,input_button);
		}
		
		
		public void change_state(UI_state input_state){
			
			
			prev_state = state;
			state = input_state;
		}
		
		public void change_state_skip_prev(UI_state input_state){
			state = input_state;
		}
		
		public Vector2 button_position_to_screen_position(Transform input_transform){
			
			Vector3 temp = world_position_to_screen_position (input_transform.position);
			
			return new Vector2 (temp.x,temp.y);
		}
		
		public Vector3 world_position_to_screen_position( Vector3 input_point){
			
			Vector3 temp=UI_camera.WorldToScreenPoint (input_point);
			
			return new Vector3((temp.x-(Screen.width/2)) *(640.0f/Screen.width),(temp.y-(Screen.height/2))*(640.0f/Screen.width),0);
		}
		
		public void refresh_UI_buttons_title(){
			
			UI_label_progress_level.GetComponent<UILabel>().text = main.save_data.get_level_progress ();
			UI_label_progress_dan.GetComponent<UILabel>().text = main.save_data.get_dan_progress ();
			
			if (main.save_data.is_dan_ready()) {
				
				UI_button_mode_dan.GetComponent<BoxCollider>().enabled=true;
				UI_button_mode_dan.GetComponent<UIWidget>().color= new Color(255,255,255,255);
				
			}
			
			
		}
		
		


	} 
}
