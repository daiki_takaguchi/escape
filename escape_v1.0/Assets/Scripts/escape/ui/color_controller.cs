﻿using UnityEngine;
using System.Collections;

namespace ESCAPE.UI{
public class color_controller : MonoBehaviour {

		// Use this for initialization
		public Ui ui;

		public UIWidget uiwidget;


		public enum color_type{
			base_color,
			white,
			red,
			button_unavailable,
			button_unavailable_label
		}

		public color_type color;

		void Start () {
			set_color ();
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void set_color(){

			if(ui==null){
				ui=GameObject.FindObjectOfType<Ui>();
			}

			Color temp= ui.main.settings.base_color;

			if (color == color_type.base_color) {
				temp= ui.main.settings.base_color;
			}else if(color==color_type.white){
				temp= ui.main.settings.white;
			}else if(color==color_type.red){
				temp= ui.main.settings.red;
			}else if(color==color_type.button_unavailable){
				temp= ui.main.settings.button_unavailable;
			}else if(color==color_type.button_unavailable_label){
				temp= ui.main.settings.button_unavailable_label;
			}

			uiwidget.color=temp;
		}
	}

}
