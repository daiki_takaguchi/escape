﻿using UnityEngine;
using System.Collections;
using ESCAPE.SYSTEM;

namespace ESCAPE.UI{
	public class Button_touch_panel : MonoBehaviour {


		/// </summary>

		public BoxCollider box;
		public Transform block_back;
		public Vector3 press_start_position;
		public Vector3 press_start_transform_position;
		private float bottom_limit=300.0f;
		private float top_limit=467.0f;
		public int max_block=10;
		public int block_height=64;
		
		void Start ()
		{

		}
		
		/// <summary>
		/// Create a plane on which we will be performing the dragging.
		/// </summary>


		public void enable_touch_panel(){
			box.enabled = true;
			block_back.localPosition = new Vector3 (0,0,0);
		}

		public void disable_touch_panel(){
			box.enabled = false;
		}

		void OnPress (bool input_pressed){

			if(input_pressed){
				press_start_position=Main.get_mouse_position();
				press_start_transform_position=block_back.localPosition;
			}


		}
		
		void OnDrag (){
			
			Vector3 difference = Main.get_mouse_position () - press_start_position;

			Vector3 next_position=press_start_transform_position+ new Vector3 (0,difference.y,0);;

			//if(Mathf.Abs(next_position.y)<block_height*max_block/2){

			if(next_position.y>Main.main_static.data.block_base_list.center_to_bottom_length){
				next_position= new Vector3(next_position.x,Main.main_static.data.block_base_list.center_to_bottom_length,next_position.z);

			}else if(next_position.y<-Main.main_static.data.block_base_list.center_to_top_length){
				next_position= new Vector3(next_position.x,-Main.main_static.data.block_base_list.center_to_top_length,next_position.z);

			}

			block_back.localPosition =next_position;
			
		}




	}
}

