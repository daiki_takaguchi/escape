﻿using UnityEngine;
using System.Collections;

namespace ESCAPE.UI{
public class Background_title_controller : MonoBehaviour {

	// Use this for initialization

	public UIPanel panel_background;
	public UIPanel panel_button;

	public Depth_controller background_depth;
	public Depth_controller button_mode_normal_depth;
	public Depth_controller button_mode_dan_depth;
	public Depth_controller button_mode_normal_label_depth;
	public Depth_controller button_mode_dan_label_depth;

	public Transform animation_position_background;
	public Transform animation_position_buttons;
	public Transform animation_position_buttons_label;


	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void initialize(){

			print ("initialized");
			animation_position_background.localPosition = new Vector3 (0, 0, 0);
			animation_position_buttons.localPosition = new Vector3 (0, 0, 0);
			animation_position_buttons_label.localPosition = new Vector3 (0, 0, 0);

	}

	public void change_layer(Depth_controller.depth_layer back_depth, Depth_controller.depth_layer button_depth,Depth_controller.depth_layer label_depth, int input_layer_no){
		background_depth.change_layer (back_depth,input_layer_no);
		button_mode_normal_depth.change_layer (button_depth,input_layer_no);
		button_mode_dan_depth.change_layer (button_depth,input_layer_no);
		button_mode_normal_label_depth.change_layer (label_depth,input_layer_no);
		button_mode_dan_label_depth.change_layer (label_depth,input_layer_no);
		
	}

	public void change_layer_back(){
		
		int temp_layer = gameObject.layer;

		
		change_layer(Depth_controller.depth_layer.Background_1,Depth_controller.depth_layer.Background_1_button,Depth_controller.depth_layer.Background_1_Label,temp_layer);

	}

	public void change_layer_front(){

		int temp_layer = gameObject.layer;
		
		

		change_layer(Depth_controller.depth_layer.Background_1_anim,Depth_controller.depth_layer.Background_1_button_anim,Depth_controller.depth_layer.Background_1_Label_anim,temp_layer);

	}

	}
}
