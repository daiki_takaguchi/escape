﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace ESCAPE.UI{
	public class UI_animation_loop : MonoBehaviour {

			// Use this for initialization

		public event EventHandler event_handler;

		public List<Transform> animation_transform_list;

		public bool is_animation_on;

		public float start_time;

		public float end_sec;


		public List<Vector3> start_position_list;

		public Vector3 difference;


		void Start () {

			
		}

		void Update () {

			if(is_animation_on){

				if(Time.time>start_time+end_sec){

					is_animation_on=false;

					animation_end();
				}else{
					move_transform();
				}

			}
			
		}

		public void move_transform(){
			for(int i =0;i<animation_transform_list.Count; i++){

				float temp=(Time.time-start_time)/end_sec;

				animation_transform_list[i].localPosition=start_position_list[i]+temp*difference;

			}
		}

		public void animation_start(List<Transform> input_transform_list,Vector3 input_difference,float input_sec){
			animation_transform_list = input_transform_list;
			difference = input_difference;
			end_sec = input_sec;
			start_time = Time.time;

			start_position_list = new List<Vector3> ();

			foreach(Transform temp in input_transform_list){
				start_position_list.Add(temp.localPosition);
			}

			is_animation_on = true;
			
		}



		private void animation_end(){
			if(event_handler!=null){
				event_handler(this,null);

			}
		}



	}
}