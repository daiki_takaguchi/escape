﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ESCAPE.DATA;

namespace ESCAPE.UI{
	public class Block_image_list : MonoBehaviour {

		public GameObject block_image_prefab;

		public List<Block_image> block_image_list;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

	

		public void initialize(){

			foreach (Block_image temp in block_image_list) {
				DestroyImmediate(temp.gameObject);
			}
			
			block_image_list.Clear ();

		}
		/*
		public void initialize_position(){
			
			foreach (Block_image temp in block_image_list) {
				temp.initialize_position();
			}

			
		}*/

		public Block_image add_block_image(){
			GameObject temp = (GameObject)Instantiate (block_image_prefab);

			temp.transform.parent = transform;
			temp.transform.localScale = new Vector3 (1,1,1);
			temp.SetActive (true);

			Block_image output = temp.GetComponent<Block_image> ();

			block_image_list.Add (output);

			return output;

		}


		/*
		public void add_block(Block input_block){
			GameObject temp =(GameObject) Instantiate (block_image_prefab);

			temp.transform.parent = transform;
			Block_image temp_image = temp.AddComponent<Block_image> ();

		}*/
	}
}
