﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace ESCAPE.UI{
	public class UI_animation_controller : MonoBehaviour {

			// Use this for initialization

		public Ui ui;

		//public Animator animator;

		//public RuntimeAnimatorController controller;

		public UI_animation_loop animation_loop;

		public Background_title_controller title;

		public enum animation_type{
			empty,
			normal_mode,
			dan_mode,
			play_level,
			play_dan,
			home

		}
		public animation_type type=animation_type.empty;
		public bool animate_by_script=true;


		//public bool is_animation_end=false;
		public bool is_animation_running = false;
		//public float animation_start_time;
		//public float animation_time=0.25f;
		//public bool is_animation_start=false;

		public Transform animation_position_background_title;
		public Transform animation_position_buttons_title;
		public Transform animation_position_buttons_title_label;
		public Transform animation_position_background_select_level;
		public Transform animation_position_background_select_level_label;
		public Transform animation_position_buttons_select_level;
		//public Transform animation_position_background_select_dan;
		public Transform animation_position_buttons_select_dan;
		public Transform animation_position_background_play;
		public Transform animation_position_background_play_label;
		public Transform animation_position_touch_panel;


			//public bool refresh_animator= false;

		/*
		public Transform[] title_animation_positions;
		public Transform[] select_level_animation_positions;
		public Transform[] play_animation_positions;*/


		void Start () {
			animation_loop.event_handler+=animation_end;
		
		}
		
		// Update is called once per frame
		void Update () {


			/*
			if (animate_by_script) {

				if(is_animation_start){
					animation_start_delay();
					is_animation_start=false;
				}

				if(is_animation_running){
					if(animation_time<Time.time-animation_start_time){

					}else{
						is_animation_running=false;
						is_animation_end=true;
					}
				}

				if (is_animation_end) {

					if(type==animation_type.home){
						
						title.change_layer_back();
						
					}

					adjust_end_position(type);
					
					ui.animation_end(type);
					
					type=animation_type.empty;
					
					is_animation_end=false;
					
				}

			
			}else{
			
				if(is_animation_start){
					animation_start_delay();
					is_animation_start=false;
				}


				//if(refresh_animator){
					

					animator = (Animator) gameObject.AddComponent<Animator> ();
					animator.runtimeAnimatorController = controller;
					animator.enabled=false;
					
					refresh_animator=false;
				//}



				if (is_animation_end) {



					if(type==animation_type.home){

						title.change_layer_back();

					}else if(type==animation_type.play_level||type==animation_type.play_dan){
						
						//refresh_animator=true;
					
					}

					ui.animation_end(type);
				
				

					//Destroy(animator);

					type=animation_type.empty;
					is_animation_end=false;

					//refresh_animator=true;

				}

			}*/



		}
		/*
		public void animation_end(){

			if(type!=animation_type.empty){
				is_animation_end = true;
			}

		}*/

		public void animation_start(animation_type input_type){
			if(!is_animation_running){
				if(animate_by_script){

					type=input_type;


					adjust_start_position(type);

					Vector3 temp= new Vector3(0,1136,0);


					animation_loop.animation_start(get_transform_list(input_type),temp,0.20f);



					

				}else{

				}

			/*
			animator.enabled = true;

			if(type==animation_type.normal_mode||type==animation_type.dan_mode){
				animator.Play ("background-select_level_slide_anim");

			}else if(type==animation_type.play_level||type==animation_type.play_dan){
				
			
				animator.Play ("background-play_slide_anim");



			}else if(type==animation_type.home){

							title.change_layer_front();
				animator.Play ("background-title_slide_anim");

			}*/
			}

		}

		public void animation_end(object sender, EventArgs c){


		

			ui.animation_end(type);

			is_animation_running = false;

			adjust_end_position (type);

		}

		public List<Transform> get_transform_list(animation_type input_type){

			List<Transform> output = new List<Transform> ();

			
			if (input_type == animation_type.normal_mode) {
				//animation_position_background_title.localPosition=new Vector3(0,0,0);
				///select_animation_transform.localPosition=new Vector3(0,0,0);
				//select_level_button_animation_transform.localPosition=new Vector3(0,0,0);

				output.Add (animation_position_background_select_level);
				output.Add (animation_position_background_select_level_label);
				output.Add (animation_position_buttons_select_level);

			}else if (input_type == animation_type.dan_mode) {

					
				output.Add (animation_position_background_select_level);
				output.Add (animation_position_background_select_level_label);
				output.Add (animation_position_buttons_select_dan);

			}else if(input_type==animation_type.play_level){
				output.Add (animation_position_touch_panel);
				output.Add (animation_position_background_play);
				output.Add (animation_position_background_play_label);

			

			}else if(input_type==animation_type.home){

				output.Add (animation_position_background_title);
				output.Add (animation_position_buttons_title);
				output.Add (animation_position_buttons_title_label);
			}



			return output;
			
		}

		public void adjust_start_position(animation_type input_type){
			
			print ("initializing position");
			
			if(input_type==animation_type.normal_mode){
				//animation_position_background_title.localPosition=new Vector3(0,0,0);
				animation_position_background_select_level.localPosition=new Vector3(0,-1136,0);
				animation_position_buttons_select_level.localPosition=new Vector3(0,-1136,0);
				animation_position_background_select_level_label.localPosition=new Vector3(0,-1136,0);

			}else if (input_type == animation_type.dan_mode) {
				
				animation_position_background_select_level.localPosition=new Vector3(0,-1136,0);
				animation_position_buttons_select_dan.localPosition=new Vector3(0,-1136,0);
				animation_position_background_select_level_label.localPosition=new Vector3(0,-1136,0);
				
			}else if(input_type==animation_type.play_level){

				animation_position_touch_panel.localPosition=new Vector3(0,-1136,0);
				animation_position_background_play.localPosition=new Vector3(0,-1136,0);
				animation_position_background_play_label.localPosition=new Vector3(0,-1136,0);
			}else if(input_type==animation_type.home){
				title.change_layer_front();
				animation_position_background_title.localPosition=new Vector3(0,-1136,0);
				animation_position_buttons_title.localPosition=new Vector3(0,-1136,0);
				animation_position_buttons_title_label.localPosition=new Vector3(0,-1136,0);
			}
			
		}


		public void adjust_end_position(animation_type input_type){

			print ("initializing position");

			if(input_type==animation_type.normal_mode){
				//animation_position_background_title.localPosition=new Vector3(0,0,0);
				animation_position_background_select_level.localPosition=new Vector3(0,0,0);
				animation_position_buttons_select_level.localPosition=new Vector3(0,0,0);
				animation_position_background_select_level_label.localPosition=new Vector3(0,0,0);

			}else if (input_type == animation_type.dan_mode) {
				
				animation_position_background_select_level.localPosition=new Vector3(0,0,0);
				animation_position_buttons_select_dan.localPosition=new Vector3(0,0,0);
				animation_position_background_select_level_label.localPosition=new Vector3(0,0,0);

			}else if(input_type==animation_type.play_level){
				animation_position_background_play.localPosition=new Vector3(0,0,0);
				animation_position_touch_panel.localPosition=new Vector3(0,0,0);
				animation_position_background_play_label.localPosition=new Vector3(0,0,0);
			}else if(input_type==animation_type.play_dan){
				animation_position_background_play.localPosition=new Vector3(0,0,0);
				animation_position_touch_panel.localPosition=new Vector3(0,0,0);
				animation_position_background_play_label.localPosition=new Vector3(0,0,0);

			}else if(input_type==animation_type.home){
				title.change_layer_back();
				animation_position_background_title.localPosition=new Vector3(0,0,0);
				animation_position_buttons_title.localPosition=new Vector3(0,0,0);
				animation_position_buttons_title_label.localPosition=new Vector3(0,0,0);
			

			}

		}



	}
}
