﻿using UnityEngine;
using System.Collections;

namespace ESCAPE.UI{

	[ExecuteInEditMode]
	public class Depth_controller : MonoBehaviour {

		// Use this for initialization

		

		public enum depth_layer{
			Background_1,
			Background_2,
			Background_3,
			Point,
			Line,
			Select_button,
			Select_button_Label,
			Button,
			Window_1,
			Window_2,
			Window_3,
			Window_4,
			Window_5,
			Window_6,
			Window_7,
			Background_1_button,
			Background_1_Label,
			dragged_line,
			Background_1_anim,
			Background_1_button_anim,
			Background_1_Label_anim,
			Line_label_back,
			Line_label,
			Window_clear,
			Window_clear_label,
			label_prerender,
			Button_pressed_animation,
			Background_2_sliced,
			Background_2_label,
			Point_answer,
			Point_answer_label,
			Line_arrow,
			Background_2_sliced_back,
			Window_tutorial,
			Window_tutorial_close,
			Ad,
			Block,
			Block_base,
			Block_base_item,
			Block_item,
			Block_back,
			Block_selected,
			Block_item_selected,
			Block_back_selected,
			Background_3_sliced,

		};

		private depth_layer[] layer_order = new depth_layer[]{
			depth_layer.label_prerender,
			depth_layer.Background_1,
			depth_layer.Background_1_button,
			depth_layer.Background_1_Label,
			depth_layer.Background_2,
			depth_layer.Select_button,
			depth_layer.Select_button_Label,

			depth_layer.Background_3,
			depth_layer.Block_back,
			depth_layer.Block_base,
			depth_layer.Block,
			depth_layer.Block_item,
			depth_layer.Block_back_selected,
			depth_layer.Block_selected,
			depth_layer.Block_item_selected,
			depth_layer.Block_base_item,
			depth_layer.Background_3_sliced,
			depth_layer.Background_2_label,
			depth_layer.Background_2_sliced_back,
			depth_layer.Background_2_sliced,
			depth_layer.Line,
			depth_layer.Line_arrow,
			depth_layer.Line_label_back,
			depth_layer.Line_label,
			depth_layer.Point,
			depth_layer.dragged_line,
			depth_layer.Point_answer,
			depth_layer.Point_answer_label,
			depth_layer.Button,
			depth_layer.Window_1,
			depth_layer.Window_2,
			depth_layer.Window_3,
			depth_layer.Window_4,
			depth_layer.Window_5,
			depth_layer.Window_6,
			depth_layer.Window_7,
			depth_layer.Window_clear,
			depth_layer.Window_clear_label,
			depth_layer.Background_1_anim,
			depth_layer.Background_1_button_anim,
			depth_layer.Background_1_Label_anim,
			depth_layer.Button_pressed_animation,
			depth_layer.Window_tutorial,
			depth_layer.Window_tutorial_close,
			depth_layer.Ad
		};

		public depth_layer depth;
		
		[HideInInspector] public depth_layer prev_depth;

		[HideInInspector] public string prev_depth_string;




		void Start () {




			//print (gameObject.layer);

			change_layer (depth,gameObject.layer);

		}
		
		// Update is called once per frame
		void Update () {

			if(!Application.isPlaying){
					//prev_depth=depth;
					//prev_depth_string=prev_depth.ToString();
				

				if(prev_depth!=depth){

					if(prev_depth_string==depth.ToString()){

						depth=(depth_layer) System.Enum.Parse(typeof(depth_layer),prev_depth_string);
						
						prev_depth=depth;
						prev_depth_string=prev_depth.ToString();
					}
				}

			}
		
		}

		public void change_layer(depth_layer input_depth){
			change_layer (input_depth,gameObject.layer);
		}

		public void change_layer(depth_layer input_depth,int input_layer_no){
			depth= input_depth;
			Vector3 temp= transform.localPosition;
			transform.localPosition = new Vector3 (temp.x,temp.y,-layer_to_z(depth));

			gameObject.layer = input_layer_no;

			if(GetComponent<UIWidget> ()!=null){
				GetComponent<UIWidget> ().depth = layer_to_z(depth);

			}

			/*
			if(input_depth==depth_layer.Background_1_anim||
			   input_depth==depth_layer.Background_1_button_anim||
			   input_depth==depth_layer.Background_1_Label_anim){

			}*/

		}

		public int layer_to_z(depth_layer input_layer){

			int output = 0;

			for(int i=0;i<layer_order.Length;i++) {

				//print (layer_order[i]);
				if(layer_order[i]==input_layer){
					output=i;
				}
			}

		



			return output;

		}

		
	
	}
}



