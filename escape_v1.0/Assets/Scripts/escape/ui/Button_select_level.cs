﻿using UnityEngine;
using System.Collections;
using ESCAPE.SYSTEM;

namespace ESCAPE.UI{
	public class Button_select_level : MonoBehaviour {

		// Use this for initialization

		public int level;
		public Const.Ui.Level_state state=Const.Ui.Level_state.Locked;

		public Button_select_level parent;
		public Button button;

		public bool is_dan=false;

		public UILabel uilabel;
		public string label_text=" ";

		public BoxCollider box;

		public GameObject background;
		public GameObject label;

		public GameObject locked ;
		public GameObject zero ;
		public GameObject one ;
		public GameObject two ;
		public GameObject three ;


		public UIDraggablePanel draggablePanel;


		public Ui ui;

		public custom_scroll scroll;

		public color_controller color;

		public bool is_shown_again=false;

		public float press_started_time;
		public Vector2 press_started_position;

		[ExecuteInEditMode]


		/// <summary>
		/// This panel's contents will be dragged by the script.
		/// </summary>
		
		
		
		// Version 1.92 and earlier referenced the panel instead of UIDraggablePanel script.
		[HideInInspector][SerializeField] UIPanel panel;
		
		/// <summary>
		/// Backwards compatibility.
		/// </summary>
		
		void Awake ()
		{
			// Legacy functionality support for backwards compatibility
			if (panel != null)
			{
				if (draggablePanel == null)
				{
					draggablePanel = panel.GetComponent<UIDraggablePanel>();
					
					if (draggablePanel == null)
					{
						draggablePanel = panel.gameObject.AddComponent<UIDraggablePanel>();
					}
				}
				panel = null;
			}
		}
		
		/// <summary>
		/// Automatically find the draggable panel if possible.
		/// </summary>
		
		void Start ()
		{
			if (draggablePanel == null)
			{
				draggablePanel = NGUITools.FindInParents<UIDraggablePanel>(gameObject);
			}
		}
		
		/// <summary>
		/// Create a plane on which we will be performing the dragging.
		/// </summary>

		void OnClick(){

			print ("clicked");

			if (Time.time <= press_started_time + parent.ui.main.settings.button_select_level_click_detection_limit_time) {
				if (Mathf.Abs (get_touched_position ().y - press_started_position.y) < parent.ui.main.settings.button_select_level_click_detection_limit_pixel) {

					button_clicked ();

				}else{
					print ("dragged");
				}
			} else {
				print ("too long time");
			}
		}

		public void button_clicked(){
			if(parent==null){

				if(state!=Const.Ui.Level_state.Locked){

					//print ("aaaaaaaaaaaaaa");

					if(is_dan){

						button.type=Button.button_type.button_select_dan;
						button.level=level;
						button.clicked(ui);
						//ui.button_select_dan_pressed(level,transform);
					}else{

						button.type=Button.button_type.button_select_level;
						button.level=level;
						button.clicked(ui);

						//ui.button_select_level_pressed(level,transform);
					}
				}
			}else{

				//print ("bbbbbbbbbbbbbbb");

				parent.button_clicked();
			}
		}

		void OnPress (bool pressed)
		{
			//print ("pressed");

			
			if(pressed){
				press_started_time=Time.time;
				press_started_position=get_touched_position();
			}


			button_pressed(pressed);


		
		}
			
		public void button_pressed(bool pressed){
			if(parent==null){
				//draggablePanel.Press(pressed);

				scroll.pressed (pressed);

			}else{
				parent.button_pressed(pressed);
			}
		}
	

	/// <summary>
	/// Drag the object along the plane.
	/// </summary>
	
		void OnDrag (Vector2 delta)
		{

			button_dragged(delta);


		}

		public void button_dragged(Vector2 delta){
			if(parent==null){


			}else{
				parent.button_dragged(delta);
			}
		}
	
	/// <summary>
	/// If the object should support the scroll wheel, do it.
	/// </summary>
	
		void OnScroll (float delta)
		{
			if (enabled && NGUITools.GetActive(gameObject) && draggablePanel != null)
			{

				print ("scrolled");
				//draggablePanel.Scroll(delta);
			}
		}


		public void change_label(int input_level,string input_label, Const.Ui.Level_state input_state ,bool input_is_dan){
			is_dan = input_is_dan;
			level = input_level;
			button.level = level;

			if (uilabel != null) {
				uilabel.text=input_label;
				label_text=input_label;
			}



		}

		public void set_state(Save_data save_data){

			if (ui.main.settings.is_test) {
				state =Const.Ui.Level_state.Three;
			}else{
				if(is_dan){
					state = save_data.get_select_dan_data (level);
				//	print(state);
				}else{
					state = save_data.get_select_level_data (level);
				//	print(state);
				}
			}

			//uilabel.text = label_text;
			
			locked.SetActive(false);
			zero.SetActive(false);
			one.SetActive(false);
			two.SetActive(false);
			three.SetActive(false);
			
			if(state==Const.Ui.Level_state.Locked){
				locked.SetActive(true);
			}else if(state==Const.Ui.Level_state.One){
				one.SetActive(true);
			}else if(state==Const.Ui.Level_state.Two){
				two.SetActive(true);
			}else if(state==Const.Ui.Level_state.Three){
				three.SetActive(true);
			}

			label.GetComponent<Depth_controller>().change_layer(Depth_controller.depth_layer.Select_button_Label,label.layer);



		}
		/*
		public void show_button(){
			background.SetActive (true);

			//label.SetActive (true);
			//box.enabled = true;

			if(!is_shown_again){
				label.GetComponent<Depth_controller>().change_layer(Depth_controller.depth_layer.Select_button_Label,label.layer);
				is_shown_again=true;
			}

			uilabel.text = label_text;

			locked.SetActive(false);
			zero.SetActive(false);
			one.SetActive(false);
			two.SetActive(false);
			three.SetActive(false);

			if(state==Const.Ui.Level_state.Locked){
				locked.SetActive(true);
			}else if(state==Const.Ui.Level_state.One){
				one.SetActive(true);
			}else if(state==Const.Ui.Level_state.Two){
				two.SetActive(true);
			}else if(state==Const.Ui.Level_state.Three){
				three.SetActive(true);
			}

		}*/

		public void enable_button(){
			box.enabled = true;
		}

		public void disable_button(){
			box.enabled = false;
		}

		public void hide_button(){
			background.SetActive (false);
			//label.SetActive (false);
			box.enabled = false;
			uilabel.text="";
			
			locked.SetActive(false);
			zero.SetActive(false);
			one.SetActive(false);
			two.SetActive(false);
			three.SetActive(false);

		}

		public Vector2 get_touched_position(){


			Vector2 input_position=  new Vector2 (Input.mousePosition.x,Input.mousePosition.y);

			if (Input.touchCount>0) {
				input_position=  Input.GetTouch(0).position;
			} 

			
			return screen_position_to_local_position(input_position);;
		}

		public Vector2 screen_position_to_local_position(Vector2 input_position){
			
			return new Vector2 ((input_position.x - (Screen.width / 2)) * (640.0f / Screen.width), (input_position.y - (Screen.height / 2)) * (640.0f / Screen.width));
		}

	}
}

