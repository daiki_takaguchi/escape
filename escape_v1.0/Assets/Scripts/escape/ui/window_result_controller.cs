﻿using UnityEngine;
using System.Collections;
using ESCAPE.SYSTEM;

public class window_result_controller : MonoBehaviour {

	// Use this for initialization

	public GameObject good;
	public GameObject fantastic;
	public GameObject perfect;



	void Start () {
		//good.SetActive (false);
		//fantastic.SetActive (false);
		//perfect.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void show_result(Const.Data.Game_result input_result){

		print ("show_result:"+ input_result);

		if (input_result == Const.Data.Game_result.good) {
			good.SetActive (true);
			fantastic.SetActive (false);
			perfect.SetActive (false);
		}else if(input_result == Const.Data.Game_result.fantastic) {
			good.SetActive (false);
			fantastic.SetActive (true);
			perfect.SetActive (false);
		}else if(input_result == Const.Data.Game_result.perfect){
			good.SetActive (false);
			fantastic.SetActive (false);
			perfect.SetActive (true);
		}

	}

	public void hide_result(){
		good.SetActive (false);
		fantastic.SetActive (false);
		perfect.SetActive (false);
	}
}
