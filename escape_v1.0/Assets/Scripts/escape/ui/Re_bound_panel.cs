﻿using UnityEngine;
using System.Collections;

namespace ESCAPE.UI{
	public class Re_bound_panel : MonoBehaviour {

		public Ui ui;

		// Use this for initialization
		void Start () {

			reclip ();
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void reclip(){
			float y_top = 320.0f;
			float y_bottom= ui.main.settings.get_panel_level_offset_y_bottom();


			
			GetComponent<UIPanel> ().clipRange=new Vector4 (0,((y_top+y_bottom)/2.0f)*Screen.width / 640.0f, Screen.width ,(y_top-y_bottom)*Screen.width / 640.0f);

			//GetComponent<UIDraggablePanel> ().refresh_should_move ();

		}
	}
}
