﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ESCAPE.SYSTEM;
using ESCAPE.UI;


namespace ESCAPE.DATA{
public class Block : MonoBehaviour {


		public Block_group current_group;

		public Block_base initial_block_base;

		public Block_base current_block_base;

		public Block_base next_block_base;

		public Block_image current_block_image;

		//public List<Block> neighbours;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void initialize(Block_group input_group,Block_base input_base){
			current_group = input_group;

			initial_block_base = input_base;



		}

		public void reset(){
			/*
			if(current_block_base!=null){
				current_block_base.current_block = null;
			}*/

			current_block_base = initial_block_base;
			current_block_base.set_current_block(this);

			if(current_block_image==null){
				current_block_image=Main.main_static.block_image_list.add_block_image ();
			}
			current_block_image.reset (this);

		}
		/*
		public void initialize_image(){


			if(current_block_image==null){
				current_block_image=Main.main_static.block_image_list.add_block_image ();
			}

			//current_block_image.initialize (this);

		}*/
		/*
		public bool has_neighbour(Const.Data.Block_direction input_direction){
		
			bool output = false;

			int slot = Const.Data.direction_to_int (input_direction);
		
			if(current_block_base.neighbours[slot]!=null){
				output=true;
			}

			return output;
		}*/

		public Block_base get_neighbour(Const.Data.Block_direction input_direction){

			/*
			Block_base output =null;
			
			int slot = Const.Data.direction_to_int (input_direction);
			
			if(current_block_base.neighbours[slot]!=null){
				output=current_block_base.neighbours[slot];
			}*/
			
			return current_block_base.get_neighbour(input_direction);
		}
		/*
		public void move(Const.Data.Block_direction input_direction){

			current_block_base.current_block = null;
			current_block_base = current_block_base.get_neighbour (input_direction);
			current_block_image.initialize_position (current_block_base);

		}*/

		public void move_end_prepare(Const.Data.Block_direction input_direction){
			next_block_base=current_block_base.get_neighbour (input_direction);
		}

		public void move_end(){
			current_block_base = next_block_base;
			next_block_base = null;
			current_block_image.reposition (current_block_base);
		}

		void OnDestroy(){

			if(current_block_image!=null){
				Destroy (current_block_image.gameObject);
			}
		}


	
	}
}