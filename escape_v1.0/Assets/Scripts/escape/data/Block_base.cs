﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ESCAPE.SYSTEM;
using ESCAPE.UI;


namespace ESCAPE.DATA{
public class Block_base : MonoBehaviour {

		public Const.Data.Block_base_type block_base_type;
		public Const.Data.Block_base_state block_base_state;

		public int position_x;
		 
		public int position_y;

		public Block_image current_block_image;

		public List<Block_base> neighbours;

		[SerializeField] private Block current_block;



		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void initialize(int input_x,int input_y,List<Block_base> input_neigbours){
			position_x = input_x;
			position_y = input_y;

			neighbours = input_neigbours;
			name = "x:"+input_x +"| y:" +input_y;

		}

		public void initialize_image(Const.Data.Block_base_type input_block_base_type ,Const.Data.Block_base_state input_block_base_state){


			block_base_type = input_block_base_type;
			block_base_state = input_block_base_state;
			
			if (current_block_image == null) {
				current_block_image = Main.main_static.block_image_list.add_block_image ();
			}
			
			current_block_image.reset (this);
		}

		public void reposition(){
			if (current_block_image != null) {
				current_block_image.reposition(this);
			}
		}

		public void reset(){

			if(current_block!=null){
				current_block.current_block_base=null;
			}
			current_block=null;
		}

		public void set_current_block(Block input_block){
			current_block=input_block;
		}

		public void set_locked_gate(Const.Data.Block_base_state input_state){
			block_base_state = input_state;
			current_block_image.change_gate_image ();
		}


		public Block_base get_neighbour(Const.Data.Block_direction input_direction){
			int slot = Const.Data.direction_to_int (input_direction);

			return neighbours [slot];
		}

		public bool is_empty(Block_group input_group){
			bool output = true;

			if(current_block!=null){
				if(current_block.current_group!=input_group){

					print ("a block is on "+name);
					output=false;

				}
			}

			if(block_base_state==Const.Data.Block_base_state.non_empty){
				print (name+" is non empty");
				output=false;
			}

			/*
			if(block_base_type==Const.Data.Block_base_type.gate||
			   block_base_type==Const.Data.Block_base_type.locked_gate){

				if(input_group.block_group_type!=Const.Data.Block_group_type.human){
					output=false;
				}
			}*/

			return output;
		}

		public bool has_human(){
			
			bool output = false;
			
			if(current_block!=null){
				if(current_block.current_group.block_group_type==Const.Data.Block_group_type.human){
					output=true;
				}

			}
			
			return output;
		}

		public bool has_key(){
			bool output=false;

			if(current_block!=null){
				if(current_block.current_group.block_group_type==Const.Data.Block_group_type.key){
					output=true;
				}
				
			}

			return output;

		}

		public bool has_block(){
			bool output=false;
			
			if(current_block!=null){

				output=true;
				
			}
			
			return output;
			
		}

		public bool is_killer_in_neighborhood(){

			bool output = false;
			
			foreach(Block_base temp in neighbours){
				if(temp!=null){

					if(temp.current_block!=null){
						if(temp.current_block.current_group.block_group_type==Const.Data.Block_group_type.killer){
							output=true;
						}
					}

				}
			}
			
			return output;
		}

		public Const.Data.Block_direction is_selected_group_in_neighborhood(){
			
			Const.Data.Block_direction output = Const.Data.Block_direction.none;

			int count = 0;
			
			for(int i=0;i<4;i++){


				if(neighbours[i].current_block!=null){
					if(neighbours[i].current_block.current_group.block_group_state==Const.Data.Block_group_state.selected){
						output =Const.Data.int_to_direction(i);
						count++;
					}

				}
			}

			if(count>1){
				output = Const.Data.Block_direction.none;
			}

			
			return output;
		}




	}
}