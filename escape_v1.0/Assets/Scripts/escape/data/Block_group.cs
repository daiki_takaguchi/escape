using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ESCAPE.SYSTEM;
using ESCAPE.UI;

namespace ESCAPE.DATA{
public class Block_group: MonoBehaviour {



		public int group_no;

		public Const.Data.Block_group_type block_group_type;
		public Const.Data.Block_group_state  block_group_state;

		
		public int initial_count;
		public int current_count;


		public List<Block> block_list = new List<Block>();

		public UI_animation_loop animation_loop;

		//public Const.Data.Block_direction move_direction;

	
		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public Block add_block(Block_base input_base){

			print ("creating block");

			GameObject temp= new GameObject ();
			Block output = temp.AddComponent<Block> ();
			output.initialize (this, input_base);

			output.name = "x:"+input_base.position_x +"| y:" +input_base.position_y;

			output.transform.parent = transform;

			block_list.Add (output);

			return output;

		}

		public void set_group_type(Const.Data.Block_group_type input_type){
			
			block_group_type = input_type;
				
		}

		public void initialize_count(int input_int){

			initial_count = input_int;
			current_count = input_int;

		}




		public void change_state(Const.Data.Block_group_state input_state){
			if(block_group_state==Const.Data.Block_group_state.none){

				if(input_state==Const.Data.Block_group_state.selected){

					block_group_state=input_state;

					foreach(Block one_block in block_list){


						one_block.current_block_image.change_block_image();
					}

					Main.main_static.data.block_group_list.selected_group=this;
				}
			}else if(block_group_state==Const.Data.Block_group_state.selected){
				if(input_state==Const.Data.Block_group_state.none){
					block_group_state=input_state;

					foreach(Block one_block in block_list){

						one_block.current_block_image.change_block_image();
					}

				}

			}
		}

		public void reset(){
			current_count = initial_count;
			block_group_state = Const.Data.Block_group_state.none;

			foreach (Block temp in block_list) {
				temp.reset ();
			}


		}



		/*
		public bool move_if_movable(Const.Data.Block_direction input_direction){

			bool output = check_movability(input_direction);


			return output;

		}*/

		public void move_start(Const.Data.Block_direction input_direction){
		
		}

		public void move_end(Const.Data.Block_direction input_direction){


			foreach(Block one_block in block_list){

				one_block.move_end_prepare(input_direction);
				one_block.current_block_base.set_current_block(null);
			}

			foreach (Block one_block in block_list) {
				one_block.move_end();
				one_block.current_block_base.set_current_block(one_block);

			}



			check_after_move ();

			
		}

		public void check_after_move(){



			if(block_group_type==Const.Data.Block_group_type.number){
				if(Main.main_static.game_state_manager.pressed_button==Button.button_type.block_image){
					current_count--;
					foreach(Block one_block in block_list){

						one_block.current_block_image.change_count_image();
					}

				}else if(Main.main_static.game_state_manager.pressed_button==Button.button_type.button_undo){
					current_count++;
					foreach(Block one_block in block_list){
						
						one_block.current_block_image.change_count_image();
					}

				}

			}else if(block_group_type==Const.Data.Block_group_type.human){
				//Block_group human= Main.main_static.data.block_group_list.human;

				bool is_killer_found=false;
				bool is_game_clear=false;

				foreach(Block temp in block_list){
					is_killer_found=is_killer_found||temp.current_block_base.is_killer_in_neighborhood();
					is_game_clear=is_game_clear||(temp.current_block_base.block_base_type==Const.Data.Block_base_type.gate);
					is_game_clear=is_game_clear||((temp.current_block_base.block_base_type==Const.Data.Block_base_type.locked_gate)&&(temp.current_block_base.block_base_state==Const.Data.Block_base_state.empty));
				
				}

				if(is_killer_found){
					Main.main_static.game_state_manager.game_over();
				}else{
					if(is_game_clear){
						Main.main_static.game_state_manager.game_clear();
					}
				}

			}else if(block_group_type==Const.Data.Block_group_type.killer){
				Block_group human= Main.main_static.data.block_group_list.human;
				
				bool is_killer_found=false;
			
				
				foreach(Block temp in human.block_list){
					is_killer_found=is_killer_found||temp.current_block_base.is_killer_in_neighborhood();
		
				}
				
				if(is_killer_found){
					Main.main_static.game_state_manager.game_over();
				
				}

			}else if(block_group_type==Const.Data.Block_group_type.key){


				bool is_lock_open=true;

				foreach(Block_base temp in Main.main_static.data.block_base_list.lock_base_list){

					if(!temp.has_key()){
	
						is_lock_open=false;
					}

				}

				if(is_lock_open){

					bool is_game_clear=false;

					foreach(Block_base temp in Main.main_static.data.block_base_list.locked_gate_list){
						temp.set_locked_gate(Const.Data.Block_base_state.empty);

						is_game_clear=is_game_clear||temp.has_human();
					}

					if(is_game_clear){
						Main.main_static.game_state_manager.game_clear();
					}



				}else{
					foreach(Block_base temp in Main.main_static.data.block_base_list.locked_gate_list){
						temp.set_locked_gate(Const.Data.Block_base_state.gate_closed);
					}
				}
			}


		
		}




		public bool check_movability(Const.Data.Block_direction input_direction){

			bool output = true;

			if(block_group_type==Const.Data.Block_group_type.number){
				if(current_count==0){
					output=false;
				}
			}

			if(input_direction==null){
				output=false;
			}

			if(output){

				foreach(Block one_block in block_list){
				
					Block_base temp= one_block.get_neighbour(input_direction);

					if(temp!=null){


						if(!temp.is_empty(this)){


							print("move faild: neighbour is non empty");

							output=false;
						}


					}else{

						print("move faild: no neighbour");

						output=false;
					}
						
					
				}
			}



			return output;

		}
	}
}