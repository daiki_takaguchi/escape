﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ESCAPE.SYSTEM;


namespace ESCAPE.DATA{
public class Block_base_list : MonoBehaviour {

		public List<Block_base> block_base_list;
		public List<Block_base> lock_base_list;
		public List<Block_base> locked_gate_list;

		public List<bool> is_wall_line_list= new List<bool>();

		public int max_x;

		public int max_y;

		public float top;

		public float bottom;

		public float block_base_center_x;

		public float block_base_center_y;

		public float center_to_top_length;

		public float center_to_bottom_length;


		// Use this for initialization
		void Start () { 
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void initialize(int input_max_x, int input_max_y){


			foreach (Block_base temp in block_base_list) {
				if(temp!=null){
					DestroyImmediate(temp.gameObject);
				}
					
			}

			block_base_list.Clear ();
			lock_base_list.Clear ();
			locked_gate_list.Clear ();

			is_wall_line_list.Clear();



			max_x = input_max_x;
			max_y = input_max_y;

			for (int y=0;y<max_y;y++){
				for (int x=0;x<max_x;x++){

					GameObject temp= new GameObject();
					temp.transform.parent=transform;
					Block_base temp_block_base= temp.AddComponent<Block_base>();

					block_base_list.Add (temp_block_base);

				}
			}


			for (int y=0;y<max_y;y++){
				for (int x=0;x<max_x;x++){
					Block_base temp_block_base=get_block_base(x,y);

					List<Block_base> temp_list= new List<Block_base>();

					temp_list.Add (get_block_base(x-1,y));
					temp_list.Add (get_block_base(x+1,y));
					temp_list.Add (get_block_base(x,y-1));
					temp_list.Add (get_block_base(x,y+1));

					temp_block_base.initialize(x,y,temp_list);

				}
			}


		}


		public int non_wall_line_from_top(){

			int output=0;

			for (int y=0;y<max_y;y++){

				if(!is_wall_line_list[y]){
					break;
				}

				output++;
			}

			return output;

		}

		public int non_wall_line_from_bottom(){
			
			int output=0;


			
			for (int y=max_y-1;y>=0;y--){

				if(!is_wall_line_list[y]){
					break;
				}

				output++;
			}
			
			
			return output;
		}


		public Block_base get_block_base(int x, int y){

			Block_base output = null;

			if(x>=0&&x<max_x){
				if(y>=0&&y<max_y){
					output=block_base_list[x+y*max_x];
				}
			}
		
			return output;

		}

		public void calculate_bound(){

			block_base_center_x=64*3.5f;

			top= non_wall_line_from_top();
			bottom= non_wall_line_from_bottom();

			//int total_length=(max_y-top-bottom)*64.0f;

			float center=(top+max_y-bottom)/2.0f;

			block_base_center_y=center*64.0f;

			if(block_base_center_y>256){
				block_base_center_y=256;
			}

			center_to_top_length=block_base_center_y-top*64.0f;

			float total_length=(max_y-top-bottom)*64.0f;

			center_to_bottom_length=total_length- center_to_top_length;
		

		}

		public void reposition(){
			foreach(Block_base temp in block_base_list){
				temp.reposition();
			}

		}

		public void reset(){
			
			foreach(Block_base temp in block_base_list){
				temp.reset();
			}
			
			
			//moved_direction_list.Clear();
			//moved_group_list.Clear();
			
		}

		/*
		public float get_block_base_center_x(){
			return ;
		}

		public float get_block_base_center_y(){
			return 256;
		}*/




	}
}