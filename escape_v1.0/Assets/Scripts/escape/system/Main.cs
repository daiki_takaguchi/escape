﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ESCAPE.DATA;
using ESCAPE.UI;


namespace ESCAPE.SYSTEM{

	public class Main : MonoBehaviour {

		public static Main main_static;

		public Data data;

		public Save_data save_data;

		public Loader loader;

		public Settings settings;

		public System_button system_button;

		public Ui ui;

		public Game_state_manager game_state_manager;

		public Block_image_list block_image_list;
		public GameObject block_image_prefab;





		void Start () {

			if(Main.main_static==null){
				Main.main_static=this;
			}
		
		}
		
		// Update is called once per frame
		void Update () {
			if (Input.GetMouseButtonDown(0)) 
			{
				//RaycastHit2D hit = Physics2D.Raycast(Ui.UI_camera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

				Ray ray = ui.UI_camera.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if(Physics.Raycast(ray,out hit))
				{
					print ("raycasting... clicked:" +hit.collider.gameObject.name);
				}
			}
		
		}

		public static Vector3 get_mouse_position(){
			Vector3 temp= Input.mousePosition;
			temp.x = temp.x* 640 / Screen.width;
			temp.y= temp.y*640 / Screen.width;

			return temp;
		}



	}
}
