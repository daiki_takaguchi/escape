﻿using UnityEngine;
using System.Collections;

namespace ESCAPE.UI{
	public class button_pressed_animation_controller : MonoBehaviour {

		// Use this for initialization

		public Ui ui;
		public button_pressed_animation_controller parent;
		public button_pressed_animation_controller child;
		public color_controller circle;
		public Animator animator;
		public ngui_animation animation;
		public UITexture texture;

		public bool is_child;

		public bool animation_next_bool = false;
		public bool animation_end_bool=false;


		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {

			if(animation_next_bool){
				animation_next_update();
				animation_next_bool=false;
			}

			if(animation_end_bool){
				animation_end_update();
				animation_end_bool=false;
			}

		}

		public void animation_start(Vector2 input_position, int input_diameter,button_controller button){

			if(!is_child){
				Vector3 temp = transform.localPosition;

				transform.localScale = new Vector3 (input_diameter,input_diameter,1);
				transform.localPosition = new Vector3 (input_position.x,input_position.y,temp.z);
				texture.mainTexture=animation.get_first_sprite ();

				animator.enabled=true;

			}
			
		}



		public void animation_end(){
		
			animation_end_bool = true;
		}

		public void animation_end_update(){

			if(is_child&&parent!=null){

				texture.mainTexture=animation.get_first_sprite ();
				Destroy (parent.gameObject);
			}
			
		}

		public void animation_next(){
			
			animation_next_bool = true;
		}


		public void animation_next_update(){
			texture.mainTexture = animation.get_next_sprite ();
		}
		

	}
}
