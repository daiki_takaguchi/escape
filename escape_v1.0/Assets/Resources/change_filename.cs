﻿using UnityEngine;
using System.Collections;
using System;

[ExecuteInEditMode]
public class change_filename : MonoBehaviour {

	// Use this for initialization

	public int from;
	public int to;

	public bool convert_now;

	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		if(!Application.isPlaying){
			if(convert_now){

				for (int i=from; i<=to;i++ ){
					string path=Application.dataPath+"/Resources/escape_data/hako"+i.ToString ("000");
					print(path);
					System.IO.File.Move(path+".dat", path+".txt");
				}

				convert_now=false;
			}
		}
	
	}
}
