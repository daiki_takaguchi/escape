﻿
using System;
using System.Collections;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using System.Runtime.InteropServices;
using ImobileSdkAdsUnityPluginUtility;


public class ad_controller : MonoBehaviour {



	public string iOS_key_admob_banner="ca-app-pub-7812266075211445/4901070417";

	public string iOS_key_admob_banner_2="ca-app-pub-7812266075211445/4761469613";

	public string iOS_key_admob_intersitial= "ca-app-pub-7812266075211445/6238202811";

	public string Android_key_admob_banner= "ca-app-pub-7812266075211445/9191669210";

	public string Android_key_admob_banner_2= "ca-app-pub-7812266075211445/1668402417";

	public string Android_key_admob_intersitial="ca-app-pub-7812266075211445/3145135615";


	public string  iOS_key_imobile_banner_pub_id="38295";
	public string  iOS_key_imobile_banner_media_id="170999";
	public string  iOS_key_imobile_banner_spot_id="463779";

	public string  iOS_key_imobile_banner_pub_id_2="38295";
	public string  iOS_key_imobile_banner_media_id_2="170999";
	public string  iOS_key_imobile_banner_spot_id_2="502458";

	public string  iOS_key_imobile_interstitial_pub_id="38295";
	public string  iOS_key_imobile_interstitial_media_id="170999";
	public string  iOS_key_imobile_interstitial_spot_id="463799";

	public string  Android_key_imobile_banner_pub_id="38295";
	public string  Android_key_imobile_banner_media_id="171268";
	public string  Android_key_imobile_banner_spot_id="463800";

	public string  Android_key_imobile_banner_pub_id_2="38295";
	public string  Android_key_imobile_banner_media_id_2="171268";
	public string  Android_key_imobile_banner_spot_id_2="502459";

	public string  Android_key_imobile_interstitial_pub_id="38295";
	public string  Android_key_imobile_interstitial_media_id="171268";
	public string  Android_key_imobile_interstitial_spot_id="463803";

	public string  GreeAds_site_id ="11829";
	public string  GreeAds_site_key="bdfea2a7e3cc5740ab59088f420d491a";
	public string  GreeAds_campaign_id ="10368";
	public string  GreeAds_Advertisement ="install";
	public string  GreeAds_scheme ="hitofude2://hitofude2-host";

	public bool enable_imobile_first_banner=true;
	public bool enable_imobile_second_banner=true;

	//public string iOS_key_applipromotion="TUSAAB0Z1H1QJVPL";
	//public string Android_key_applipromotion="JUKZLHWDC3V33TLX";
	
	//public NendAdBanner nendadbanner;
	//public NendAdIcon nendadicon;
	public bool is_test;
	//public bool is_japanese;


	private BannerView bannerView_g;
	private BannerView bannerView_g_2;
	private InterstitialAd interstitial_g;

	private int ad_result_count;

	public ESCAPE.SYSTEM.Settings settings; 

	private bool is_imobile_interstitial_on=false;

	//private DisplayMetricsAndroid dma;


	// Use this for initialization


	void Start () {

		//GreeAdsReward.setAppInfo(GreeAds_site_id, GreeAds_site_key, false);
		//GreeAdsReward.sendAction(GreeAds_campaign_id, GreeAds_Advertisement, GreeAds_scheme);
	
		print ("density:"+DisplayMetricsAndroid.Density);

		if (((!is_test) && Application.systemLanguage == SystemLanguage.Japanese) || (is_test&&settings.is_japanese())) {

			load_intersititial_imobile();

		}else{

			load_intersititial_google ();
		}


		#if UNITY_EDITOR
		#elif UNITY_IPHONE || UNITY_ANDROID
		IMobileSdkAdsUnityPlugin.addObserver(this.name);
		#endif

		show_banner ();
	}
	
	// Update is called once per frame
	void Update () {
		/*
		show_intersititial_imobile ();*/

		if(is_imobile_interstitial_on){
			show_intersititial_imobile();
		}

	}



	public void show_banner(){

		if (((!is_test)&&Application.systemLanguage == SystemLanguage.Japanese)||(is_test&&settings.is_japanese()))
		{
			//nendadbanner.Show();
			show_banner_imobile();
		}
		else
		{
			show_banner_google ();
		}
		
	}

	public void hide_banner(){
		
		if (((!is_test)&&Application.systemLanguage == SystemLanguage.Japanese)||(is_test&&settings.is_japanese()))
		{
			//nendadbanner.Hide();
			hide_banner_imobile();
			
		}
		else
		{
			hide_banner_google ();
		}
		
	}

	public void show_icon(){
		if (((!is_test)&&Application.systemLanguage == SystemLanguage.Japanese)||(is_test&&settings.is_japanese()))
		{
			//nendadicon.Show();
		}
		else
		{
			
		}
	}

	public void hide_icon(){
		if (((!is_test)&&Application.systemLanguage == SystemLanguage.Japanese)||(is_test&&settings.is_japanese()))
		{
			//nendadicon.Hide();
		}
		else
		{
			
		}
	}

	public void show_wall(){

		if (((!is_test)&&Application.systemLanguage == SystemLanguage.Japanese)||(is_test&&settings.is_japanese()))
		{
			show_applipromotion();
		}else{
			show_intersititial_google();
		}
	}

	public void show_wall_result(){

		/*

		if(ad_result_count>=settings.ad_result_interval){
		
			if (((!is_test)&&Application.systemLanguage == SystemLanguage.Japanese)||(is_test&&is_japanese))
			{
				show_intersititial_imobile();
			}else{
				show_intersititial_google();
			}

			ad_result_count=0;
		}

		ad_result_count++;
		+>*/

		print ("showing_interstitial...");

		if (((!is_test)&&Application.systemLanguage == SystemLanguage.Japanese)||(is_test&&settings.is_japanese()))
		{
			//show_intersititial_imobile();
			is_imobile_interstitial_on=true;
		}else{
			show_intersititial_google();
		}

	}

	public void show_banner_imobile(){

		string pub_id = "";
		string media_id="";
		string spot_id="";

		string pub_id_2 = "";
		string media_id_2="";
		string spot_id_2="";


		#if UNITY_IPHONE 

		pub_id = iOS_key_imobile_banner_pub_id;
		media_id = iOS_key_imobile_banner_media_id;
		spot_id = iOS_key_imobile_banner_spot_id;

		pub_id_2 = iOS_key_imobile_banner_pub_id_2;
		media_id_2 = iOS_key_imobile_banner_media_id_2;
		spot_id_2 = iOS_key_imobile_banner_spot_id_2;


		#elif UNITY_ANDROID

		pub_id = Android_key_imobile_banner_pub_id;
		media_id = Android_key_imobile_banner_media_id;
		spot_id =  Android_key_imobile_banner_spot_id;

		pub_id_2 = Android_key_imobile_banner_pub_id_2;
		media_id_2 = Android_key_imobile_banner_media_id_2;
		spot_id_2 =  Android_key_imobile_banner_spot_id_2;
		
		#endif


		print ("pub id:"+pub_id+" media id:"+media_id+" spot id:"+spot_id);

		IMobileSdkAdsUnityPlugin.registerInline(pub_id,media_id,spot_id);
		IMobileSdkAdsUnityPlugin.start(spot_id);

		int ad_left = 0;
		int ad_top = 0;

		//ad_top = (int) (Screen.height -150.0f);

		float screen_ratio = IMobileSdkAdsUnityPlugin.getScreenHeight ()/(Screen.height*1.0f);


		int offset = 200;



		float ad_top_original=(Screen.height- (Screen.width / 640.0f) * offset);

		float ad_top_float = screen_ratio*ad_top_original;

		ad_top = (int) ad_top_float;




		if (enable_imobile_first_banner) {

			IMobileSdkAdsUnityPlugin.show (spot_id, IMobileSdkAdsUnityPlugin.AdType.BANNER, ad_left, ad_top,true);
		}




		if (enable_imobile_second_banner) {

			IMobileSdkAdsUnityPlugin.registerInline(pub_id_2,media_id_2,spot_id_2);
			IMobileSdkAdsUnityPlugin.start(spot_id_2);

			offset = 300;
			
			ad_top_original=(Screen.height- (Screen.width / 640.0f) * offset);
			
			ad_top_float = screen_ratio*ad_top_original;
			
			ad_top = (int) ad_top_float;
			
			IMobileSdkAdsUnityPlugin.show (spot_id_2, IMobileSdkAdsUnityPlugin.AdType.BANNER, ad_left, ad_top,true);
		}
	
	
	}
	public void hide_banner_imobile(){
		
		
	}

	public void load_intersititial_imobile(){

		#if UNITY_IPHONE 
		IMobileSdkAdsUnityPlugin.registerFullScreen(iOS_key_imobile_interstitial_pub_id, iOS_key_imobile_interstitial_media_id, iOS_key_imobile_interstitial_spot_id);
		IMobileSdkAdsUnityPlugin.start(iOS_key_imobile_interstitial_spot_id);
		#elif UNITY_ANDROID



		IMobileSdkAdsUnityPlugin.registerFullScreen(Android_key_imobile_interstitial_pub_id, Android_key_imobile_interstitial_media_id, Android_key_imobile_interstitial_spot_id);
		IMobileSdkAdsUnityPlugin.start(Android_key_imobile_interstitial_spot_id);

		#endif
		
		
		
	}

	public void show_intersititial_imobile(){

		print ("showing imobile interstitial");

		#if UNITY_IPHONE 
		IMobileSdkAdsUnityPlugin.show(iOS_key_imobile_interstitial_spot_id);
		#elif UNITY_ANDROID
		
		IMobileSdkAdsUnityPlugin.show(Android_key_imobile_interstitial_spot_id);
		

		
		#endif



	}


	public void show_banner_google()
	{
		if (enable_imobile_first_banner) {
			#if UNITY_EDITOR
			string adUnitId = "unused";
			#elif UNITY_ANDROID
			string adUnitId = Android_key_admob_banner;
			#elif UNITY_IPHONE
			string adUnitId = iOS_key_admob_banner;
			#else
			string adUnitId = "unexpected_platform";
			#endif
		
			//GADBannerView.hで表示位置を修正しています。
			bannerView_g = new BannerView (adUnitId, AdSize.Banner, AdPosition.Top);

			AdRequest request = new AdRequest.Builder ().Build ();

			bannerView_g.LoadAd (request);
		}

		if (enable_imobile_second_banner) {

			#if UNITY_EDITOR
			string adUnitId = "unused";
			#elif UNITY_ANDROID
			string adUnitId = Android_key_admob_banner_2;
			#elif UNITY_IPHONE
			string adUnitId = iOS_key_admob_banner_2;
			#else
			string adUnitId = "unexpected_platform";
			#endif
		

			
			//GADBannerView.hで表示位置を修正しています。
			bannerView_g_2 = new BannerView (adUnitId, AdSize.Banner, AdPosition.Bottom);
			
			AdRequest request = new AdRequest.Builder ().Build ();
			
			bannerView_g_2.LoadAd (request);
		}
		//bannerView.sh
		// Load a banner ad.
		//bannerView.LoadAd(createAdRequest());
	}

	public void hide_banner_google()
	{
		bannerView_g.Hide ();
	}

	public void load_intersititial_google(){
		// Initialize an InterstitialAd.

		print ("loading google interstitial");

		#if UNITY_EDITOR
		string adUnitId = "unused";
		#elif UNITY_ANDROID
		string adUnitId = Android_key_admob_intersitial;
		#elif UNITY_IPHONE
		string adUnitId = iOS_key_admob_intersitial;
		#else
		string adUnitId = "unexpected_platform";
		#endif

		interstitial_g = new InterstitialAd(adUnitId);
		// Create an empty ad request.

		interstitial_g.AdClosed += HandleInterstitialClosed;
		AdRequest request = new AdRequest.Builder().Build();
		// Load the interstitial with the request.
		interstitial_g.LoadAd(request);
	}

	public void HandleInterstitialClosed(object sender, EventArgs args)
	{
		load_intersititial_google ();
	}

	public void show_intersititial_google(){

		print ("showing google interstitial");

		if (interstitial_g.IsLoaded()) {
			interstitial_g.Show();
		}
	}



	public void show_applipromotion(){

		/*
		var parameters = new APUnityPluginSetting();
		parameters.setParam(APUnityPluginSetting.iOSAppkey, iOS_key_applipromotion);
		parameters.setParam(APUnityPluginSetting.onStatusArea, true);
		parameters.setParam (APUnityPluginSetting.orientation, "UIInterfaceOrientationPortrait");
		//parameters.setParam(APUnityPluginSetting.orientation, "UIInterfaceOrientationLandscapeLeft");
		parameters.setParam(APUnityPluginSetting.isClose, true);
		parameters.setParam(APUnityPluginSetting.AndroidAppKey, Android_key_applipromotion);
		APUnityPlugin.ShowAppliPromotionWall(parameters);*/
	}

	void imobileSdkAdsSpotDidShow (string value) {

		string spot_id = "";

		#if UNITY_IPHONE 

		spot_id=iOS_key_imobile_interstitial_spot_id;

		#elif UNITY_ANDROID

		spot_id=Android_key_imobile_interstitial_spot_id;
		
		#endif

		//string arg_spotid=

		print ("imobile ad did show with spot id: " + value+ " interstitial:"+Android_key_imobile_interstitial_spot_id);

		if(value.Contains(spot_id)){

			print ("imobile interstitial off");


			is_imobile_interstitial_on=false;
		}
	}


	
}
