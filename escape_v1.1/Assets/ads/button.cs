﻿/*
using UnityEngine;
using System.Collections;

public class button : MonoBehaviour {


	public ad_controller ad;

	public bool is_japanese=true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	
	void OnGUI()
	{
		// Puts some basic buttons onto the screen.
		GUI.skin.button.fontSize = (int) (0.05f * Screen.height);

		float button_height = 0.05f * Screen.height;

		//float 


		Rect languageRect = new Rect(0.1f * Screen.width, 0.05f * Screen.height,
		                             0.8f * Screen.width,button_height );
		
		string label= "Japanese";
		if(!is_japanese){
			label="Not Japanese";
		}
		
		
		if (GUI.Button(languageRect,label ))
		{
			is_japanese=!is_japanese;
			ad.is_japanese=is_japanese;
		}

		Rect requestBannerRect = new Rect(0.1f * Screen.width, 0.15f * Screen.height,
		                                  0.8f * Screen.width,button_height );

		if (GUI.Button(requestBannerRect, "show banner"))
		{
			ad.show_banner();
		}

		Rect hideBannerRect = new Rect(0.1f * Screen.width, 0.25f * Screen.height,
		                                  0.8f * Screen.width,button_height );
		
		if (GUI.Button(hideBannerRect, "hide banner"))
		{
			ad.hide_banner();
		}


		Rect showIconRect = new Rect(0.1f * Screen.width, 0.35f * Screen.height,
		                               0.8f * Screen.width, button_height);
		if (GUI.Button(showIconRect, "show_icon"))
		{
			ad.show_icon();
		}
		
		Rect hideIconRect = new Rect(0.1f * Screen.width, 0.45f * Screen.height,
		                               0.8f * Screen.width, button_height);
		if (GUI.Button(hideIconRect, "hide_icon"))
		{
			ad.hide_icon();
		}




		Rect destroyBannerRect = new Rect(0.1f * Screen.width, 0.55f * Screen.height,
		                                  0.8f * Screen.width, button_height);
		if (GUI.Button(destroyBannerRect, "wall"))
		{
			ad.show_wall();
		}



		/*
		Rect requestInterstitialRect = new Rect(0.1f * Screen.width, 0.55f * Screen.height,
		                                        0.8f * Screen.width, 0.1f * Screen.height);
		if (GUI.Button(requestInterstitialRect, "Request Interstitial"))
		{
			RequestInterstitial();
		}
		
		Rect showInterstitialRect = new Rect(0.1f * Screen.width, 0.675f * Screen.height,
		                                     0.8f * Screen.width, 0.1f * Screen.height);
		if (GUI.Button(showInterstitialRect, "Show Interstitial"))
		{
			ShowInterstitial();
		}
		
		Rect destroyInterstitialRect = new Rect(0.1f * Screen.width, 0.8f * Screen.height,
		                                        0.8f * Screen.width, 0.1f * Screen.height);
		if (GUI.Button(destroyInterstitialRect, "Destroy Interstitial"))
		{
			interstitial.Destroy();
		}
	}
}
*/