﻿using UnityEngine;
using System.Collections;
using System.IO;

public class hitofude_data_loader : MonoBehaviour {

	// Use this for initialization

	public hitofude_data_controller hitofude_data;

	public TextAsset data;
	//public string text;

	void Start () {

		//load_data ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public string load_data(int input_level){

		//string output = "success";

		string output = "fail";
		//if(hitofude_data!=null){

		if(hitofude_data.settings.is_load_test){
			data= (TextAsset) Resources.Load("hitofude_data/001");
		}else{

			data= (TextAsset) Resources.Load("hitofude_data/"+input_level.ToString ("000"));

		}
			
			
			StringReader reader = new StringReader(data.text);

			int count = 0;

			bool point_flag = false;
			bool line_flag = false;
			bool answer_flag = false;
			bool quest_flag = false;

			string last_string = "";
			
			float min_x=10000;
			float max_x=0;
			
			float min_y=10000;
			float max_y=0;
			
			while (reader.Peek() > -1) {


				string line = reader.ReadLine();

				//print (line);

				//if(count==){
				//string[] values = line.Split(',');
				//}

				if(point_flag){

					if(line=="#END"){
						point_flag=false;

						string[] values = last_string.Split(':');
						
						hitofude_data.points= new hitofude_point_controller[values.Length-1];
						
						int point_count=0;
						foreach(string value in values){
							
							if(point_count!=0){
								//print (value);
								string[] coords = value.Split(',');
								
								//GameObject temp= new GameObject();
								//temp.AddComponent<hitofude_point_controller>();

								GameObject temp= (GameObject) Instantiate(hitofude_data.point_prefab);
								
								temp.transform.parent=hitofude_data.points_parent.transform;
								
								temp.transform.localPosition= new Vector3(0,0,0);
								
								temp.transform.localScale= new Vector3(1,1,1);

								hitofude_data.points[point_count-1] =  temp.GetComponent<hitofude_point_parent_controller>().point;
							
							hitofude_data.points[point_count-1].initialize(hitofude_data,point_count-1,int.Parse (coords[0]),int.Parse (coords[1]),hitofude_point_controller.point_state.normal);

								
								if(hitofude_data.points[point_count-1].x<min_x){
									min_x=hitofude_data.points[point_count-1].x;
								}
								
								if(hitofude_data.points[point_count-1].x>max_x){
									max_x=hitofude_data.points[point_count-1].x;
								}
								
								if(hitofude_data.points[point_count-1].y<min_y){
									min_y=hitofude_data.points[point_count-1].y;
								}
								
								if(hitofude_data.points[point_count-1].y>max_y){
									max_y=hitofude_data.points[point_count-1].y;
								}
							}
							point_count++;
						}
					//print ((max_x+","+min_x));

					foreach (hitofude_point_controller temp in hitofude_data.points){

						temp.set_position(min_x,max_x,min_y,max_y);

					}
					

					}else{
						last_string+=":"+line;
					}

					
					
				}
					
				if(line=="#POINT"){
					last_string="";
					point_flag=true;
					
				}


				if(line_flag){
				
					if(line=="#END"){
						line_flag=false;
						
						string[] values = last_string.Split(':');
						
						hitofude_data.lines= new hitofude_line_controller[values.Length-1];
						
						int line_count=0;
						foreach(string value in values){
							
							if(line_count!=0){
								//print (value);
								string[] coords = value.Split(',');
								
								
								GameObject temp= (GameObject) Instantiate(hitofude_data.line_prefab);
								temp.transform.parent=hitofude_data.lines_parent.transform;

								temp.transform.localPosition= new Vector3(0,0,0);
								temp.transform.localScale= new Vector3(1,1,1);
								
								hitofude_data.lines[line_count-1] = temp.GetComponent<hitofude_line_parent_controller>().line;
								
								bool is_one_direction=false;

								int line_type=int.Parse(coords[2]);

								int selection_count=1;
							
								if(line_type==1){

									is_one_direction=true;

								}else if(line_type==2){

									selection_count=2;

								}else if(line_type==3){
									
									is_one_direction=true;

									selection_count=2;

								}


								



								hitofude_data.lines[line_count-1].initialize(hitofude_data,hitofude_data.points[int.Parse (coords[0])],hitofude_data.points[int.Parse (coords[1])],is_one_direction,selection_count);
								
							}
							line_count++;
						}
					}else{
						last_string+=":"+line;
					}

				}

				
				if(line=="#LINE"){
					last_string="";
					line_flag=true;
					
				}

				if(answer_flag){
					answer_flag=false;

					string[] values = line.Split(',');
					int answer_count=0;
					int prev_point=-1;

					hitofude_data.answer=new int[values.Length-1];
					hitofude_data.answer_lines=new hitofude_line_controller[values.Length-2];


					foreach(string value in values){
						
						
						int parsed_int;

						if(int.TryParse(value,out parsed_int)){
							hitofude_data.answer[answer_count]=parsed_int;

							if(prev_point>-1){

								hitofude_data.answer_lines[answer_count-1]=hitofude_data.points[prev_point].set_line_answer_no(parsed_int,answer_count);
								
							}
							prev_point=parsed_int;

						}else{

						}
						
						
						answer_count++;
					}

				}

				if(line=="#ANSER"){
					last_string="";
					answer_flag=true;

					
					
				}

				if(quest_flag){
					quest_flag=false;

					string[] values = line.Split(',');
					int quest_count=0;
					int prev_point=-1;

					hitofude_data.quest=new bool[values.Length-1];
					foreach(string value in values){
						int parsed_int;
						
				
						if(int.TryParse(value,out parsed_int)){

								
							bool quest=false;

							if(parsed_int==1){
								quest=true;
							}

							hitofude_data.quest[quest_count]=quest;

							if(quest_count<hitofude_data.answer.Length-1){
								
								hitofude_data.answer_lines[quest_count].set_quest(quest);
								
								//hitofude_data.points[hitofude_data.answer[quest_count]].set_line_quest_no(hitofude_data.answer[quest_count+1],quest);
							}

							prev_point=parsed_int;
						}


						

						quest_count++;
					}

					
				}
				
				if(line=="#QUEST"){
					last_string="";
					quest_flag=true;
					
				}
				
				
				count++;

			}

			hitofude_data.refresh_point_availability ();

			foreach (hitofude_line_controller line in hitofude_data.lines) {
				line.refresh_line();
			}

			

		return output;
	}
}
