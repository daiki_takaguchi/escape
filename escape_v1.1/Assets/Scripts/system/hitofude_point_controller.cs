﻿using UnityEngine;
using System.Collections;

public class hitofude_point_controller : MonoBehaviour {

	// Use this for initialization

	public GameObject parent; 
	public Transform scale_point;
	public hitofude_data_controller hitofude_data;
	public hitofude_line_controller[] connected_lines=new hitofude_line_controller[0];
	//public hitofude_line_controller[] selected_lines=new hitofude_line_controller[0];

	public UIWidget point_texture;
	public BoxCollider box;

	public int id;

	public bool is_avaiable=true;
	//public int answer_no;
	//public bool quest_no;

	public int x;
	public int y;
	public int selected_count = 0;

	public point_state state;

	public bool is_gray=false;

	public enum point_state{
		normal,
		selected

	}

	public enum line_availability{
		yes,
		no,
		reverse,
		quest,
		reverse_and_quest
	}

	public enum Point_animation_state{
		none,
		start,
		grow,
		shrink,
		max,
		end
	}

	public float point_animation_start_time;

	public Vector3 prev_scale;

	public Point_animation_state point_animation_state=Point_animation_state.none;
	
	void Start () {

		float temp = hitofude_data.settings.point_collider_diameter/hitofude_data.settings.point_diameter;
		box.size = new Vector3 (temp,temp,0.5f);
	}
	
	// Update is called once per frame
	void Update () {

		if (point_animation_state == Point_animation_state.start) {

			point_animation_start_time = Time.time;
			point_animation_state=Point_animation_state.grow;
			prev_scale=transform.localScale;
			transform.localScale= new Vector3(prev_scale.x*hitofude_data.settings.ripple_current_point_max_diameter_ratio,prev_scale.y*hitofude_data.settings.ripple_current_point_max_diameter_ratio,prev_scale.z);

			//print ("start");
		} else if (point_animation_state == Point_animation_state.grow) {

			if(Time.time>point_animation_start_time+hitofude_data.settings.ripple_current_point_end_time){
				point_animation_state=Point_animation_state.end;
			}


						
		} else if (point_animation_state == Point_animation_state.shrink) {
	
		} else if (point_animation_state == Point_animation_state.end) {
			transform.localScale = prev_scale;
			point_animation_state = Point_animation_state.none;
		}


	}


	public hitofude_line_controller set_line_answer_no(int end_point_id,int answer_no){

		hitofude_line_controller temp=  search_line(end_point_id);

		if(temp!=null){

			temp.set_answer(answer_no);
		}

		return temp;
	}



	public line_availability check_line_availabiliy(int input_end_point_id,int answer_count,out hitofude_line_controller output_line){
		line_availability output = line_availability.no;

		bool is_reverse = false;

		hitofude_line_controller temp=  search_line(input_end_point_id);

		output_line = temp;
		
		if (temp != null) {

			output = temp.check_line_availability (answer_count, hitofude_data.quest[answer_count-1],id, input_end_point_id);


		} else {
			output =line_availability.no;
		}

		//print (output);

		return output;
	}
	
	/*
	public hitofude_line_controller search_line(int end_point_id, out bool is_reverse_direction){
		
		hitofude_line_controller output = null;

		is_reverse_direction=false;
		
		foreach (hitofude_line_controller line in connected_lines) {
			
			if(line.is_one_direction){
				
				if(line.start_point_id==id&&line.end_point_id==end_point_id){
					output=line;

				}

				if(line.end_point_id==id&&line.start_point_id==end_point_id){
					output=line;
					is_reverse_direction=true;
				}
				
			}else{
				if((line.start_point_id==id&&line.end_point_id==end_point_id)||(line.end_point_id==id&&line.start_point_id==end_point_id)){
					output=line;
				}
			}
			
		}
		
		
		return output;
	}*/


	public hitofude_line_controller search_line(int end_point_id){

		hitofude_line_controller output = null;

		foreach (hitofude_line_controller line in connected_lines){

			//bool is_reverse=false;

			if(line.is_this_line(id,end_point_id)){
				output=line;
				break;
			}

		}


		return output;
	}


/*
	public void change_line_state(int end_point_id,hitofude_line_controller.line_state state){
		hitofude_line_controller temp=  search_line(end_point_id);
		
		if (temp != null) {
			temp.refresh_line();
		}
	}*/

	public void add_connected_line(hitofude_line_controller input_line){

		hitofude_line_controller[] new_lines = new hitofude_line_controller[connected_lines.Length+1];


		for (int i=0; i<connected_lines.Length; i++) {
			new_lines[i]=connected_lines[i];
		}

		new_lines[connected_lines.Length]=input_line;

		connected_lines = new_lines;
	}



	public void set_position(float input_min_x,float input_max_x,float input_min_y,float input_max_y){
		float min_x = 0;
		float max_x = 1000;
		float min_y= 0;
		float max_y=1000;


		if(hitofude_data.settings.fit_hitofude_shape){
			min_x=input_min_x;
			max_x=input_max_x;
			min_y=input_min_y;
			max_y=input_max_y;
		}

		Vector3 temp = transform.localPosition;

		float scale_factor = hitofude_data.settings.get_hitofude_width();

		float normalized_x =((x-((min_x+max_x)/2.0f))/(max_x-min_x));
		float normalized_y =-((y-((min_y+max_y)/2.0f))/(max_y-min_y));

		float reflection_x = hitofude_data.is_reflected_x?-1:1;
		float reflection_y = hitofude_data.is_reflected_y?-1:1;

		int offset_y = hitofude_data.settings.get_hitofude_offset_y();
		//int offset_y = 0;
		transform.localPosition =new Vector3(0,offset_y,0)+ Quaternion.Euler (0,0,hitofude_data.rotation_angle)*new Vector3 (reflection_x*normalized_x*scale_factor,reflection_y*normalized_y*scale_factor,temp.z);
	}




	public void change_state(point_state input_state){

		state = input_state;

		if(input_state==point_state.selected){
			point_texture.color= hitofude_data.settings.red;
		}else if(input_state==point_state.normal){
			point_texture.color= hitofude_data.settings.base_color;
		}

	}

	public void initialize(hitofude_data_controller input_hitofude_data,int input_id,int input_x,int input_y,point_state input_state){
		id = input_id;
		x = input_x;
		y = input_y;
		selected_count = 0;
		hitofude_data = input_hitofude_data;
		change_state (input_state);

		parent.name="point_parent "+input_id+":("+input_x+","+input_y+")";
		transform.localScale = new Vector3 (input_hitofude_data.settings.point_diameter,input_hitofude_data.settings.point_diameter,1);
	}
	/*

	public void check_availability(){
		if(connected_lines.Length==0){
			disable_touch();
		}
	}*/

	public void rescale_point(float scale_factor){

		transform.localScale = new Vector3 (hitofude_data.settings.point_diameter*scale_factor, hitofude_data.settings.point_diameter*scale_factor, 1);
	}

	public void enable_touch(){
		is_avaiable = true;
		//box.enabled=true;
	}

	public void disable_touch(){
		is_avaiable = false;
		//box.enabled=false;
	}

	public void quest_mode_gray(){

		if(selected_count==0){
			point_texture.color= hitofude_data.settings.point_unavailable;
			disable_touch();
			is_gray=true;
		}
		
	}
	
	public void quest_mode_ungray(){
		
		if (is_gray) {
			change_state (state);
			enable_touch();
			is_gray=false;
		}
	}


	void OnDrug() {
		//print ("drag");
		//hitofude_data.ui.button_point_pressed (this);
	}

	void OnHover(bool is_over){
		//print ("hover:"+is_over);
		if (Input.GetMouseButtonDown(0)){
			hitofude_data.ui.button_point_pressed (this);

		}
	}

	void OnDragOver(GameObject draggedObject){
		//print ("dragover");
	}


	void OnPress(bool is_pressed) {

		if(is_pressed&&is_avaiable){
			point_pressed();
		}

	}


	public void point_pressed(){
		hitofude_data.ui.button_point_pressed (this);
		hitofude_data.ui.UI_button_touch_panel.GetComponent<button_touch_panel_controller> ().non_drag_mode = true;
	}

}
