﻿using UnityEngine;
using System.Collections;

public class hitofude_draw_controller : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}



	public static void draw_line(GameObject line, Vector3 from, Vector3 to){

		Vector3 from_2d = new Vector3 (from.x, from.y, 0);
		Vector3 to_2d = new  Vector3 (to.x, to.y, 0);

		float length = (to_2d - from_2d).magnitude;

		Vector3 temp_scale = line.transform.localScale;


		//Quaternion rotation = 
		Vector3 temp = line.transform.localPosition;
		line.transform.localPosition =  new Vector3(from.x,from.y,temp.z);
		line.transform.localScale = new Vector3 (length*2,temp_scale.y,temp_scale.z);
		line.transform.localRotation=Quaternion.FromToRotation (new Vector3(1,0,0),(to_2d-from_2d).normalized);


	} 


}
