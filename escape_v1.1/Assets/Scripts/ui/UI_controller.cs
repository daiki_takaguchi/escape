﻿using UnityEngine;
using System.Collections;

public class UI_controller : MonoBehaviour {

	// Use this for initialization

	public bool is_unlock_url_found=false;

	public Camera UI_camera;
	//public Camera UI_camera_UI_2;

	public ad_controller ad;

	public hitofude_data_controller hitofude_data;

	public ESCAPE.UI.UI_animation_controller UI_animator;


	//public background_title_depth_controller b
	public answer_point_animation answer_point;



	//public BGM_controller BGM;
	//public SE_controller SE;

	public GameObject button_pressed_animation_parent;
	public GameObject button_pressed_animation;


	public GameObject select_level_button;

	public GameObject UI_background_title;
	public GameObject UI_background_play;
	public GameObject UI_background_select_level;

	public GameObject UI_effect;

	public GameObject UI_buttons_title;

	//public GameObject UI_buttons_select_level_parent;
	//public GameObject UI_buttons_select_dan_parent;

	public custom_scroll UI_buttons_select_level_parent;
	public custom_scroll UI_buttons_select_dan_parent;

	public GameObject UI_buttons_result;

	public GameObject[] UI_buttons_select_level;
	public GameObject[] UI_buttons_select_dan;

	public GameObject UI_button_mode_normal_parent;
	public GameObject UI_button_mode_dan_parent;
	public GameObject UI_button_mode_dan;

	public GameObject UI_button_home;
	public GameObject UI_button_undo;
	public GameObject UI_button_restart;
	public GameObject UI_button_hint;
	public GameObject UI_button_answer;



	public GameObject UI_button_applipromotion;

	public GameObject UI_buttons_bottom_background;

	public GameObject UI_window_hint;
	public GameObject UI_window_answer;
	public GameObject UI_window_result;
	public window_result_controller UI_window_result_controller;
	public GameObject UI_window_result_comment;
	
	public GameObject UI_window_dan_clear;
	public GameObject UI_window_tutorial;
	public UIAnchor UI_window_tutorial_anchor;

	public GameObject UI_label_large;
	public GameObject UI_label_small;
	public GameObject UI_label_turn;

	public GameObject UI_label_progress_level;
	public GameObject UI_label_progress_dan;

	public GameObject UI_label_select_level;

	//public panel_scroll_controller scroll_level;
	//public panel_scroll_controller scroll_dan;

	public Transform UI_touch_panel_scale;
	public Transform UI_touch_panel_position;
	public GameObject UI_button_touch_panel;

	//public bool is_animation_on;
	//public bool is_window_shown=false;
	public bool is_button_animation_on=false;
	public bool is_repositioning_screen=false;

	public enum UI_state{
		title,
		title_animation,
		select_level,
		select_level_animation,
		select_dan,
		select_dan_animation,
		hitofude_normal,
		hitorude_animation,
		hitofude_hint,
		hitofude_hint_window,
		hitofude_answer,
		hitofude_answer_window,
		button_animation,
		result,
		dan_clear,
		time_out
	}

	public UI_state state= UI_state.title;
	public UI_state prev_state=UI_state.title;

	public int result_shown_count=0;

	void Start () {

		Application.targetFrameRate = 60;

		create_UI_buttons_select_level ();
		refresh_UI_buttons_select_level ();
		hide_UI_select_level();

		/*
		foreach (GameObject temp in UI_buttons_select_level) {
			temp.GetComponent<button_select_level_controller>().show_button();
		}*/

		create_UI_buttons_select_dan ();
		refresh_UI_buttons_select_dan ();
		hide_UI_select_dan();

		/*
		foreach (GameObject temp in UI_buttons_select_dan) {
			temp.GetComponent<button_select_level_controller>().show_button();
		}
*/
		//UI_buttons_select_level_parent.SetActive (false);
		//UI_buttons_select_dan_parent.SetActive (false);
		

		refresh_UI_buttons_title();
		/*
		UI_background_title.GetComponent<background_title_depth_controller>().animation_position_background.localPosition = new Vector3 (0,0,0);
		UI_background_title.GetComponent<background_title_depth_controller>().animation_position_buttons.localPosition = new Vector3 (0,0,0);
		UI_background_title.GetComponent<background_title_depth_controller>().animation_position_buttons_label.localPosition = new Vector3 (0,0,0);
*/

		//show_UI_select_level ();
		/*
		if(check_tutorial()){
			UI_window_tutorial.SetActive (true);
			
		}*/

		refresh_UI_button_applipromotion (true);

		StartCoroutine (check_unlock_url());
	
	}


	private IEnumerator check_unlock_url(){
		WWW www = new WWW(hitofude_data.settings.dan_unlock_url);
		
		yield return StartCoroutine(ResponceCheckForTimeOutWWW(www, 5.0f));
		
		if (www.error != null) {

		}else if(www.isDone){
			is_unlock_url_found=true;
		}
			//Debug.Log(www.text); //リクエスト成功の場合
	}

	private IEnumerator ResponceCheckForTimeOutWWW(WWW www, float timeout)
	{
		float requestTime = Time.time;
		
		while(!www.isDone)
		{
			if(Time.time - requestTime < timeout)
				yield return null;
			else
			{
				Debug.LogWarning("TimeOut"); //タイムアウト
				break;
			}
		}
		
		yield return null;
	}
	
	/*
	public bool check_tutorial(){
		bool output = false;

		long temp= hitofude_data.save_data.get_time_from_tutorial ();

		if(temp>7776000){
			output=true;
		}
		
		return output;
		
	}*/
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool is_button_available(button_controller.button_type input_type){

		bool output = false;

		if(state==UI_state.title){
			if(input_type==button_controller.button_type.button_mode_normal
			   ||input_type==button_controller.button_type.button_mode_dan){
				output=true;
			}

		}else if(state==UI_state.select_level){

			if(input_type==button_controller.button_type.button_select_level
			   ||input_type==button_controller.button_type.button_select_dan
			   ||input_type==button_controller.button_type.button_home){
				output=true;
			}

		}else if(state==UI_state.select_dan){
			
			if(input_type==button_controller.button_type.button_select_level
			   ||input_type==button_controller.button_type.button_select_dan
			   ||input_type==button_controller.button_type.button_home){
				output=true;
			}
		}else if(state==UI_state.hitofude_normal){
			output=true;


			if(input_type==button_controller.button_type.button_select_level
			   ||input_type==button_controller.button_type.button_select_dan
			   ||input_type==button_controller.button_type.button_next){
				output=false;
			}
		}else if(state==UI_state.hitofude_hint){
			output=true;
			/*if( input_type==button_controller.button_type.button_hint){
				output=false;
			}*/
		}else if(state==UI_state.hitofude_hint_window){
			if(input_type==button_controller.button_type.button_hint_yes||input_type==button_controller.button_type.button_hint_no){
				output=true;
			}
		}else if(state==UI_state.hitofude_answer){
			output=true;

			/*
			if( input_type==button_controller.button_type.button_hint||input_type==button_controller.button_type.button_answer){
				output=false;
			}*/
		}else if(state==UI_state.hitofude_answer_window){

			if(input_type==button_controller.button_type.button_answer_yes||input_type==button_controller.button_type.button_answer_no){
				output=true;
			}

		}else if(state==UI_state.result){
			output=true;
		}

		if(input_type==button_controller.button_type.button_applipromotion){
			output=true;
		}

		if(input_type==button_controller.button_type.button_unlock_dan_no
		   ||input_type==button_controller.button_type.button_unlock_dan_yes){
			output=true;
		}

		if(is_repositioning_screen){
			if(input_type!=button_controller.button_type.touch_panel){
				output=false;
			}
		}

		return output&&(!is_button_animation_on);
	}

	public void button_pressed_delay(button_controller input_button){
		if (is_button_available (input_button.type)) {

			float wait_time=0.0f;


			if(input_button.type==button_controller.button_type.button_mode_normal
			   ||input_button.type==button_controller.button_type.button_mode_dan){

				button_pressed_animation_start (input_button,hitofude_data.settings.button_mode_ripple_diameter);
				wait_time=hitofude_data.settings.button_pressed_animation_wait_time;
				//SE.play_audio(SE_controller.SE_list.click);
				UI_window_tutorial_anchor.enabled=false;

			}else if(input_button.type==button_controller.button_type.button_home
			         ||input_button.type==button_controller.button_type.button_undo
			         ||input_button.type==button_controller.button_type.button_restart
			         ||input_button.type==button_controller.button_type.button_hint
			         ||input_button.type==button_controller.button_type.button_answer){

				button_pressed_animation_start (input_button,hitofude_data.settings.button_home_ripple_diameter);
				wait_time=hitofude_data.settings.button_pressed_animation_wait_time;
				//SE.play_audio(SE_controller.SE_list.click);

			}else if(input_button.type==button_controller.button_type.button_hint_yes
			         ||input_button.type==button_controller.button_type.button_hint_no
			         ||input_button.type==button_controller.button_type.button_answer_yes
			         ||input_button.type==button_controller.button_type.button_answer_no){
				
				button_pressed_animation_start (input_button,hitofude_data.settings.button_window_ripple_diameter);
				wait_time=hitofude_data.settings.button_pressed_animation_wait_time;
				//SE.play_audio(SE_controller.SE_list.click);

			}else if(input_button.type==button_controller.button_type.button_select_level
			         ||input_button.type==button_controller.button_type.button_select_dan){
				
				button_pressed_animation_start (input_button,hitofude_data.settings.button_level_ripple_diameter);
				wait_time=hitofude_data.settings.button_pressed_animation_wait_time;
				//SE.play_audio(SE_controller.SE_list.click);

			}else if(input_button.type==button_controller.button_type.button_next
			         ||input_button.type==button_controller.button_type.button_tweet
			         ||input_button.type==button_controller.button_type.button_review){
				
				button_pressed_animation_start (input_button,hitofude_data.settings.button_next_ripple_diameter);
				wait_time=hitofude_data.settings.button_pressed_animation_wait_time;
				//SE.play_audio(SE_controller.SE_list.click);

			}else if(input_button.type==button_controller.button_type.button_applipromotion){
				
				//button_pressed_animation_start (input_button,hitofude_data.settings.button_window_ripple_diameter);
				wait_time=hitofude_data.settings.button_pressed_animation_wait_time;
				//SE.play_audio(SE_controller.SE_list.click);

			}else if(input_button.type==button_controller.button_type.button_unlock_dan_yes){

				//wait_time=hitofude_data.settings.button_pressed_animation_wait_time;
				//SE.play_audio(SE_controller.SE_list.click);

			}else if(input_button.type==button_controller.button_type.button_unlock_dan_no){
				//wait_time=hitofude_data.settings.button_pressed_animation_wait_time;
				//SE.play_audio(SE_controller.SE_list.click);
			}

			is_button_animation_on=true;
			StartCoroutine(button_pressed_wait_for_seconds(input_button,wait_time));
		}
	}

	public IEnumerator button_pressed_wait_for_seconds(button_controller input_button,float wait_time){
		yield return new WaitForSeconds(wait_time);

		is_button_animation_on=false;
		button_pressed(input_button);

	}


	public void button_pressed(button_controller input_button){
		if(is_button_available(input_button.type)){
			if(input_button.type==button_controller.button_type.button_mode_normal){
				
				show_UI_select_level ();
				

				//UI_animator.animation_start(UI_animation_controller.animation_type.normal_mode);


			
				change_state(UI_state.select_level_animation);


			
			}else if(input_button.type==button_controller.button_type.button_mode_dan){

				show_UI_select_dan();
				

				

				//ui_animator.Play ("background-play_slide_anim");
				//UI_animator.animation_start(UI_animation_controller.animation_type.dan_mode);




				change_state(UI_state.select_dan_animation);
				
			}else if(input_button.type==button_controller.button_type.button_home){

				ad.hide_icon();

				show_UI_title();


				//UI_animator.animation_start(UI_animation_controller.animation_type.home);

				UI_label_large.GetComponent<time_count_controller> ().stop_count();



				change_state(UI_state.title_animation);

			}else if(input_button.type==button_controller.button_type.button_home_no_SE){

				ad.hide_icon();

				show_UI_title();



				//UI_animator.animation_start(UI_animation_controller.animation_type.home);

				UI_label_large.GetComponent<time_count_controller> ().stop_count();

				
				change_state(UI_state.title_animation);

			}else if(input_button.type==button_controller.button_type.button_undo){



				hitofude_data.button_undo_pressed ();
				button_touch_panel_unpressed();


			}else if(input_button.type==button_controller.button_type.button_restart){

			

				hitofude_data.button_restart_pressed (true);
				button_touch_panel_unpressed();


			}else if(input_button.type==button_controller.button_type.button_hint){


				if((!hitofude_data.is_hint_mode)&&(!hitofude_data.is_answer_mode)){

					UI_label_large.GetComponent<time_count_controller> ().pause_count ();
					show_UI_window_hint ();



					change_state(UI_state.hitofude_hint_window);
				}


			}else if(input_button.type==button_controller.button_type.button_hint_yes){



				UI_label_large.GetComponent<time_count_controller> ().resume_count ();

				hitofude_data.save_data.hint_used ();
				hitofude_data.button_hint_yes_pressed();

				hide_UI_window_hint ();

			

				change_state(UI_state.hitofude_hint);

			}else if(input_button.type==button_controller.button_type.button_hint_no){

			

				UI_label_large.GetComponent<time_count_controller> ().resume_count ();
				
				hide_UI_window_hint ();



				change_state(prev_state);

			}else if(input_button.type==button_controller.button_type.button_answer){


				if((!hitofude_data.is_answer_mode)){
					UI_label_large.GetComponent<time_count_controller> ().pause_count ();
					show_UI_window_answer ();

				

					change_state(UI_state.hitofude_answer_window);
				}

			}else if(input_button.type==button_controller.button_type.button_answer_yes){



				hitofude_data.save_data.answer_used ();
				hitofude_data.button_answer_yes_pressed();
				
				UI_label_large.GetComponent<time_count_controller> ().resume_count ();

				
				hide_UI_window_answer();

			

				change_state(UI_state.hitofude_answer);

			}else if(input_button.type==button_controller.button_type.button_answer_no){



				UI_label_large.GetComponent<time_count_controller> ().resume_count ();
	
				hide_UI_window_answer();

				change_state(prev_state);


			}else if(input_button.type==button_controller.button_type.button_next){



				if(hitofude_data.is_dan){

					button_controller temp = new button_controller();
					temp.type=button_controller.button_type.button_home_no_SE;

					button_pressed (temp);
				}else{
					if(hitofude_data.current_level==(hitofude_data.settings.max_level-1)){
						button_controller temp = new button_controller();
						temp.type=button_controller.button_type.button_home_no_SE;

						button_pressed (temp);

					}else{
						hide_UI_result();
						show_UI_play_level(hitofude_data.current_level+1);

					}
				}



			}else if(input_button.type==button_controller.button_type.button_tweet){

				string url="";
				
				if(Application.platform==RuntimePlatform.IPhonePlayer){
					
					url=hitofude_data.settings.tweet_url_iOS;
				}else if(Application.platform==RuntimePlatform.Android){
					url=hitofude_data.settings.tweet_url_android;
				}

				string comment="";
				
				if(Application.platform==RuntimePlatform.IPhonePlayer){
					
					comment=hitofude_data.settings.tweet_comment_iOS;
				}else if(Application.platform==RuntimePlatform.Android){
					comment=hitofude_data.settings.tweet_comment_android;
				}

				SocialConnector.Share (comment,url);

			}else if(input_button.type==button_controller.button_type.button_review
			         ||input_button.type==button_controller.button_type.button_unlock_dan_yes){

				string url="https://www.google.com/";

				if(Application.platform==RuntimePlatform.IPhonePlayer){

					url=hitofude_data.settings.review_url_iOS;
				}else if(Application.platform==RuntimePlatform.Android){
					url=hitofude_data.settings.review_url_andorid;
				}

				if(input_button.type==button_controller.button_type.button_unlock_dan_yes){
					hitofude_data.save_data.set_dan_ready();
					//UI_window_tutorial.GetComponent<window_tutorial_controller>().hide ();
				}

				Application.OpenURL(url);



			}else if(input_button.type==button_controller.button_type.button_select_level){
			
				show_UI_play_level (input_button.level);
				//hide_UI_select_level ();

				//UI_animator.animation_start(UI_animation_controller.animation_type.play_level);
				
				change_state(UI_state.hitofude_normal);

			}else if(input_button.type==button_controller.button_type.button_select_dan){

				show_UI_play_dan (input_button.level);
				//hide_UI_select_dan ();

				//UI_animator.animation_start(UI_animation_controller.animation_type.play_dan);
				
				change_state(UI_state.hitofude_normal);

			}else if(input_button.type==button_controller.button_type.button_applipromotion){
				//ad.show_wall();

				string url=hitofude_data.settings.osusume_url;

				Application.OpenURL(url);

			}else if(input_button.type==button_controller.button_type.button_unlock_dan_no){

				//UI_window_tutorial.GetComponent<window_tutorial_controller>().hide ();
			}


		}
	}

	public void button_point_pressed(hitofude_point_controller pressed){

		if(is_button_available(button_controller.button_type.point)){
			if(hitofude_data.is_point_available(pressed)){

				//SE.play_audio(SE_controller.SE_list.dot);
				pressed.change_state(hitofude_point_controller.point_state.selected);
				hitofude_data.add_selected_line(pressed);
			
			}
		}
	}







	public void button_result_pressed(){
		
		hitofude_data.clear_data ();
		UI_label_large.GetComponent<time_count_controller> ().pause_count ();

		
		UI_button_undo.SetActive (false);
		UI_button_restart.SetActive (false);
		UI_button_hint.SetActive (false);
		UI_button_answer.SetActive (false);
		
		show_UI_result ();


		UI_button_touch_panel.GetComponent<button_touch_panel_controller>().disable_touch_panel();
		change_state (UI_state.result);
		
	}


	public void button_next_dan_stage_pressed(){



		UI_label_large.GetComponent<time_count_controller> ().pause_count ();
		//UI_window_dan_clear.SetActive (true);

		//print ();
		//UI_window_dan_clear.GetComponent<window_dan_clear_controller> ().show_window ((hitofude_data.current_dan_stage+1)+"/"+hitofude_data.settings.max_dan_stage+" Cleared", window_dan_clear_controller.animation_state.cleared);


		change_state(UI_state.dan_clear);

		StartCoroutine (hide_UI_window_dan_clear_timer(1.5f));

	}

	public void button_time_out_pressed(){

		//if()
			UI_window_dan_clear.GetComponent<window_dan_clear_controller> ().show_window (window_dan_clear_controller.animation_state.time_out);
			

			UI_window_dan_clear.SetActive (true);

			UI_button_home.SetActive (false);
			UI_button_undo.SetActive (false);
			UI_button_restart.SetActive (false);
			UI_button_hint.SetActive (false);
			UI_button_answer.SetActive (false);

			change_state (UI_state.time_out);

			//SE.play_audio (SE_controller.SE_list.time_out);

			StartCoroutine (hide_UI_window_dan_time_out_timer(1.5f));

		//}
	}


	public void button_touch_panel_pressed(Vector2 input_position){

		if(is_button_available(button_controller.button_type.touch_panel)){
			//print (input_position.x + "," + input_position.y);
			//print ("ok");
			hitofude_data.show_dragged_line (input_position);
		}

	}

	public void button_touch_panel_unpressed(){

		if(is_button_available(button_controller.button_type.touch_panel_unpressed)){
			hitofude_data.hide_dragged_line ();
		}

	}

	public void button_touch_panel_pinched(Vector2 input_position,float input_scale){
		
		if(is_button_available(button_controller.button_type.touch_panel)){
			scale_UI_play(input_position,input_scale);
		}
		
	}


	public void animation_end(ESCAPE.UI.UI_animation_controller.animation_type input_type){
		/*
		print ("animation_end:"+input_type);

		if (input_type == UI_animation_controller.animation_type.normal_mode||input_type == UI_animation_controller.animation_type.dan_mode) {

			hide_UI_title ();
		

			if(input_type == UI_animation_controller.animation_type.normal_mode){
				show_UI_buttons_select_level(hitofude_data.settings.max_button_level_shown,UI_buttons_select_level.Length);
				//scroll_level.reposition();
				//scroll_level.refresh();

				enable_UI_buttons_select_level();
			}else{
				show_UI_buttons_select_dan(hitofude_data.settings.max_button_level_shown,UI_buttons_select_dan.Length);
				//scroll_dan.reposition();
				//scroll_dan.refresh();
				enable_UI_buttons_select_dan();
			}

			change_state(UI_state.select_level);

		} else if(input_type==UI_animation_controller.animation_type.play_level){

			hide_UI_select_level();


		} else if(input_type==UI_animation_controller.animation_type.play_dan){

			hide_UI_select_dan();

		} else if(input_type==UI_animation_controller.animation_type.home){
			hide_UI_play();
			hide_UI_select_dan();
			hide_UI_select_level();
		

			change_state(UI_state.title);


		}*/

	}

	public void create_UI_buttons_select_level(){

		UI_buttons_select_level =new GameObject[hitofude_data.settings.max_level];

		for (int i =0;i<hitofude_data.settings.max_level;i++){
			GameObject temp = (GameObject) Instantiate (select_level_button);
			UI_buttons_select_level[i]=temp;

			UI_buttons_select_level_parent.add_gameobject(temp);

			temp.transform.localScale=new Vector3(1,1,1);
			//temp.GetComponent<Button>().ui=this;
			///temp.GetComponent<Button>().color.ui=this;
			//temp.GetComponent<Button>().change_label(i,(i+1).ToString(),hitofude_data.save_data.get_select_level_data(i),false);
		}

		UI_buttons_select_level_parent.gameObject.SetActive (false);
		
		
		//UI_buttons_select_level_parent.GetComponent<UIGrid> ().Reposition ();
		
	}



	
	public void create_UI_buttons_select_dan(){

		UI_buttons_select_dan =new GameObject[hitofude_data.settings.max_dan];

		for (int i =0;i<hitofude_data.settings.max_dan;i++){
			GameObject temp = (GameObject) Instantiate (select_level_button);

			UI_buttons_select_dan[i]=temp;

			//temp.transform.parent = UI_buttons_select_dan_parent.transform;

			UI_buttons_select_dan_parent.add_gameobject(temp);

			//temp.transform.localPosition=new Vector3(0,0,0);
			temp.transform.localScale=new Vector3(1,1,1);
			//temp.GetComponent<button_select_level_controller>().ui=this;
			//temp.GetComponent<button_select_level_controller>().color.ui=this;
			//temp.GetComponent<button_select_level_controller>().change_label(i,hitofude_data.settings.int_to_dan(i),hitofude_data.save_data.get_select_dan_data(i),true);
		}
		
		
		//UI_buttons_select_dan_parent.GetComponent<UIGrid> ().Reposition ();
		
	}

	public void refresh_UI_buttons_select_level(){

		foreach (GameObject temp in UI_buttons_select_level) {
		//	temp.GetComponent<button_select_level_controller>().set_state(hitofude_data.save_data);
		}


	}

	public void destroy_UI_buttons_select_level(){
		foreach (GameObject temp in UI_buttons_select_level) {
			Destroy(temp.gameObject);
		}

	}




	public void refresh_UI_buttons_select_dan(){
		foreach (GameObject temp in UI_buttons_select_dan) {
			//temp.GetComponent<button_select_level_controller>().set_state(hitofude_data.save_data);
		}
	}

	public void destroy_UI_buttons_select_dan(){
		foreach (GameObject temp in UI_buttons_select_dan) {
			Destroy(temp.gameObject);
		}
		
	}

	public void refresh_UI_button_applipromotion(bool is_title){

		Vector3 temp= UI_button_applipromotion.transform.localPosition;

		if (hitofude_data.settings.get_screen_type () == hitofude_settings.screen_type_enum.ipad_3_4) {



			//print ("aaaaa");

			if(is_title){

				UI_button_applipromotion.transform.localPosition= new Vector3(220,-50,temp.z);

			}else{
				UI_button_applipromotion.transform.localPosition= new Vector3(220,-130,temp.z);
			}
		
		}else if(hitofude_data.settings.get_screen_type () == hitofude_settings.screen_type_enum.iphone_4_2_3){
			UI_button_applipromotion.transform.localPosition= new Vector3(220,-50,temp.z);
		}


	}

	public void show_UI_title(){

	

		UI_background_title.SetActive (true);
		UI_buttons_title.SetActive (true);

		refresh_UI_buttons_title ();

		refresh_UI_button_applipromotion (true);

	}

	public void hide_UI_title(){
		UI_background_title.SetActive (false);
		UI_buttons_title.SetActive (false);

		UI_label_progress_level.GetComponent<UILabel> ().text = "";
		UI_label_progress_dan.GetComponent<UILabel> ().text = "";
	}



	public void show_UI_select_level(){

		//UI_label_select_level.GetComponent<depth_controller> ().change_layer (depth_controller.depth_layer.Background_2_label,UI_label_select_level.layer);

		UI_label_select_level.GetComponent<UILabel> ().text = "Select Level";

		UI_background_select_level.SetActive (true);
		UI_button_home.SetActive (true);
		UI_buttons_bottom_background.SetActive (true);

		refresh_UI_buttons_select_level ();
		UI_buttons_select_level_parent.enabled = true;
		UI_buttons_select_level_parent.initialize_position ();
		//scroll_level.reposition ();

		show_UI_buttons_select_level (0,hitofude_data.settings.max_button_level_shown);

		/*
		foreach (GameObject temp in UI_buttons_select_level) {
			temp.GetComponent<button_select_level_controller>().show_button();
		}*/


		//UI_buttons_select_level_parent.SetActive (true);
	
	
	}
	

	
	public void show_UI_select_dan(){

		//UI_label_large.GetComponent<UILabel> ().text = "";
		//UI_label_select_level.GetComponent<depth_controller> ().change_layer (depth_controller.depth_layer.Background_2_label,UI_label_select_level.layer);

		UI_label_select_level.GetComponent<UILabel> ().text = "IQ Test";

		UI_background_select_level.SetActive (true);
		UI_button_home.SetActive (true);
		UI_buttons_bottom_background.SetActive (true);

		refresh_UI_buttons_select_dan ();
		UI_buttons_select_dan_parent.enabled = true;
		UI_buttons_select_dan_parent.initialize_position ();

		//scroll_dan.reposition ();

		show_UI_buttons_select_dan(0,hitofude_data.settings.max_button_level_shown);

		/*
		foreach (GameObject temp in UI_buttons_select_dan) {
			temp.GetComponent<button_select_level_controller>().show_button();
		}*/



		//UI_buttons_select_dan_parent.SetActive (true);

	

	}

	/*
	public void hide_UI_select_level(){

		UI_background_select_level.SetActive (false);

		
	}
	*/

	


	public void show_UI_buttons_select_level(int input_min, int input_max){

		if(input_min<input_max&&input_min<UI_buttons_select_level.Length){
			int limit =input_max;

			if(limit>UI_buttons_select_level.Length){
				limit=UI_buttons_select_level.Length;
			}

			for(int i=input_min;i<limit;i++){

				UI_buttons_select_level[i].GetComponent<button_select_level_controller>().show_button();

			}
		}

		//scroll_level.panel.Refresh ();

	}

	public void enable_UI_buttons_select_level(){
		foreach (GameObject temp in UI_buttons_select_level) {
			temp.GetComponent<button_select_level_controller>().enable_button();
		}

	}

	public void hide_UI_select_level(){
		
		UI_background_select_level.SetActive (false);
		UI_label_select_level.GetComponent<UILabel> ().text = "";
		foreach (GameObject temp in UI_buttons_select_level) {
			temp.GetComponent<button_select_level_controller>().hide_button();
		}

		UI_buttons_select_level_parent.enabled = false;
		UI_buttons_bottom_background.SetActive (false);


		//UI_buttons_select_level_parent.SetActive (false);
	}
	
	public void show_UI_buttons_select_dan(int input_min, int input_max){
		
		if(input_min<input_max&&input_min<UI_buttons_select_dan.Length){
			int limit =input_max;
			
			if(limit>UI_buttons_select_dan.Length){
				limit=UI_buttons_select_dan.Length;
			}
			
			for(int i=input_min;i<limit;i++){
				UI_buttons_select_dan[i].GetComponent<button_select_level_controller>().show_button();
			}
		}

		//scroll_dan.panel.Refresh ();
	}

	public void enable_UI_buttons_select_dan(){
		foreach (GameObject temp in UI_buttons_select_dan) {
			temp.GetComponent<button_select_level_controller>().enable_button();
		}
	}

	public void hide_UI_select_dan(){

		UI_background_select_level.SetActive (false);

		UI_label_select_level.GetComponent<UILabel> ().text = "";

		foreach (GameObject temp in UI_buttons_select_dan) {
			temp.GetComponent<button_select_level_controller>().hide_button();
		}

		UI_buttons_select_dan_parent.enabled = false;
		UI_buttons_bottom_background.SetActive (false);
		
		//UI_buttons_select_dan_parent.SetActive (false);
		
		
	}





	public void show_UI_play_level(int input_level){

		UI_background_play.SetActive (true);

		//UI_label_large.SetActive (false);
		//UI_label_large.GetComponent<UILabel> ().text = "aa";

		//UI_label_large.GetComponent<depth_controller> ().change_layer (depth_controller.depth_layer.Background_1,UI_label_large.layer);

		//UI_label_large.GetComponent<depth_controller> ().change_layer (depth_controller.depth_layer.Button,UI_label_large.layer);
		//UI_label_small.GetComponent<depth_controller> ().change_layer (depth_controller.depth_layer.Button,UI_label_small.layer);
		//UI_label_turn.GetComponent<depth_controller> ().change_layer (depth_controller.depth_layer.Button,UI_label_turn.layer);

		UI_label_large.GetComponent<UILabel> ().text = "";
		UI_label_small.GetComponent<UILabel> ().text = "Level "+(input_level+1);
		UI_label_turn.GetComponent<UILabel> ().text = "Turn 1";


		UI_button_home.SetActive (true);
		UI_button_undo.SetActive (true);
		UI_button_restart.SetActive (true);
		UI_button_hint.SetActive (true);
		UI_button_answer.SetActive (true);

		//hitofude_data.current_level = input_level;
		hitofude_data.load_level (input_level);

		UI_camera.GetComponent<UICamera> ().stickyPress = false;
		//UI_camera_UI_2.GetComponent<UICamera> ().stickyPress = false;
		UI_button_touch_panel.GetComponent<button_touch_panel_controller>().enable_touch_panel();

		show_UI_window_tutorial_level(input_level);

		refresh_UI_button_applipromotion (false);


	}

	public void show_UI_window_tutorial_level(int input_level){

		if(!hitofude_data.save_data.has_tutorial_shown(input_level,false)){


			if(input_level==0){
				UI_window_tutorial.SetActive(true);
				//UI_window_tutorial.GetComponent<window_tutorial_controller>().show (0);
			}else if(input_level==1){
				UI_window_tutorial.SetActive(true);
				//UI_window_tutorial.GetComponent<window_tutorial_controller>().show (1);
			}else if(input_level==5){
				UI_window_tutorial.SetActive(true);
				//UI_window_tutorial.GetComponent<window_tutorial_controller>().show (2);
			}else if(input_level==10){
				UI_window_tutorial.SetActive(true);
				//UI_window_tutorial.GetComponent<window_tutorial_controller>().show (3);

			}else if(input_level==15){

				UI_window_tutorial.SetActive(true);
				//UI_window_tutorial.GetComponent<window_tutorial_controller>().show (4);

			}else if(input_level==20){

				UI_window_tutorial.SetActive(true);
				//UI_window_tutorial.GetComponent<window_tutorial_controller>().show (5);
			}
		}
		
	}

	/*
	public void hide_UI_hitofude_level(){

		UI_label_large.GetComponent<time_count_controller> ().stop_count ();
		UI_background_play.SetActive (false);

		UI_button_home.SetActive (false);
		UI_button_undo.SetActive (false);
		UI_button_restart.SetActive (false);
		UI_button_hint.SetActive (false);
		UI_button_answer.SetActive (false);


	}*/



	public void show_UI_play_dan(int input_level){
		
		print (input_level+"-dan");

		UI_background_play.SetActive (true);
		/*
		UI_label_large.GetComponent<depth_controller> ().change_layer (depth_controller.depth_layer.Button,UI_label_large.layer);
		UI_label_small.GetComponent<depth_controller> ().change_layer (depth_controller.depth_layer.Button,UI_label_small.layer);
		UI_label_turn.GetComponent<depth_controller> ().change_layer (depth_controller.depth_layer.Button,UI_label_turn.layer);
		*/
		/*
		if(input_level!=0){

			UI_label_large.GetComponent<time_count_controller> ().start_count (hitofude_data.settings.dan_stage_remaining_time_in_seconds);

		}*/
		UI_label_small.GetComponent<UILabel> ().text = "IQ "+hitofude_data.settings.int_to_dan(input_level)+"  1/"+hitofude_data.settings.max_dan_stage;
		UI_label_turn.GetComponent<UILabel> ().text = "Turn 1";

		UI_label_large.GetComponent<ngui_screen_adjust> ().type = ngui_screen_adjust.object_type.large_label_dan;
		UI_label_large.GetComponent<ngui_screen_adjust> ().reposition ();

		UI_label_small.GetComponent<ngui_screen_adjust> ().type = ngui_screen_adjust.object_type.small_label_dan;
		UI_label_small.GetComponent<ngui_screen_adjust> ().reposition ();

		UI_button_home.SetActive (true);
		UI_button_undo.SetActive (true);
		UI_button_restart.SetActive (true);
		UI_button_hint.SetActive (true);
		UI_button_answer.SetActive (true);
		
		hitofude_data.load_dan (input_level);

		UI_camera.GetComponent<UICamera> ().stickyPress = false;
		//UI_camera_UI_2.GetComponent<UICamera> ().stickyPress = false;
		UI_button_touch_panel.GetComponent<button_touch_panel_controller>().enable_touch_panel();
		//UI_camera_UI_2.GetComponent<UICamera> ().stickyPress = false;

		//show_UI_window_tutorial_dan (input_level);

		if (!show_UI_window_tutorial_dan (input_level)) {
			
			UI_label_large.GetComponent<time_count_controller> ().start_count (hitofude_data.settings.dan_stage_remaining_time_in_seconds);

			//UI_label_small.GetComponent<UILabel> ().text =

		} else {
			UI_label_large.GetComponent<time_count_controller> ().set_text(hitofude_data.settings.dan_stage_remaining_time_in_seconds);
		}

		refresh_UI_button_applipromotion (false);
	}

	public bool show_UI_window_tutorial_dan(int input_level){

		bool output = false;

		if(!hitofude_data.save_data.has_tutorial_shown(input_level,true)){
			
			
			if(input_level==0){
				UI_window_tutorial.SetActive(true);
				//UI_window_tutorial.GetComponent<window_tutorial_controller>().show (6);
			
				output=true;
			}
		}

		return output;
		
	}

	public void show_UI_hitofude_dan_again(){


		//UI_label_large.SetActive (true);
		//UI_label_large.GetComponent<depth_controller> ().change_layer (depth_controller.depth_layer.Button,UI_label_large.layer);
		UI_label_large.SetActive (true);
		UI_label_large.GetComponent<time_count_controller> ().resume_count ();
		UI_label_small.GetComponent<UILabel> ().text =  hitofude_data.settings.int_to_dan(hitofude_data.current_dan)+" "+(hitofude_data.current_dan_stage+2)+"/"+hitofude_data.settings.max_dan_stage;

		UI_label_turn.GetComponent<UILabel> ().text = "Turn 0";

		UI_label_large.GetComponent<ngui_screen_adjust> ().type = ngui_screen_adjust.object_type.large_label_dan;
		UI_label_large.GetComponent<ngui_screen_adjust> ().reposition ();

		UI_label_small.GetComponent<ngui_screen_adjust> ().type = ngui_screen_adjust.object_type.small_label_dan;
		UI_label_small.GetComponent<ngui_screen_adjust> ().reposition ();

		UI_button_home.SetActive (true);
		UI_button_undo.SetActive (true);
		UI_button_restart.SetActive (true);
		UI_button_hint.SetActive (true);
		UI_button_answer.SetActive (true);

		hitofude_data.load_dan_again ();

		UI_camera.GetComponent<UICamera> ().stickyPress = false;
		//UI_camera_UI_2.GetComponent<UICamera> ().stickyPress = false;
		UI_button_touch_panel.GetComponent<button_touch_panel_controller>().enable_touch_panel();
		//UI_camera_UI_2.GetComponent<UICamera> ().stickyPress = false;
	}

	public void hide_UI_play(){
		hitofude_data.clear_data();
		scale_UI_play (new Vector2(0,0),1);
		
		UI_label_large.GetComponent<ngui_screen_adjust> ().type = ngui_screen_adjust.object_type.large_label_level;
		UI_label_large.GetComponent<ngui_screen_adjust> ().reposition ();
		
		UI_label_small.GetComponent<ngui_screen_adjust> ().type = ngui_screen_adjust.object_type.small_label_level;
		UI_label_small.GetComponent<ngui_screen_adjust> ().reposition ();


		UI_label_large.GetComponent<UILabel> ().text = "";
		UI_label_small.GetComponent<UILabel> ().text = "";
		UI_label_turn.GetComponent<UILabel> ().text = "";

		UI_background_play.SetActive (false);
		
		UI_button_home.SetActive (false);
		UI_button_undo.SetActive (false);
		UI_button_restart.SetActive (false);
		UI_button_hint.SetActive (false);
		UI_button_answer.SetActive (false);
		
		hide_UI_result ();

		UI_camera.GetComponent<UICamera> ().stickyPress = true;
		//UI_camera_UI_2.GetComponent<UICamera> ().stickyPress = true;
		UI_button_touch_panel.GetComponent<button_touch_panel_controller>().disable_touch_panel();
		
	}

	public void scale_UI_play(Vector2 center, float scale_factor ){
		UI_touch_panel_scale.localScale = new Vector3 (scale_factor,scale_factor,1);
		UI_touch_panel_position.localPosition = new Vector3 (center.x,center.y,0);

		if(hitofude_data.settings.fix_scale_lines_and_points){
			hitofude_data.rescale_lines_and_points(1/scale_factor);
		}
	}

	/*
	public void hide_UI_hitofude_dan(){
		
		UI_label_large.GetComponent<time_count_controller> ().stop_count ();
		UI_background_play.SetActive (false);
		
		UI_button_home.SetActive (false);
		UI_button_undo.SetActive (false);
		UI_button_restart.SetActive (false);
		UI_button_hint.SetActive (false);
		UI_button_answer.SetActive (false);
		
		hitofude_data.destroy_all ();
		
	}*/


	public void show_UI_window_hint(){
	
		UI_window_hint.SetActive (true);
	}

	public void hide_UI_window_hint(){
		UI_window_hint.SetActive (false);
	}

	public void show_UI_window_answer(){

		UI_window_answer.SetActive (true);
	}
	
	public void hide_UI_window_answer(){

		UI_window_answer.SetActive (false);
	}




	public void show_UI_result(){

		UI_window_result.SetActive (true);

		UI_window_result_controller.show_result (hitofude_data.hitofude_result());

		UI_window_result_comment.GetComponent<comment_switcher> ().show (hitofude_data.current_level);

		if((!hitofude_data.is_dan)&&(hitofude_data.current_level==24)&&(!hitofude_data.save_data.is_dan_ready())){
			print ("is_dan_ready"+hitofude_data.save_data.is_dan_ready());

			if(is_unlock_url_found){
				UI_window_tutorial.SetActive(true);
				//UI_window_tutorial.GetComponent<window_tutorial_controller>().show(7);
			}
		}

		if (hitofude_data.is_dan) {

		}else{
			//print (hitofude_data.hitofude_result());

		}



		//if((Application.systemLanguage != SystemLanguage.Japanese)&&hitofude_data.settings.show_interstitial_automatically){
		if(hitofude_data.settings.show_interstitial_automatically){
			//print ("result shown: "+ result_shown_count);

			result_shown_count++;

			if(result_shown_count>=hitofude_data.settings.interstitial_interval){
				result_shown_count=0;
				//ad.show_intersititial_google();

				ad.show_wall_result();
			}else{

			}

		}

		UI_buttons_result.SetActive (true);


	}

	public void hide_UI_result(){


		
		UI_window_result.GetComponent<window_result_controller> ().hide_result ();

		UI_window_result.SetActive (false);
		UI_buttons_result.SetActive (false);

		ad.hide_icon ();
	}
	

	IEnumerator hide_UI_window_dan_clear_timer(float time){
		yield return new WaitForSeconds(time);

		hitofude_data.clear_data ();
		hide_UI_window_dan_clear ();
		show_UI_hitofude_dan_again ();

		change_state (UI_state.hitofude_normal);
	}

	IEnumerator hide_UI_window_dan_time_out_timer(float time){

		yield return new WaitForSeconds(time);

		hide_UI_window_dan_time_out();

		change_state (UI_state.hitofude_normal);
		 
		button_controller temp =new button_controller();
		temp.type = button_controller.button_type.button_home_no_SE;
		button_pressed (temp);

	}

	public void hide_UI_window_dan_clear(){
		UI_window_dan_clear.SetActive (false);
	}

	public void hide_UI_window_dan_time_out(){
		UI_window_dan_clear.SetActive (false);
	}

	public button_select_level_controller.level_state result_to_level_state(ESCAPE.SYSTEM.Const.Data.Game_result input_result){
		button_select_level_controller.level_state output = button_select_level_controller.level_state.Zero;

		/*
		if(input_result==window_result_controller.result.perfect){
			output=button_select_level_controller.level_state.Three;
		}else if(input_result==window_result_controller.result.fantastic){
			output=button_select_level_controller.level_state.Two;
		}else if(input_result==window_result_controller.result.good){
			output=button_select_level_controller.level_state.One;
		}*/

		return output;

	}
	
	public void button_pressed_animation_start(button_controller input_button, int input_diameter){

		GameObject temp = (GameObject)Instantiate ( (GameObject)button_pressed_animation);

		temp.transform.parent = button_pressed_animation_parent.transform;

		/*temp.GetComponent<button_pressed_animation_controller> ().ui = this;
		temp.GetComponent<button_pressed_animation_controller> ().child.ui = this;
		temp.GetComponent<button_pressed_animation_controller> ().circle.ui = this;
		temp.GetComponent<button_pressed_animation_controller> ().animation_start (button_position_to_screen_position(input_button.transform),input_diameter,input_button);
	*/
	}


	public void change_state(UI_state input_state){


		prev_state = state;
		state = input_state;
	}

	public void change_state_skip_prev(UI_state input_state){
		state = input_state;
	}

	public Vector2 button_position_to_screen_position(Transform input_transform){

		Vector3 temp = world_position_to_screen_position (input_transform.position);

		return new Vector2 (temp.x,temp.y);
	}

	public Vector3 world_position_to_screen_position( Vector3 input_point){

		Vector3 temp=UI_camera.WorldToScreenPoint (input_point);

		return new Vector3((temp.x-(Screen.width/2)) *(640.0f/Screen.width),(temp.y-(Screen.height/2))*(640.0f/Screen.width),0);
	}

	public void refresh_UI_buttons_title(){

		UI_label_progress_level.GetComponent<UILabel>().text = hitofude_data.save_data.get_level_progress ();
		UI_label_progress_dan.GetComponent<UILabel>().text = hitofude_data.save_data.get_dan_progress ();

		if (hitofude_data.save_data.is_dan_ready()) {

			UI_button_mode_dan.GetComponent<BoxCollider>().enabled=true;
			UI_button_mode_dan.GetComponent<UIWidget>().color= new Color(255,255,255,255);

		}


	}


}
