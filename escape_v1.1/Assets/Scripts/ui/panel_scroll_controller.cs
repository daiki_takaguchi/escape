﻿using UnityEngine;
using System.Collections;

namespace ESCAPE.UI{
public class panel_scroll_controller : MonoBehaviour {

		// Use this for initialization

		public UIPanel panel;
		public UIDraggablePanel drag;
		public Re_bound_panel rebound;

		public bool reposition_now=false;

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			/*
			if(reposition_now){
				reposition ();
				reposition_now=false;
			}*/
		
		}


		public void reposition(){

			Vector3 temp = panel.transform.localPosition;
			panel.transform.localPosition = new Vector3 (temp.x,0,temp.z);

			rebound.reclip ();

		}

		public void refresh(){
			drag.Press (true);
			drag.Press (false);
		}
	}
}
