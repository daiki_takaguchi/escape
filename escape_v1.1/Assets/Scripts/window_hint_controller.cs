﻿using UnityEngine;
using System.Collections;

public class window_hint_controller : MonoBehaviour {

	// Use this for initialization

	public UI_controller ui;

	public UILabel main_text;
	public UILabel remaining_time;

	public UIWidget yes_back;
	public UIWidget yes_fore;
	public UIWidget yes_label;

	public BoxCollider box;

	public bool is_hint;
	public bool is_button_available=false;

	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
		//show_remaining_time ();
	}

	void OnEnable() {
		is_button_available=false;
		box.enabled=false;
		yes_fore.color= ui.hitofude_data.settings.button_unavailable;
		yes_label.color= ui.hitofude_data.settings.button_unavailable_label;
		remaining_time.text ="";

		//show_remaining_time ();

	}
	/*
	public void show_remaining_time(){

		if(!is_button_available){
			long time =0;

			if(is_hint){
				time = ui.hitofude_data.save_data.remaining_time_hint ();
			}else{
				time = ui.hitofude_data.save_data.remaining_time_answer ();
			}
				if(time==0){

					if(!is_button_available){
						is_button_available=true;
						box.enabled=true;
						yes_fore.color= ui.hitofude_data.settings.white;
						yes_label.color= ui.hitofude_data.settings.base_color;
						remaining_time.text ="";

						if(	GameObject.FindObjectOfType<ad_controller> ().is_japanese_function()){
							if(is_hint){
								main_text.text="ヒントを見る";
							}else{
								main_text.text="回答を見る";
							}
						}else{

							if(is_hint){
								main_text.text="See Hint?";
							}else{
								main_text.text="See Answer?";
							}

						}
					}
				}else{
					long hour = (time / 3600)%60;
					long minute = (time / 60)%60;
					long second = time %60;
					if(	GameObject.FindObjectOfType<ad_controller> ().is_japanese_function()){
						remaining_time.text = "あと " +hour.ToString ("D2") +":" +minute.ToString ("D2") +":"+ second.ToString ("D2");
					}else{
						remaining_time.text = "Remaining time: " +hour.ToString ("D2") +":" +minute.ToString ("D2") +":"+ second.ToString ("D2");
					}

				}

			

			

		}
	}*/
}
