﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ESCAPE.UI;
using ESCAPE.DATA;


namespace ESCAPE.SYSTEM{
public class Game_state_manager: MonoBehaviour {

		//public Main main;

		public int current_level;
		public int current_dan;
		public List<int> current_dan_list;
		public int current_dan_stage;

		public int turn;
		public int penalty;
		public enum Game_mode{
			normal,
			dan
		}
		//public bool is_base_reset_done=false;

		public Game_mode game_mode;

		public float game_over_start_time;
		public float time_out_start_time;
		public float dan_stage_clear_start_time;

		public Vector3 mouse_pressed_position;

		public Const.Data.Game_state game_state;

		public Button.button_type pressed_button;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {

			if (game_state == Const.Data.Game_state.game_over) {

				if (Time.time > game_over_start_time + Main.main_static.settings.game_over_wait_time) {
					game_state = Const.Data.Game_state.initializing;
					Main.main_static.ui.UI_game_over.SetActive (false);

					//Main.main_static.ui.UI_window_dan_clear.SetActive (false);
					//reset ();

					Button temp=gameObject.AddComponent<Button>();

					temp.type=Button.button_type.button_home_no_SE;

					Main.main_static.ui.button_pressed(temp);

					Destroy(temp);
				

				} else {

				}
			} else if(game_state==Const.Data.Game_state.time_out){
				if (Time.time > time_out_start_time +  Main.main_static.settings.game_over_wait_time) {
					game_state = Const.Data.Game_state.initializing;
					Main.main_static.ui.UI_window_dan_clear.SetActive (false);
					Main.main_static.ui.hide_UI_window_dan_time_out();

					
				} else {
					
				}

			} else if(game_state==Const.Data.Game_state.dan_stage_clear){
				if (Time.time > dan_stage_clear_start_time + Main.main_static.settings.dan_clear_wait_time) {
					game_state = Const.Data.Game_state.initializing;
					Main.main_static.ui.UI_window_dan_clear.SetActive (false);

					load_dan_again();

					
				} else {
					
				}


			}
		
		}

		public void load_level(int input_level){


			initialize ();
			game_mode = Game_mode.normal;
			current_level = input_level;
			Main.main_static.loader.load_data (input_level);
			game_state = Const.Data.Game_state.play;

		}

		public void load_dan(int input_dan){
			initialize ();
			game_mode = Game_mode.dan;
			current_dan = input_dan;
			current_dan_stage = 0;

			current_dan_list= pick_random_ints (range_to_ints (Main.main_static.settings.dan_range_min[input_dan], Main.main_static.settings.dan_range_max[input_dan]),  Main.main_static.settings.max_dan_stage);

			Main.main_static.loader.load_data (current_dan_list[current_dan_stage]);
			game_state = Const.Data.Game_state.play;

			foreach(int i in current_dan_list){
				print (i);
			}

		
		}

		public void load_dan_again(){

			current_dan_stage++;
			Main.main_static.ui.UI_label_small.GetComponent<UILabel> ().text = "IQ "+Main.main_static.settings.int_to_dan(current_dan)+"  "+(current_dan_stage+1)+"/"+Main.main_static.settings.max_dan_stage;

			//is_base_reset_done=false;

			turn=0;

			mouse_pressed_position=new Vector3(0,0,0);
			Main.main_static.ui.UI_label_large.GetComponent<time_count_controller> ().resume_count();
			Main.main_static.loader.load_data (current_dan_list[current_dan_stage]);
			game_state = Const.Data.Game_state.play;
			
		}

		public void initialize(){

			game_state = Const.Data.Game_state.initializing;
			current_level=0;
			current_dan_stage=0;
			turn=0;
			penalty = 0;
			mouse_pressed_position=new Vector3(0,0,0);
			Main.main_static.block_image_list.gameObject.SetActive (true);

			//is_base_reset_done=false;

			//pressed_button=Button.button_type.block_image;
		}
	/*
		public void change_game_state(Const.Data.Game_state input_game_state){

			if (input_game_state == Const.Data.Game_state.game_over) {

			}else if(input_game_state==Const.Data.Game_state.game_claer){

			}
		}*/

		public void set_mouse_pressed_position(){
			mouse_pressed_position = Main.get_mouse_position ();
		}

		public Vector3 get_mouse_pressed_position(){
			return mouse_pressed_position ;
		}


		public void game_over(){
			print ("game over");
			Main.main_static.ui.SE.play_audio (SE_controller.SE_list.time_out);
			//Main.main_static.ui.UI_window_dan_clear.GetComponent<window_dan_clear_controller> ().show_window (window_dan_clear_controller.animation_state.game_over);
			Main.main_static.ui.UI_game_over.SetActive (true);
			game_over_start_time = Time.time;
			game_state=Const.Data.Game_state.game_over;
		}

		public void time_out(){
			print ("game over");
			Main.main_static.ui.SE.play_audio (SE_controller.SE_list.time_out);
			Main.main_static.ui.UI_window_dan_clear.GetComponent<window_dan_clear_controller> ().show_window (window_dan_clear_controller.animation_state.time_out);
			time_out_start_time = Time.time;
			game_state=Const.Data.Game_state.time_out;
		}



		/*
		public game_over_end(float time){


		}*/

		public void game_clear(){

			if (game_mode == Game_mode.normal) {
				Main.main_static.save_data.set_select_level_data (current_level, Const.Data.result_to_level_state (get_game_result ()));
				Main.main_static.save_data.set_select_level_data (current_level + 1, Const.Ui.Level_state.Zero);
				Main.main_static.block_image_list.gameObject.SetActive (false);
				Main.main_static.ui.unrefreshed_select_buttons_level.Add (current_level);
				Main.main_static.ui.unrefreshed_select_buttons_level.Add (current_level + 1);
				Main.main_static.ui.button_result_pressed ();
				

			} else {

				if(current_dan_stage<Main.main_static.settings.max_dan_stage-1){
					Main.main_static.ui.UI_label_large.GetComponent<time_count_controller> ().pause_count();
					Main.main_static.ui.SE.play_audio (SE_controller.SE_list.clear);
					//Main.main_static.ui.UI_window_dan_clear.SetActive(true);
					Main.main_static.ui.UI_window_dan_clear.GetComponent<window_dan_clear_controller> ().show_window (window_dan_clear_controller.animation_state.cleared);
					dan_stage_clear_start_time = Time.time;

					game_state=Const.Data.Game_state.dan_stage_clear;

				}else{
					
					Main.main_static.save_data.set_select_dan_data(current_dan,Const.Data.result_to_level_state(get_game_result()));
					Main.main_static.save_data.set_select_dan_data(current_dan+1,Const.Ui.Level_state.Zero);
					Main.main_static.block_image_list.gameObject.SetActive (false);
					Main.main_static.ui.unrefreshed_select_buttons_dan.Add (current_dan);
					Main.main_static.ui.unrefreshed_select_buttons_dan.Add (current_dan + 1);
					Main.main_static.ui.button_result_pressed();
				}
			
			}

		}

		public void next_level(){
			if(game_mode==Game_mode.dan){
				
				Button temp = new Button();
				temp.type=Button.button_type.button_home_no_SE;
				Main.main_static.ui.button_pressed (temp);
			}else{
				if(current_level==(Main.main_static.settings.max_level-1)){
					Button temp = new Button();
					temp.type=Button.button_type.button_home_no_SE;
					
					Main.main_static.ui.button_pressed (temp);
					
				}else{
					Main.main_static.ui.hide_UI_result();
					Main.main_static.ui.show_UI_play_level(current_level+1);
					
				}
			}
		}

		public void check_if_move(){
			Vector3 temp = Main.get_mouse_position () - mouse_pressed_position;
			Const.Data.Block_direction direction = Const.Data.vector3_to_direction (temp, 64);
		
			if (direction != Const.Data.Block_direction.none) {

				print("tried to move: "+direction);

				if(Main.main_static.data.block_group_list.selected_group!=null){
					move_if_movable (direction);
				
					set_mouse_pressed_position ();
				}
			}
		}


		public void move_if_movable(Const.Data.Block_direction input_direction){

			if(is_ok_to_move()){
				pressed_button=Button.button_type.block_image;
				if (input_direction != Const.Data.Block_direction.none) {
					if (Main.main_static.data.block_group_list.selected_group.check_movability (input_direction)) {
						//Main.main_static.data.block_group_list.selected_group.move_end(input_direction);
						//Main.main_static.ui.SE.play_audio (SE_controller.SE_list.dot);
						set_turn (1);
						Main.main_static.ui.SE.play_audio (SE_controller.SE_list.dot);
						Main.main_static.data.block_group_list.move_end (input_direction);

					}
				};
			}

		}

		public bool is_ok_to_move(){
			bool output = true;

			if(game_state==Const.Data.Game_state.game_over){
				output=false;
			}

			if(game_state==Const.Data.Game_state.game_over){
				output=false;
			}

			if(game_state==Const.Data.Game_state.dan_stage_clear){
				output=false;
			}


			return output;
		}

		public void touch_panel_dragged(){
			Vector3 temp = Main.get_mouse_position () - mouse_pressed_position;
			Const.Data.Block_direction direction = Const.Data.vector3_to_direction (temp, 64);
			
			if (direction != Const.Data.Block_direction.none) {
				
				print("tried to move: "+direction);
				
				if(Main.main_static.data.block_group_list.selected_group!=null){
					move_if_movable (direction);
					
					set_mouse_pressed_position ();
				}
			}
		}


		public void set_turn(int input_int){

			if (input_int == 0) {
				turn = 1;
			} else {
				turn+=input_int;
			}

			Main.main_static.ui.UI_label_turn.GetComponent<UILabel> ().text = "Turn " + turn;

		}

		public void move_end(){

		}

		public void select_group(Block_group input_group){
			Main.main_static.ui.SE.play_audio (SE_controller.SE_list.dot);
			Main.main_static.data.block_group_list.select_group (input_group);
		}

		public void undo(){


			if(is_ok_to_move()){
				if(Main.main_static.data.block_group_list.undo ()){
					increment_penalty();
					pressed_button = Button.button_type.button_undo;
					
					set_turn (-1);
				}

			}

		}

		public void reset(){

			if (is_ok_to_move ()) {
				increment_penalty();
				pressed_button = Button.button_type.button_restart;

				Main.main_static.data.block_base_list.reset();

				Main.main_static.data.block_group_list.reset ();

				set_turn (0);
			}

		}

		public void increment_penalty(){
			if(game_state!=Const.Data.Game_state.initializing){
				penalty++;
			}
		}

		public Const.Data.Game_result get_game_result(){

			Const.Data.Game_result output = Const.Data.Game_result.perfect;

			if (penalty == 1) {
				output = Const.Data.Game_result.fantastic;
			} else if(penalty > 1) {
				output = Const.Data.Game_result.good;
			}

			return output;
		}

		public List<int> range_to_ints(int input_min, int input_max){
			
			List<int> output = new List<int>();
			if(input_max>=input_min){
				output = new List<int>();
				
				//int count=0;
				for(int i= input_min ;i<=input_max; i++){
					output.Add (i);
					//count++;
				}
			}
			
			return output;
			
		}


		public List<int> pick_random_ints (List<int> input_ints,int number_of_pick){
			List<int> output=  new List<int>();
			List<int> remaining = input_ints;
			
			if(input_ints.Count>=number_of_pick){
				
				for (int i=1;i<=number_of_pick;i++){
					
					int picked_int=input_ints[0];
					int picked_slot=0;
					
					if(remaining.Count!=0){
						
						picked_slot=Random.Range(0,remaining.Count);
						//print (picked_slot);
						picked_int=remaining[picked_slot];
						remaining.Remove (picked_int);
					}
					
					output.Add(picked_int);
				}
				
			}else{
				
			}
			
			return output;
			
		}



	}
}