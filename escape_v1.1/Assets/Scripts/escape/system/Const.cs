﻿using UnityEngine;
using System.Collections;

namespace ESCAPE.SYSTEM{

	public class Const {

		public class Ui{
			public enum Level_state{
				Locked,
				Zero,
				One,
				Two,
				Three
			};





		}

		public class Data{

			public enum Game_result{
				good,
				fantastic,
				perfect
			}



			public enum Game_state{
				initializing,
				play,
				moving_block,
				game_over,
				game_claer,
				time_out,
				dan_stage_clear
			}



			public enum Block_base_type{
				empty,
				wall,
				lock_base,
				gate,
				locked_gate
			}


			public enum Block_base_state{
				empty,
				gate_closed,
				non_empty
			}

			public enum Block_group_type{
				normal,
				killer,
				number,
				key,
				human
			}

			public enum Block_group_state{
				none,
				selected,
				moving
			}

			public enum Block_direction{
				none,
				left,
				right,
				up,
				down
			}

			public static Block_group_type int_to_block_group_type(int input_int){
				
				Block_group_type output = Block_group_type.normal;

				if(input_int==0){
					output=Block_group_type.normal;
				}else if(input_int==1){
					output=Block_group_type.killer;
				}else if(input_int==2){
					output=Block_group_type.number;
				}else if(input_int==3){
					output=Block_group_type.key;
				}
				
				return output;
				
			}

			public static int direction_to_int(Block_direction input_direction){

				int output = 0;


				if(input_direction==Block_direction.left){
					output=0;
				}else if(input_direction==Block_direction.right){
					output=1;

				}else if(input_direction==Block_direction.up){
					output=2;

				}else if(input_direction==Block_direction.down){
					output=3;

				}

				return output;

			}

			public static Block_direction int_to_direction(int input_int){
				
				Block_direction output = Block_direction.none;
				
				
				if(input_int==0){
					output=Block_direction.left;
				}else if(input_int==1){
					output=Block_direction.right;
					
				}else if(input_int==2){
					output=Block_direction.up;
					
				}else if(input_int==3){
					output=Block_direction.down;
					
				}
				
				return output;
				
			}

			public static Block_direction get_opposite_direction(Block_direction input_direction){
				
				Block_direction output = Block_direction.none;
				
				
				if(input_direction==Block_direction.left){
					output=Block_direction.right;
				}else if(input_direction==Block_direction.right){
					output=Block_direction.left;
					
				}else if(input_direction==Block_direction.up){
					output=Block_direction.down;
					
				}else if(input_direction==Block_direction.down){
					output=Block_direction.up;
					
				}
				
				return output;
				
			}

			public static Block_direction vector3_to_direction(Vector3 input_vector, float threshold){
				
				//int output = 0;
				Block_direction output = Block_direction.none;
				//float threshold =  64; 

				if (Mathf.Abs (input_vector.x) > Mathf.Abs (input_vector.y)) {
					if(Mathf.Abs (input_vector.x)>threshold){
						if(input_vector.x>0){
							output=Block_direction.right;
						}else{
							output=Block_direction.left;
						}
					}


				} else {
					if(Mathf.Abs (input_vector.y)>threshold){
						if(input_vector.y>0){
							output=Block_direction.up;
						}else{
							output=Block_direction.down;
						}
					}
				}

				
				return output;
				
			}

			public static Const.Ui.Level_state result_to_level_state(Const.Data.Game_result input_result){
				Const.Ui.Level_state output = Const.Ui.Level_state.Zero;
				
				
				if(input_result==Const.Data.Game_result.perfect){
					output=Const.Ui.Level_state.Three;
				}else if(input_result==Const.Data.Game_result.fantastic){
					output=Const.Ui.Level_state.Two;
				}else if(input_result==Const.Data.Game_result.good){
					output=Const.Ui.Level_state.One;
				}
				
				return output;
				
			}



		
		}

	}
} 
