﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ESCAPE.DATA;


namespace ESCAPE.SYSTEM{
	

	public class Settings : MonoBehaviour {

		public enum System_language{
			system,
			jp,
			en
		}
		public System_language system_language;

		public bool is_load_test;

		
		public bool clear_now=false;
		
		public bool is_test=false;
		
		public bool clear_data=false;
		
		public int max_level;
		public int max_dan;
		public int dan_unlock_level;
		public string dan_unlock_url ="http://ketcha.eco.to/hitofude2/re.txt";
		
		//public int ad_result_interval;

		public float game_over_wait_time=3.0f;
		public float dan_clear_wait_time=1.0f;
		
		public string tweet_comment_iOS;
		public string tweet_url_iOS;
		
		public string tweet_comment_android;
		public string tweet_url_android;
		
		public string review_url_iOS;
		public string review_url_andorid;
		
		public string osusume_url_iOS="http://ketcha.eco.to/index.html";
		public string osusume_url_android="http://ketcha.eco.to/index.html";
		
		public int interstitial_interval;
		
		public int dan_stage_remaining_time_in_seconds=300;
		
		public int hint_interval_in_seconds=7200;
		public int answer_interval_in_seconds=86400;
		
		
		[HideInInspector]public bool disable_interstitial=false;
		
		

		public bool is_pinch_test=false;
		
		
		
		public bool is_line_collider_available=false;
		public bool reposition_aftar_pinch=false;
		
		public bool fit_hitofude_shape=false;
		public bool fix_scale_lines_and_points=false;
		public bool move_on_border=false;
		public float border_ratio;
		public float move_speed_pixel_in_second;
		public bool move_by_drag=false;
		//public bool show_arrow_when_move=false;
		public bool make_line_gray_when_quest=false;
		public bool blink_arrow_when_wrong_direction=false;
		public int blink_arrow_times;
		public float blink_arrow_interval;
		public bool blink_label_when_wrong_turn=false;
		public int blink_label_times;
		public float blink_label_interval;
		public bool ripple_current_point=false;
		public float ripple_current_point_max_diameter_ratio;
		//public float ripple_current_point_maintain_time;
		public float ripple_current_point_end_time;
		
		public float button_select_level_click_detection_limit_time;
		public bool apply_click_detection_limit_time_to_all_buttons;
		public float button_select_level_click_detection_limit_pixel;
		public float button_applipromotion_click_detection_delay_time;
		
		
		
		//public bool jump_hitofude_shape_when_button_input=false;
		//public float jump_wating_time=6.0f;
		
		
		
		
		[HideInInspector] public int max_dan_stage=5;
		
		
		public string[] dan_list;
		
		public int[] dan_range_max;
		public int[] dan_range_min;
		
		public int line_width;
		public int line_collider_width;
		public int line_number_pixel;
		public int point_diameter;
		public int point_collider_diameter;
		public int arrow_height;
		public int arrow_width;
		
		public float max_touch_scale;

		public List<Color> block_color;
		
		
		public Color base_color;
		public Color white;
		public Color red;
		public Color blue;
		
		public Color button_unavailable;
		public Color button_unavailable_label;
		public Color point_unavailable;
		
		
		public int button_mode_ripple_diameter;
		public int button_level_ripple_diameter;
		public int button_home_ripple_diameter;
		public int button_window_ripple_diameter;
		public int button_next_ripple_diameter;
		
		
		public float button_pressed_animation_wait_time;
		
		[HideInInspector] public int max_button_level_shown=35;
		
		public int hitofude_width_iphone5 = 500;
		public int hitofude_width_iphone4 = 450;
		public int hitofude_width_ipad = 350;
		
		public int hitofude_width_android_9_16 = 400;
		
		
		[HideInInspector] public int drag_margine_iphone5 = 60;
		
		
		
		public int hitofude_offset_y_iphone4 = 25;
		public int hitofude_offset_y_ipad = 50;
		
		public int hitofude_offset_y_android_9_16 =  -10;
		
		public enum screen_type_enum{
			initalizing,
			iphone_5_9_16,
			iphone_4_2_3,
			ipad_3_4
			
		}
		
		public screen_type_enum screen_type=screen_type_enum.initalizing;

		public bool is_japanese(){
			bool output=(Application.systemLanguage==SystemLanguage.Japanese);

			switch (system_language){
				case System_language.jp:

					output=true;
					break;
				case System_language.en:
					output=false;
					break;
			}


			return output;
		}
		
		void Start () {
			screen_type = get_screen_type ();
		}
		
		
		
		// Update is called once per frame
		void Update () {

			if(clear_now){
				Main.main_static.game_state_manager.game_clear();
				clear_now=false;

			}
			
		}
		
		public int dan_to_level(int input_dan){
			
			return Random.Range (dan_range_min [input_dan], dan_range_max [input_dan] + 1);
			
		}
		
		
		public string int_to_dan(int input_dan){
			return dan_list [input_dan];
		}
		
		public bool is_dan_available(int input_dan){
			bool output = false;
			
			if(input_dan<=max_dan){
				output=true;
			}
			
			return output;
		}
		
		public screen_type_enum get_screen_type(){
			
			screen_type_enum output = screen_type;
			
			if(output==screen_type_enum.initalizing){
				
				float aspect = (Screen.height*1.0f) / (Screen.width*1.0f);
				
				print ("width: "+Screen.width);
				print ("height: "+Screen.height);
				print ("aspect: "+aspect);
				
				output=screen_type_enum.ipad_3_4;
				if(aspect>1.6f){
					output= screen_type_enum.iphone_5_9_16;
				}else if(aspect>1.4f){
					output= screen_type_enum.iphone_4_2_3;
				}
			}
			
			screen_type = output;
			
			return output;
		}
		
		public int get_hitofude_width(){
			
			int output = hitofude_width_ipad;
			
			screen_type_enum temp = get_screen_type ();
			
			if(temp==screen_type_enum.iphone_5_9_16){
				output= hitofude_width_iphone5;
				
				#if UNITY_ANDROID
				output= hitofude_width_android_9_16;
				#endif
				
				#if UNITY_EDITOR
				output= hitofude_width_android_9_16;
				#endif
				
			}else if(temp==screen_type_enum.iphone_4_2_3){
				output= hitofude_width_iphone4;
			}else if(temp==screen_type_enum.ipad_3_4){
				output = hitofude_width_ipad;
			}
			
			return output;
		}
		
		public int get_hitofude_offset_y(){
			int output = 0;
			
			screen_type_enum temp = get_screen_type ();
			
			if (temp == screen_type_enum.iphone_5_9_16) {
				#if UNITY_ANDROID
				output= hitofude_offset_y_android_9_16;
				#endif
				
				#if UNITY_EDITOR
				output= hitofude_offset_y_android_9_16;
				#endif
			}else if (temp == screen_type_enum.ipad_3_4) {
				output=hitofude_offset_y_ipad;
			}else if(temp==screen_type_enum.iphone_4_2_3){
				output=hitofude_offset_y_iphone4;
			}
			
			return output;
		}
		
		public float get_panel_level_offset_y_bottom(){
			float output = -220.0f;
			
			screen_type_enum temp = get_screen_type ();
			
			
			
			if(temp==screen_type_enum.iphone_5_9_16){
				
				//1136/2 -200 = 368
				
				//output= -360.0f;
				
				output= -260.0f;
				
			} else if(temp==screen_type_enum.iphone_4_2_3){
				//960 /2  -200 =280
				
				//output= -272.0f;
				output=  -172.0f;
				output=  -172.0f;
				
			} else if(temp==screen_type_enum.ipad_3_4){
				
				
				//853 /2 -200 =226
				
				//output=  -220.0f;
				output=  -120.0f;
			}
			
			
			return output;
		}
		
		public Vector2 object_reposition(ngui_screen_adjust.object_type input_object ){
			
			Vector2 output = new Vector2(0,0);
			
			if(input_object==ngui_screen_adjust.object_type.result_button){
				
				output= new Vector2(0,-210);
				
				if(get_screen_type()==screen_type_enum.ipad_3_4){
					output= new Vector2(0,-160);
				}
				
			}else if(input_object==ngui_screen_adjust.object_type.normal_mode){
				
				//output= new Vector2(-128,-92);
				output= new Vector2(-128,0);
				
				if(get_screen_type()==screen_type_enum.iphone_5_9_16){
					//output= new Vector2(0,-32);
					output= new Vector2(0,50);
					
					#if UNITY_ANDROID
					output= new Vector2(0,60);
					#endif
					
					#if UNITY_EDITOR
					output= new Vector2(0,60);
					#endif
				}
			}else if(input_object==ngui_screen_adjust.object_type.dan_mode){
				
				
				//output= new Vector2(128,-92);
				output= new Vector2(128,0);
				
				if(get_screen_type()==screen_type_enum.iphone_5_9_16){
					//output= new Vector2(0,-240);
					output= new Vector2(0,-160);
					
					#if UNITY_ANDROID
					output= new Vector2(0,-130);
					#endif
					
					#if UNITY_EDITOR
					output= new Vector2(0,-130);
					#endif
				}
				
				
			}else if(input_object==ngui_screen_adjust.object_type.large_label_level){
				output= new Vector2(0,430);
				
				
				
			}else if(input_object==ngui_screen_adjust.object_type.small_label_level){
				output= new Vector2(-140,360);
				
				
				
			}else if(input_object==ngui_screen_adjust.object_type.large_label_dan){
				output= new Vector2(0,430);
				
				if(get_screen_type()==screen_type_enum.ipad_3_4){
					output= new Vector2(-140,375);
				}
				
			}else if(input_object==ngui_screen_adjust.object_type.small_label_dan){
				output= new Vector2(-140,360);
				
				if(get_screen_type()==screen_type_enum.ipad_3_4){
					output= new Vector2(140,400);
				}
				
			}else if(input_object==ngui_screen_adjust.object_type.turn_label_level){
				output= new Vector2(140,360);
				
				
			}else if(input_object==ngui_screen_adjust.object_type.turn_label_dan){
				output= new Vector2(140,360);
				
				if(get_screen_type()==screen_type_enum.ipad_3_4){
					output= new Vector2(140,360);
				}
				
				
			}else if(input_object==ngui_screen_adjust.object_type.result_stars){
				
				
				output= new Vector2(0,70);
				if(get_screen_type()==screen_type_enum.iphone_4_2_3){
					output= new Vector2(0,80);
				}else if(get_screen_type()==screen_type_enum.ipad_3_4){
					output= new Vector2(0,160);
				}
				
				
			}else if(input_object==ngui_screen_adjust.object_type.applimotion){
				output= new Vector2(0,200);
				
				if(get_screen_type()==screen_type_enum.iphone_4_2_3){
					output= new Vector2(0,260);
				}else if(get_screen_type()==screen_type_enum.ipad_3_4){
					output= new Vector2(0,280);
				}
			}else if(input_object==ngui_screen_adjust.object_type.hitofude_touch_panel){
				
				output= new Vector2(0,30);
				
				if(get_screen_type()==screen_type_enum.iphone_4_2_3){
					output= new Vector2(0,50);
				}else if(get_screen_type()==screen_type_enum.ipad_3_4){
					output= new Vector2(0,50);
				}
				
				
			}
			
			
			return output;
		}
		


	}
}
