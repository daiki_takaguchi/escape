﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using ESCAPE.DATA;



namespace ESCAPE.SYSTEM{
	[ExecuteInEditMode]
	public class Loader : MonoBehaviour {

		public bool load_data_now;

		public Data data;
		public Main main;
		public TextAsset text_data;
		public List<string> loaded_data;


		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {

			if(!Application.isPlaying){
				if(load_data_now){

					load_data(0);
					load_data_now=false;
				}
			}
		
		}

		public string load_data(int input_level){

			string output = "fail";

			if(load_data_now){
				text_data = (TextAsset)Resources.Load ("escape_data/hako000");
			}else{
				if (Main.main_static.settings.is_load_test) {
					text_data = (TextAsset)Resources.Load ("escape_data_test/hako000");
				} else {
					text_data = (TextAsset)Resources.Load ("escape_data/hako" + input_level.ToString ("000"));
					
				}
			}

			data.block_base_list.initialize (8,10);
			data.block_group_list.initialize ();
			main.block_image_list.initialize ();
			
			
			StringReader reader = new StringReader (text_data.text);
			

			
			bool point_flag = false;
			bool type_flag = false;
			bool count_flag = false;
			
			string last_string = "";

			List<string> type_string_list = new List<string> ();
			List<string> count_string_list = new List<string> ();

			int x = 0;
			int y = 0;

			int max_x = 0;
			int max_y = 0;
			
			while (reader.Peek() > -1) {
				
				
				string line = reader.ReadLine ();

				
				if (point_flag) {
					
					if (line == "#END") {

						point_flag = false;

						
					} else {

						loaded_data =  new List<string>(line.Split (','));

						loaded_data.RemoveAt(loaded_data.Count-1);


						x=0;

						bool is_wall_line=true;

						foreach (string one_string in loaded_data){

							int block_base_int=0;

							int.TryParse(one_string, out block_base_int);

							bool is_wall=false;

							if(block_base_int==0){

								data.block_base_list.get_block_base(x,y).initialize_image(Const.Data.Block_base_type.empty,Const.Data.Block_base_state.empty);

			
							}else if(block_base_int<100){

								Block_base temp=data.block_base_list.get_block_base(x,y);

								temp.initialize_image(Const.Data.Block_base_type.empty,Const.Data.Block_base_state.empty);

								data.block_group_list.add_block_to_group(temp,block_base_int-1);


							}else if(block_base_int==100){

								Block_base temp=data.block_base_list.get_block_base(x,y);

								temp.initialize_image(Const.Data.Block_base_type.empty,Const.Data.Block_base_state.empty);

								data.block_group_list.human.add_block(temp);


							}else if(block_base_int==101){

								data.block_base_list.get_block_base(x,y).initialize_image(Const.Data.Block_base_type.wall,Const.Data.Block_base_state.non_empty);

								is_wall=true;

							}else if(block_base_int==200){

								data.block_base_list.get_block_base(x,y).initialize_image(Const.Data.Block_base_type.gate,Const.Data.Block_base_state.empty);

								
								
							}else if(block_base_int==201){

								Block_base temp=data.block_base_list.get_block_base(x,y);

								data.block_base_list.locked_gate_list.Add (temp);

								temp.initialize_image(Const.Data.Block_base_type.locked_gate,Const.Data.Block_base_state.gate_closed);
								


							}else if(block_base_int==202){



								Block_base temp=data.block_base_list.get_block_base(x,y);

								data.block_base_list.lock_base_list.Add (temp);
								
								temp.initialize_image(Const.Data.Block_base_type.lock_base,Const.Data.Block_base_state.empty);

								
							}

							is_wall_line&=is_wall;

							x++;
						}

						if(is_wall_line){
							print (y + " is wall line!");
						}else{
							print (y + " is non wall line!");
						}

						data.block_base_list.is_wall_line_list.Add(is_wall_line);

						y++;
					}
					
					
					
				}


				if (type_flag) {
					
					if (line == "#END") {
						
						type_flag = false;

						//print ("type");


						int count=0;
						foreach(string one_string in type_string_list){

							int parsed_int=0;
							
							int.TryParse(one_string, out parsed_int);

							if(parsed_int!=0){
								if(data.block_group_list.has_block_group(count)){
									Block_group temp=data.block_group_list.get_block_group(count);

									temp.set_group_type(Const.Data.int_to_block_group_type(parsed_int));

								}
							}
							count++;
						}

					}else{
						loaded_data =  new List<string>(line.Split (','));
						
						loaded_data.RemoveAt(loaded_data.Count-1);

						type_string_list.AddRange(loaded_data);

					}
				
				
				
				}


				
				if (count_flag) {
					
					if (line == "#END") {
						
						count_flag = false;
						
						//print ("type");
						
						
						int count=0;
						foreach(string one_string in count_string_list){
							
							int parsed_int=0;
							
							int.TryParse(one_string, out parsed_int);
							

							if(data.block_group_list.has_block_group(count)){
								Block_group temp=data.block_group_list.get_block_group(count);
								if(parsed_int!=0){
									temp.initialize_count(parsed_int);

								}
							
								
							}


							count++;
						}
						
					}else{
						loaded_data =  new List<string>(line.Split (','));
						
						loaded_data.RemoveAt(loaded_data.Count-1);
						
						count_string_list.AddRange(loaded_data);
						
					}
					
					
					
				}


				//data.block_group_list.human.initialize_image();





				if (line == "#POINT") {
					last_string = "";
					point_flag = true;
					y=0;
				} else if (line == "#TYPE") {
					
					last_string = "";
					type_flag = true;
					
					
				} else if (line == "#COUNT") {
					last_string = "";
					count_flag = true;

				}


				
			
			}


			data.block_base_list.calculate_bound();
			data.block_base_list.reposition();
			main.game_state_manager.reset();


			//print (data.block_base_list.non_wall_line_from_top());
			//print (data.block_base_list.non_wall_line_from_bottom());
			
			
			return output;
		}
	
	}
} 
