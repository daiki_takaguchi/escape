﻿using UnityEngine;
using System;
using System.Collections;
using ESCAPE.SYSTEM;

namespace ESCAPE.SYSTEM{
	public class Save_data : MonoBehaviour {

		// Use this for initialization

		public Main main;

		void Start () {


			if(main.settings.clear_data){
				PlayerPrefs.DeleteAll ();
			}
		

			if(!PlayerPrefs.HasKey("level_0")){
				PlayerPrefs.SetInt("level_0",1);
			}

			if(!PlayerPrefs.HasKey("dan_0")){
				PlayerPrefs.SetInt("dan_0",1);
			}


		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void initialize(){

			//PlayerPrefs.SetInt ("initialized",1);
			PlayerPrefs.SetInt("level_0",1);
			PlayerPrefs.SetInt("dan_0",1);
			PlayerPrefs.Save ();

		}


		public Const.Ui.Level_state get_select_level_data(int level_no){
			Const.Ui.Level_state output = Const.Ui.Level_state.Locked;

			string label = "level_" + level_no;

			if(PlayerPrefs.HasKey(label)){
				output=int_to_level_state(PlayerPrefs.GetInt(label));
			}
			

			return output;
		}

		public Const.Ui.Level_state get_select_dan_data(int dan_no){
			Const.Ui.Level_state output = Const.Ui.Level_state.Locked;
			
			string label = "dan_" + dan_no;

			if(PlayerPrefs.HasKey(label)){

				output=int_to_level_state(PlayerPrefs.GetInt(label));
		
			}
			
			
			return output;
		}



		public void set_select_level_data(int level_no,Const.Ui.Level_state input_state){
			int temp = level_state_to_int (input_state);

			string label = "level_" + level_no;

			if(PlayerPrefs.HasKey(label)){
				if(PlayerPrefs.GetInt(label)<temp){
					PlayerPrefs.SetInt(label,temp);
					PlayerPrefs.Save();
				}
			}else{
				PlayerPrefs.SetInt(label,temp);
				PlayerPrefs.Save();
			}
		}

		public void set_select_dan_data(int level_no,Const.Ui.Level_state input_state){
			int temp = level_state_to_int (input_state);
			
			string label = "dan_" + level_no;
			
			if(PlayerPrefs.HasKey(label)){
				if(PlayerPrefs.GetInt(label)<temp){
					PlayerPrefs.SetInt(label,temp);
					PlayerPrefs.Save();
				}
			}else{
				PlayerPrefs.SetInt(label,temp);
				PlayerPrefs.Save();
			}
		}

		public int level_state_to_int (Const.Ui.Level_state input_state){
			int output = 0;
			
			if(input_state==Const.Ui.Level_state.Locked){
				output=0;
			}else if(input_state==Const.Ui.Level_state.Zero){
				output=1;
			}else if(input_state==Const.Ui.Level_state.One){
				output=2;
			}else if(input_state==Const.Ui.Level_state.Two){
				output=3;
			}else if(input_state==Const.Ui.Level_state.Three){
				output=4;
			}
			
			return output;
		}
		

		public Const.Ui.Level_state int_to_level_state(int input_int){
			Const.Ui.Level_state output = Const.Ui.Level_state.Locked;
			
			if(input_int==0){
				output=Const.Ui.Level_state.Locked;
			}else if(input_int==1){
				output=Const.Ui.Level_state.Zero;
			}else if(input_int==2){
				output=Const.Ui.Level_state.One;
			}else if(input_int==3){
				output=Const.Ui.Level_state.Two;
			}else if(input_int==4){
				output=Const.Ui.Level_state.Three;
			}
			
		
			return output;
		}

	public string get_level_progress(){

			int total_score = 0;
			int max = main.settings.max_level;


			//if(is_dan){
			//	max=system.settings.max_dan;
			//}


			for (int i =0; i < max;i++){

				Const.Ui.Level_state temp =Const.Ui.Level_state.Locked;

				//if(is_dan){
				//	temp =get_select_dan_data(i);
				//}else{
					temp =get_select_level_data(i);
				//}

				int score=0;

				if(temp==Const.Ui.Level_state.Locked){
					score=0;
				}else if(temp==Const.Ui.Level_state.Zero){
					score=0;
				}else if(temp==Const.Ui.Level_state.One){
					score=1;
				}else if(temp==Const.Ui.Level_state.Two){
					score=2;
				}else if(temp==Const.Ui.Level_state.Three){
					score=3;
				}


				total_score+=score;
			}

			string output = "";

			if(total_score==max*3){
				output = "★100%";
			
			
			}else {

				float total_score_float= Mathf.RoundToInt (1000*total_score/(max*3.0f))/10.0f;


				if(total_score_float<=0.1f&&total_score>0){
					total_score_float=0.1f;
				}

				if(total_score_float>=100.0f){
					total_score_float=99.9f;
				}

				output = "★"+total_score_float.ToString("00.0")+"%";
			}

			//print (total_score);

			return output;


		}


		public string get_dan_progress(){
			//string label;

			int cleared = -1;
			int max = main.settings.max_dan;

			for (int i =0; i < max; i++) {
				string label="dan_"+i;

				if(PlayerPrefs.HasKey(label)){
					if(PlayerPrefs.GetInt(label)>1){
						cleared=i;
					}

				}
			}

			string output="";

			if(cleared>-1){
				output=main.settings.dan_list[cleared];

			}

			return output;

			
		}
		

				



		public void hint_used(){
			PlayerPrefs.SetString ("hint", get_unix_time_now().ToString());
			PlayerPrefs.Save ();
		}

		public void answer_used(){
			PlayerPrefs.SetString ("answer", get_unix_time_now().ToString ());
			PlayerPrefs.Save ();
		}

		public long remaining_time_hint(){

			//long output = 7200;

			long output = main.settings.hint_interval_in_seconds;
			//print (PlayerPrefs.GetString("hint"));

			if(PlayerPrefs.HasKey("hint")){
				output-=(get_unix_time_now()-long.Parse(PlayerPrefs.GetString("hint")));
			}else{
				output=0;
			}



			if(output<0){
				output=0;
			}



			return output;


			
		}

		public long remaining_time_answer(){
			//long output = 86400;
			//output = 5;
			long output = main.settings.answer_interval_in_seconds;
			
			if(PlayerPrefs.HasKey("answer")){
				
				output-=(get_unix_time_now()-long.Parse(PlayerPrefs.GetString("answer")));
			}else{
				output=0;
			}
			
			
			if(output<=0){
				output=0;
				PlayerPrefs.DeleteKey ("answer");
			}
			
			return output;
			
		}
	

		public bool has_tutorial_shown(int level,bool is_dan){

			string key = "tutorial_" + level +"_"+ is_dan;


			bool output = false;

			if(PlayerPrefs.HasKey(key)){
				output=true;
			}

			PlayerPrefs.SetInt (key,0);

			return output;
		
		}

		public bool is_dan_ready(){
			bool output = false;

			if(PlayerPrefs.HasKey("level_"+main.settings.dan_unlock_level)){
				output=true;
			}

			if(PlayerPrefs.HasKey ("is_dan_ready")){
				output=true;
			}


			return output;
		
		}

		public void set_dan_ready(){
			PlayerPrefs.SetInt ("is_dan_ready",0);
		}

		public long get_unix_time_now(){
			DateTime UnixEpoch =new DateTime (1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			return (long) (DateTime.UtcNow - UnixEpoch).TotalSeconds;
		}

	}
}
