﻿using UnityEngine;
using System.Collections;

public class window_dan_clear_controller : MonoBehaviour {

	// Use this for initialization

	public UILabel label; 

	public animation_state state;

	//private float wait_time=1.5f;

	public enum animation_state{
		cleared,
		time_out,
		game_over
	}

	void Start () {
	
	}


	
	// Update is called once per frame
	void Update () {
	
	}

	public void show_window(string input_text){

		//set_text (input_state);
		//state = input_state;
		label.text = input_text;
		gameObject.SetActive (true);

	}

	public void show_window(animation_state input_state){
		
		set_text (input_state);
		state = input_state;
		gameObject.SetActive (true);
		
	}

	void set_text(animation_state input_state){

		string text = "";
		if (input_state == animation_state.time_out) {
			text = "Time Out";
		} else if (input_state == animation_state.game_over) {
			text = "Game Over";
		}else if(input_state==animation_state.cleared){
			text = "Cleard!";
		}


		label.text = text;
	}

	IEnumerator wait_for_seconds(float time){
		yield return new WaitForSeconds(time);
		wait_end();
	}

	public void wait_end(){
		gameObject.SetActive (false);
	}
}
