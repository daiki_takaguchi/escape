﻿using UnityEngine;
using System.Collections;

public class image_switcher : MonoBehaviour {

	public GameObject japanese;
	public GameObject english;


	void Start () {

		ESCAPE.SYSTEM.Main main= FindObjectOfType<ESCAPE.SYSTEM.Main>();

		if (main.settings.is_japanese()) {
			japanese.SetActive (true);
		
			english.SetActive(false);
		} else {

			japanese.SetActive (false);
			
			english.SetActive(true);
		
		}
		
	}



}
