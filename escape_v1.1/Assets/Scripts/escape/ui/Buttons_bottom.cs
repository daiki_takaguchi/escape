﻿using UnityEngine;
using System.Collections;

namespace ESCAPE.UI{
	public class Buttons_bottom : MonoBehaviour {

		public GameObject home;
		public GameObject undo;
		public GameObject reset;
		public GameObject back;

		public enum Button_buttom_type{
			home,
			undo,
			reset,
			all
		}


		void Start () {

		}

		void Update(){

		}

		public void show(Button_buttom_type input_button){
			switch(input_button){
				case Button_buttom_type.home:
					home.SetActive(true);
					back.SetActive(true);
					break;
				case Button_buttom_type.all:
					home.SetActive(true);
					undo.SetActive(true);
					reset.SetActive(true);
					back.SetActive(true);
				
					break;

				default:
					Debug.LogError("error");
					break;
			}

		}

		public void hide(Button_buttom_type input_button){

			switch(input_button){
			
			case Button_buttom_type.all:
				home.SetActive(false);
				undo.SetActive(false);
				reset.SetActive(false);
				back.SetActive(false);
				
				break;
				
			default:
				Debug.LogError("error");
				break;
			}
			
		}
	}
}
