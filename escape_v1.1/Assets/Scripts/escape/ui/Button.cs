﻿using UnityEngine;
using System.Collections;

namespace ESCAPE.UI{
public class Button : MonoBehaviour {
	
	public Ui ui;

	public button_type type;

	//public Transform reference_position;

	public bool is_avalable=true;

	public int level;

	public enum button_type{
		button_home,
		button_undo,
		button_restart,
		button_hint,
		button_answer,
		button_hint_yes,
		button_hint_no,
		button_answer_yes,
		button_answer_no,
		button_next,
		button_tweet,
		button_review,
		button_mode_normal,
		button_mode_dan,
		button_select_level,
		button_select_dan,
		point,
		touch_panel,
		touch_panel_unpressed,
		button_home_no_SE,
		button_applipromotion,
		button_unlock_dan_yes,
		button_unlock_dan_no,
		block_image
	}

	public float click_detection_delay_time=0.0f;
	public float enabled_time;
	public float press_started_time;

	void Start () {

		if (type == button_type.button_applipromotion) {
			click_detection_delay_time=ui.main.settings.button_applipromotion_click_detection_delay_time;
		}

		//button_pressed.animation.Stop ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnPress (bool pressed)
	{
		//print ("pressed");
		
		
		if (pressed) {
			press_started_time = Time.time;
		}
	}

	void OnEnable(){
		enabled_time = Time.time;
	}
	
	void OnClick (){
		if(ui.main.settings.apply_click_detection_limit_time_to_all_buttons){
			if (Time.time <= press_started_time + ui.main.settings.button_select_level_click_detection_limit_time) {
				clicked (ui);
			}
		}else{
			clicked (ui);
		}
	}

	public void clicked(Ui input_ui){
		if (Time.time>=enabled_time+click_detection_delay_time) {
			print (type+" clicked");
			input_ui.button_pressed_delay (this);
		}
	
	}

}
}
