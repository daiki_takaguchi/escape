﻿using UnityEngine;
using System.Collections;
using ESCAPE.SYSTEM;

public class time_count_controller : MonoBehaviour {

	// Use this for initialization
	public UI_controller ui;

	public bool is_counting=false;
	public bool is_paused=false;
	private float remaining_time;
	//private float count_start_time;
	private float last_update_time;
	//private float paused_end_time=0.0f;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if(is_counting&&!is_paused){

			remaining_time=remaining_time-(Time.time-last_update_time);

			if(remaining_time<=0.0f){
				time_out ();
			}else{

				/*

				int minuites =Mathf.FloorToInt(remaining_time/60);
				int seconds= Mathf.FloorToInt(remaining_time %60);
				GetComponent<UILabel>().text=minuites.ToString ("D2")+":"+seconds.ToString ("D2");*/


				set_text(remaining_time);


			}


			last_update_time = Time.time;
		}
	
	}

	public void set_text(float input_remaining_time){

		int minuites =Mathf.FloorToInt(input_remaining_time/60);
		int seconds= Mathf.FloorToInt(input_remaining_time %60);
		GetComponent<UILabel>().text=minuites.ToString ("D2")+":"+seconds.ToString ("D2");
	
	}


	public void start_count(float input_remaining_time){
		is_counting = true;
		is_paused = false;

		remaining_time = input_remaining_time;

		//count_start_time = Time.time;
		last_update_time = Time.time;
	}

	public void pause_count(){
		if(is_counting){
			is_paused = true;
		}
	}

	public void resume_count(){
		if(is_counting){
			is_paused = false;
			last_update_time = Time.time;
		}
	}

	public void stop_count(){
		is_counting = false;
	}


	public void time_out(){
		GetComponent<UILabel>().text="00:00";
		stop_count();
		Main.main_static.game_state_manager.time_out ();

		//ui.button_time_out_pressed ();
	}


}
